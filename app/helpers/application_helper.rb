module ApplicationHelper
  def page_entries_info(collection, options = {})
    collection_name = options[:collection_name] || (collection.empty?? 'entry' : collection.first.class.name.underscore.sub('_', ' '))
    if collection.num_pages < 2
      case collection.size
      when 0; info = "No #{collection_name.pluralize} found"
      when 1; info = "Displaying <strong>1</strong> #{collection_name}"
      else;   info = "Displaying <strong>all #{collection.size}</strong> #{collection_name.pluralize}"
      end
    else
      start = collection.per_page * (collection.current_page - 1) + 1
      info = %{Displaying #{collection_name.pluralize} <strong>%d&ndash;%d</strong> of <strong>%d</strong> in total}% [
        start,
        start + collection.length - 1,
        collection.total_entries
      ]
    end
    info.html_safe
  end

  def available_us_states
    Carmen.states + [['International', 'International']]
  end

end
