class AppSettings < ActiveRecord::Base
  has_attached_file :blank_application

  def self.notice
    if AppSettings.first && !AppSettings.first.applications_notice.blank?
      AppSettings.first.applications_notice
    else
      nil
    end
  end

  # Serialize and store attachment categories inside this model
  # instead of having a separate model since we're not doing anything special
  # with it. Switch to a belongs_to later if more functionality is required.

  def self.attachment_categories
    begin
      JSON.parse AppSettings.first.attachment_categories_data
    rescue
      []
    end
  end

  def self.attachment_categories_with_emptiness
    self.attachment_categories.tap do |x|
      x.each do |y|
        attachments = Attachment.where :ref_id => y['id']
        y['is_empty'] = attachments.blank?
      end
    end
  end

  def self.admin_attachment_categories
    self.attachment_categories.select{|x| x['admin'] && !x['cw'] && !x['is_archived']}
  end

  def self.cw_attachment_categories
    self.attachment_categories.select{|x| x['cw'] && !x['admin'] && !x['is_archived']}
  end

  def self.admin_cw_attachment_categories
    self.attachment_categories.select{|x| (x['cw'] && x['admin'] && !x['is_archived'])}
  end

  def self.remove_category(id)
    categories = AppSettings.attachment_categories
    category = categories.select { |x| x['id'] == id}
    unless category.blank?
      attachments = Attachment.where :ref_id => id
      if attachments.blank?
        categories.delete category.first
      else
        category.first['is_archived'] = true
        categories.push category.first
      end
      AppSettings.first.update_attribute :attachment_categories_data, categories.to_json
    end
  end

  def self.update_category(id, new_category)
    categories = AppSettings.attachment_categories
    category = categories.select { |x| x['id'] == id}
    unless category.blank?
      new_category['name'] = category.first['name']
      categories.delete category.first
      categories.push new_category
      AppSettings.first.update_attribute :attachment_categories_data, categories.to_json
    end
  end

  def self.add_category(category)
    AppSettings.first.update_attribute :attachment_categories_data,
                                       (AppSettings.attachment_categories + [category]).uniq.to_json
  end

  def self.invalid_credentials_message
    "An email notification was unable to be delivered.  A Hero Miles administrator may need to update the system email settings."
  end

end
