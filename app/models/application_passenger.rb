class ApplicationPassenger < ActiveRecord::Base
  belongs_to :application

  #before_save :check_lap_child

  #validates_presence_of :first_name, :last_name, :gender, :relation, :date_of_birth, :unless => :skip_validation?

  def legal_name
    "#{self.first_name} #{self.middle_name.blank? ? '' : self.middle_name } #{self.last_name}"
  end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def export_name
    "#{self.last_name}/#{self.first_name}"
  end

  def formatted_date_of_birth
    if self.date_of_birth.blank?
      ""
    else
      self.date_of_birth.strftime("%m/%d/%Y")
    end
  end

  def skip_validation?
    self.is_exception?
  end

  def is_child?
    self.date_of_birth.is_a?(Date) && ((Date.today - self.date_of_birth)/365).to_i < 2
  end

  protected

  def check_lap_child
    if is_child?
      self.is_lap_child = true
      self.ticket_no = 'Lap Child'
    else
      self.is_lap_child = false
      self.ticket_no = '' if self.ticket_no == 'Lap Child'
    end
  end

end
