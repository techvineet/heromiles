class ApplicationNote < ActiveRecord::Base
  belongs_to :application
  belongs_to :user

  after_create :set_notification_dates

  after_create :email_admin, :unless => Proc.new { |x| x.is_action? || x.user.is_admin? }
  after_create :email_cw, :unless => Proc.new { |x| x.is_action? || !x.user.is_admin? }

  def email_admin
    if self.application.admin && self.application.admin.receive_notifications
      emails = self.application.admin.email
    else
      emails = User.admin.active.receive_notifications.collect(&:email)
    end
    begin
      AppMailer.email_note_to_admin(emails, self).deliver
    rescue
      ""
    end
  end

  def email_cw
    unless self.application.admin == self.application.user
      begin
        AppMailer.email_note_to_cw(self).deliver
      rescue
        ""
      end
    end
  end

  protected

  def set_notification_dates
    unless self.is_action?
      if self.user.is_admin?
        self.application.update_attributes({:last_admin_note_date => Time.zone.today,
                                           :last_action_by_admin => true},
                                           :as => :admin)
      else
        self.application.update_attributes({:last_cw_note_date => Time.zone.today,
                                            :last_action_by_admin => false},
                                            :as => :admin)
      end
    end
  end

end
