class Airline < ActiveRecord::Base
  default_scope where(:is_archived => false)

  scope :active, where(:is_archived => false)

end
