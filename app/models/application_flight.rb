class ApplicationFlight < ActiveRecord::Base
  belongs_to :application

  scope :outbound, where(:is_return => false)
  scope :return, where(:is_return => true)

  before_save :upcase_seats

  def flight_description
    text = "#{self.flight_no} From #{self.depart_from}"
    text += " at #{self.formatted_departure_time}"
    text += " To #{self.arrive_in}"
    text += " at #{self.formatted_arrival_time}"
  end

  def formatted_departure_time
    self.depart_time.nil? ? '-' : self.depart_time.strftime("%I:%M %P")
  end

  def formatted_arrival_time
    self.arrive_time.nil? ? '-' : self.arrive_time.strftime("%I:%M %P")
  end

  def depart_time=(time)
    if time.is_a? String
      begin
        self.depart_time = Time.parse "#{time} UTC"
      rescue
      end
    else
      super
    end
  end

  def arrive_time=(time)
    if time.is_a? String
      begin
        self.arrive_time = Time.parse "#{time} UTC"
      rescue
      end
    else
      super
    end
  end


  def formatted_seats
    seats = []
    self.application.application_passengers.size.times do |n|
      unless self.send("seat_#{n+1}").nil? || !self.send("seat_#{n+1}").is_a?(String)
        if self.send("seat_#{n+1}").strip == ''
          seats << "UNASSIGNED"
        else
          seats << self.send("seat_#{n+1}").upcase
        end
      end
    end
    seats.join ', '
  end

  def upcase_seats
    6.times do |n|
      unless self.send("seat_#{n+1}").blank?
        self.send("seat_#{n+1}").upcase!
      end
    end
  end

end
