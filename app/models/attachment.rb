class Attachment < ActiveRecord::Base
  belongs_to :application

  # FIXME save to private location
  has_attached_file :document

  before_create :set_details

  before_post_process :sanitize_file_name

  protected

  def sanitize_file_name
    extension = self.document_file_name.scan(/\.\w+$/).first
    filename = self.document_file_name.sub("#{extension}", '')
    self.document.instance_write :file_name, "#{sanit(filename)}#{extension}"
  end

  def set_details
    unless self.ref_id.blank? && self.category.blank?
      id = self.category.blank?? self.ref_id : self.category
      cat = AppSettings.attachment_categories.select{|x| x['id'] == id}.first
      if cat
        self.ref_id = cat['id']
        self.is_cw = cat['cw']
        self.is_required = cat['required']
        self.is_admin = cat['admin']
        self.category = cat['name']
      end
    end
  end

  def sanit(str)
    s = str.downcase
    s.gsub! /'/, ''
    s.gsub! /[^A-Za-z0-9]+/, ' '
    s.strip!
    s.gsub /\ +/, '-'
  end
end
