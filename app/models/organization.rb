class Organization < ActiveRecord::Base
  default_scope where(:is_archived => false)

  has_many :users
  has_many :applications
  has_many :groups

  scope :active, where(:is_archived => false)

  def self.sorted
    self.all.sort_by(&:name)
  end

  def is_empty?
    users.blank? && applications.blank? && groups.blank?
  end

end
