class Group < ActiveRecord::Base
  extend Unscoped

  has_many :applications
  belongs_to :organization
  unscope :organization

  scope :active, where(:is_archived => false)
  scope :archived, where(:is_archived => true)

  def fields
    begin
      JSON.parse self.fields_data
    rescue
      {}
    end
  end

  def set_default_fields
    self.fields_data = Group.all_fields_data
  end

  def self.all_fields
    begin
      JSON.parse self.all_fields_data
    rescue
      {}
    end
  end

  def self.all_fields_data
    "{\"information_collected_by\":\"\",\"referrer_first_name\":\"\",\"referrer_last_name\":\"\",\"referrer_job_title\":\"\",\"referrer_medical_center_id\":\"\",\"referrer_organization_id\":\"\",\"referrer_phone_1\":\"\",\"referrer_phone_2\":\"\",\"referrer_phone_3\":\"\",\"referrer_email\":\"\",\"first_name\":\"\",\"gender\":\"\",\"phone_1\":\"\",\"phone_2\":\"\",\"phone_3\":\"\",\"alt_phone_1\":\"\",\"alt_phone_2\":\"\",\"alt_phone_3\":\"\",\"email\":\"\",\"service_branch\":\"\",\"current_status\":\"\",\"is_inpatient\":\"\",\"injury_status\":\"\",\"has_used_heromiles\":\"\",\"last_use_date\":\"\",\"flight_type\":\"\",\"depart_from_1_id\":\"\",\"depart_from_2_id\":\"\",\"arrive_in_1_id\":\"\",\"arrive_in_2_id\":\"\",\"depart_from_1_select\":\"\",\"depart_from_2_select\":\"\",\"arrive_in_1_select\":\"\",\"arrive_in_2_select\":\"\",\"departure_date\":\"\",\"return_date\":\"\",\"flight_details\":\"\",\"special_request\":\"\",\"travellers\":\"\",\"passenger_count\":\"\"}"
  end


end
