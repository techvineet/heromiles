class Application < ActiveRecord::Base
  include ActiveRecord::Transitions
  extend Unscoped

  has_many :application_notes, :dependent => :destroy
  has_many :application_passengers, :dependent => :destroy
  has_many :outbound_flights, :dependent => :destroy, :class_name => 'ApplicationFlight', :conditions => {:is_return => false}
  has_many :return_flights, :dependent => :destroy, :class_name => 'ApplicationFlight', :conditions => {:is_return => true}
  has_many :attachments, :dependent => :destroy, :conditions => {:owned_by_admin => false}
  has_many :admin_attachments,
           :dependent => :destroy,
           :class_name => 'Attachment',
           :conditions => { :owned_by_admin => true }

  belongs_to :user
  belongs_to :admin, :foreign_key => :admin_id, :class_name => "User"

  belongs_to :organization
  unscope :organization

  belongs_to :medical_center
  unscope :medical_center

  belongs_to :referrer_organization, :foreign_key => :referrer_organization_id, :class_name => "Organization"
  unscope :referrer_organization

  belongs_to :referrer_medical_center, :foreign_key => :referrer_medical_center_id, :class_name  => "MedicalCenter"
  unscope :referrer_medical_center

  belongs_to :group

  belongs_to :final_depart_from_1, :foreign_key => :final_depart_from_1_id, :class_name => "Airport"
  unscope :final_depart_from_1
  belongs_to :final_depart_from_2, :foreign_key => :final_depart_from_2_id, :class_name => "Airport"
  unscope :final_depart_from_2
  belongs_to :final_arrive_in_1, :foreign_key => :final_arrive_in_1_id, :class_name => "Airport"
  unscope :final_arrive_in_1
  belongs_to :final_arrive_in_2, :foreign_key => :final_arrive_in_2_id, :class_name => "Airport"
  unscope :final_arrive_in_2

  belongs_to :depart_from_1, :foreign_key => :depart_from_1_id, :class_name => "Airport"
  unscope :depart_from_1
  belongs_to :depart_from_2, :foreign_key => :depart_from_2_id, :class_name => "Airport"
  unscope :depart_from_2
  belongs_to :arrive_in_1, :foreign_key => :arrive_in_1_id, :class_name => "Airport"
  unscope :arrive_in_1
  belongs_to :arrive_in_2, :foreign_key => :arrive_in_2_id, :class_name => "Airport"
  unscope :arrive_in_2

  accepts_nested_attributes_for :application_passengers, :allow_destroy => true
  accepts_nested_attributes_for :outbound_flights, :allow_destroy => true
  accepts_nested_attributes_for :return_flights, :allow_destroy => true
  accepts_nested_attributes_for :attachments, :allow_destroy => true
  accepts_nested_attributes_for :admin_attachments, :allow_destroy => true

  scope :open_applications, where(:state => ['saved', 'returned', 'submitted'])
  scope :submitted, where(:state => ['submitted', 'exception'])
  scope :cw_notifications, where("applications.state not in (?)", ['saved','returned', 'submitted'])
  scope :admin_notification_messages, lambda { |user| where("((applications.last_action_by_admin = (?) ) and (applications.admin_id = (?) or applications.admin_id is ? ) )",false, user, nil)}
  scope :admin_notification_completed, lambda { |user| where("(applications.state in (?))  and applications.admin_id = ?", ['completed', 'declined', 'returned'], user)}
  scope :admin_open, lambda { |user| where("(applications.state in (?)) or (applications.state in (?) and applications.user_id = ?)", ['submitted', 'exception', 'reopened'], ['saved', 'returned'], user) }

  before_save :remove_one_way_return_date, :set_admin_dates, :upcase_record_locator
  before_create :set_default_attributes

  validates_presence_of :first_name, :last_name, :phone_1, :phone_2, :phone_3,
                        :service_branch, :current_status, #, :organization_id,
                        :injury_status, :flight_type, :depart_from_1_id, :arrive_in_1_id, :departure_date, :unless => :skip_validation?

  #validates_acceptance_of :injury_after_911, :unless => :skip_validation?, :accept => true, :allow_nil => false

  validates_inclusion_of :has_used_heromiles, :is_inpatient, :in => [true, false], :unless => :skip_validation?

  attr_protected :airline, :record_locator, :total_price, :comparable_price,
                 :final_depart_from_1_id, :final_depart_from_2_id, :final_arrive_in_1_id, :final_arrive_in_2_id, :final_departure_date, :final_return_date,
                 :case_worker_first_name, :case_worker_last_name, :case_worker_job_title,
                 :case_worker_medical_center_id, :case_worker_organization_id, :is_exception, :is_locked, :admin_id, :state, :last_action_by_admin, :last_admin_action_date, :last_cw_note_date, :last_admin_note_date, :created_by_admin,
                 :as => :default

  state_machine do
    state :new
    state :saved
    state :submitted
    state :exception
    state :imported
    state :returned
    state :declined
    state :completed
    state :reopened

    event :save_application do
      transitions :to => :saved, :from => [:new, :saved, :returned], :on_transition => [:set_user, :set_saved_time]
    end

    event :submit, :success => :email_admin_users do
      transitions :to => :submitted, :from => [:new, :saved, :returned], :on_transition => [:set_user, :set_submission_time]
    end

    event :import do
      transitions :to => :imported, :from => [:new, :complete], :on_transition => [:set_user, :set_submission_time]
    end

    event :raise_exception, :success => :email_admin_users do
      transitions :to => :exception, :from => [:new, :saved, :submitted, :imported, :returned], :on_transition => [:set_user, :set_submission_time]
    end

    event :return, :success => :email_cw do
      transitions :to => :returned, :from => [:reopened, :exception, :submitted], :on_transition => [:set_admin_last_action, :set_user, :set_returned_time]
    end

    event :decline, :success => :email_cw do
      transitions :to => :declined, :from => [:reopened, :exception, :submitted], :on_transition => [:set_admin_last_action, :set_user, :set_declined_time]
    end

    event :complete, :success => :email_cw do
      transitions :to => :completed, :from => [:reopened, :exception, :submitted, :imported, :returned, :declined, :new], :on_transition => [:set_admin_last_action, :set_user, :set_completion_time]
    end

    event :reopen, :success => :email_cw do
      transitions :to => :reopened, :from => [:declined, :completed], :on_transition => :set_user
    end

  end

  define_index do
    indexes :first_name

    indexes :last_name
    indexes :email
    indexes :current_status
    indexes :depart_from_1
    indexes :depart_from_2
    indexes :arrive_in_1
    indexes :arrive_in_2

    indexes :airline
    indexes :record_locator
    indexes :total_price
    indexes :comparable_price
    indexes :case_worker_first_name
    indexes :case_worker_last_name

    # TODO Figure out where else this name change affects
    # us and rename :as accordingly.
    indexes medical_center(:name), :as => :organization
    indexes group(:name), :as => :group

    indexes application_passengers.first_name, :as => :passenger_fname
    indexes application_passengers.last_name, :as => :passenger_lname
    indexes application_passengers.gender, :as => :passenger_gender
    indexes application_passengers.relation, :as => :passenger_relation
    indexes application_passengers.city, :as => :passenger_city
    indexes application_passengers.state, :as => :passenger_state
    indexes application_passengers.email, :as => :passenger_email
    indexes application_passengers.date_of_birth, :as => :passenger_dob

    has user_id
    indexes :created_at , :as => :created, :sortable => true
    indexes :booked_at, :as => :booked, :sortable => true
    indexes :updated_at , :as => :updated, :sortable => true
    indexes :submitted_at, :as => :submitted, :sortable => true
    has organization_id
    has medical_center_id
    has case_worker_organization_id
    has case_worker_medical_center_id
    has group_id
    has completed_at
    has saved_at
    has returned_at
    has declined_at
    has departure_date
    indexes :final_departure_date, :as => :final_departure, :sortable => true
    has return_date
    has final_return_date
    has [last_name, first_name], :as => :service_member_name, :sortable => true
    has [case_worker_first_name, case_worker_last_name], :as => :case_worker_name, :sortable => true
    has medical_center(:name), :as => :medical_center_name, :sortable => true
    has "CRC32(applications.service_branch)", :as => :service_branch, :type => :integer
    has "CRC32(applications.state)", :as => :state, :type => :integer, :sortable => true
  end

  def event_fired(current, new)
    options = {}
    @user ||= nil
    @exception ||= nil
    if new.to_s == 'exception'
      #options[:message] = "#{@exception}"
      options[:message] = check_for_missing_fields(@exception)
      options[:status] = 'Submitted as an exception'
    elsif new.to_s == 'completed' && !@exception.blank?
      #options[:message] = "#{@exception}"
      options[:message] = check_for_missing_fields(@exception, true)
      options[:status] = 'Completed as an exception'
    else
      options[:message] = ""
      options[:status] = new.capitalize
    end
    options[:user] = @user
    if @user
      self.application_notes.create! options
    end
  end

  def formatted_departure_date
    self.departure_date.blank? ? "UNKNOWN" : self.departure_date.strftime("%m/%d/%Y")
  end

  def formatted_final_departure_date
    self.final_departure_date.blank? ? "UNKNOWN" : self.final_departure_date.strftime("%m/%d/%Y")
  end


  def formatted_return_date
    self.return_date.blank? ? "UNKNOWN" : self.return_date.strftime("%m/%d/%Y")
  end

  def formatted_admin_note_date
    self.last_admin_note_date.blank? ? '-' : self.last_admin_note_date.strftime("%m/%d/%Y")
  end

  def formatted_cw_note_date
    self.last_cw_note_date.blank? ? '-' : self.last_cw_note_date.strftime("%m/%d/%Y")
  end

  def formatted_submission_date
    self.submitted_at.blank? ? '-' : self.submitted_at.strftime("%m/%d/%Y")
  end

  def formatted_completion_date
    self.completed_at.blank? ? '-' : self.completed_at.strftime("%m/%d/%Y")
  end

  def formatted_booking_date
    self.booked_at.blank? ? '-' : self.booked_at.strftime("%m/%d/%Y")
  end

  def formatted_depart_from_1
    self.depart_from_1.blank? ? '' : self.depart_from_1.formatted
  end

  def formatted_depart_from_2
    self.depart_from_2.blank? ? '' : self.depart_from_2.formatted
  end

  def formatted_arrive_in_1
    self.arrive_in_1.blank? ? '' : self.arrive_in_1.formatted
  end

  def formatted_arrive_in_2
    self.arrive_in_2.blank? ? '' : self.arrive_in_2.formatted
  end

  def formatted_final_depart_from_1
    self.final_depart_from_1.blank? ? '' : self.final_depart_from_1.formatted
  end

  def formatted_final_depart_from_2
    self.final_depart_from_2.blank? ? '' : self.final_depart_from_2.formatted
  end

  def formatted_final_arrive_in_1
    self.final_arrive_in_1.blank? ? '' : self.final_arrive_in_1.formatted
  end

  def formatted_final_arrive_in_2
    self.final_arrive_in_2.blank? ? '' : self.final_arrive_in_2.formatted
  end

  def service_member_name
    (self.first_name.blank? || self.last_name.blank?) ? "UNKNOWN" : "#{self.last_name},  #{self.first_name}"
  end

  def service_member_export_name
    (self.first_name.blank? || self.last_name.blank?) ? "UNKNOWN" : "#{self.last_name}/#{self.first_name} #{self.middle_name}"
  end

  def case_worker_name
    "#{self.case_worker_first_name} #{self.case_worker_last_name}"
  end

  def admin_name
    self.admin.blank? ? 'Unassigned' : self.admin.full_name
  end

  def organization_name
    self.organization.blank? ? '' : self.organization.name
  end

  def medical_center_name
    self.medical_center.blank? ? '' : self.medical_center.name
  end

  def org_or_med_center_name
    if self.organization.blank?
      if self.medical_center.blank?
        "-"
      else
        self.medical_center.name
      end
    else
      self.organization.name
    end
  end

  def referrer_org_or_med_center_name
    if self.referrer_organization.blank?
      if self.referrer_medical_center.blank?
        ""
      else
        self.referrer_medical_center.name
      end
    else
      self.referrer_organization.name
    end
  end

  def referrer_or_cw_org_name
    if self.referrer_organization.blank?
      if self.user.organization.blank?
        ""
      else
        self.user.organization.name
      end
    else
      self.referrer_organization.name
    end
  end

  def refferer_or_cw_export_name
    if self.referrer_first_name.blank? && self.referrer_last_name.blank?
      if self.user.first_name.blank? && self.user.last_name.blank?
        ""
      else
        "#{self.user.first_name} #{self.user.last_name}"
      end
    else
      "#{self.referrer_first_name} #{self.referrer_last_name}"
    end
  end

  def self.process_new(params, user)
    application = params[:application]
    passenger_count = params[:passenger_count].to_i
    commit = params[:commit]
    passengers = application.delete 'application_passengers_attributes'

    # Remove empty attachments
    unless application['attachments_attributes'].blank?
      attachments_data = application.delete 'attachments_attributes'
      attachments_data.reject! { |k,v| v['document'].blank? }
      application['attachments_attributes'] = attachments_data
    end


    app = Application.new
    if user.is_admin?
      application[:created_by_admin] = true
      app.assign_attributes application, :as => :admin
    else
      app.assign_attributes application
    end

    passenger_count.times do |x|
      passenger = app.application_passengers.new passengers[x.to_s]
      passenger.is_exception = true unless passenger.valid?
#       if passenger.valid?
#         passenger.save
#       else
#         passenger.is_exception = true and passenger.save
#         app.raise_exception!(:user => user, :exception => params[:exception_reason]) unless app.state = 'exception'
#       end
    end

    if app.valid?
      app.save
      if commit == 'Save'
        app.save_application!(:user => user)
      else
        app.submit!(:user => user)
      end
    else
      if commit == 'Save'
        app.is_exception = true
        app.save
        app.save_application!(:user => user)
      else
        app.is_exception = true
        app.save
        app.raise_exception!(:user => user, :exception => params[:exception_reason])
      end
    end

    app.application_passengers.each do |passenger|
      if passenger.valid?
        passenger.save
      else
        passenger.is_exception = true and passenger.save
        app.raise_exception!(:user => user, :exception => params[:exception_reason]) unless app.state = 'exception'
      end
    end

    app
  end

  def is_editable? user
    if user.is_admin?
      if !self.admin.blank? && self.admin != user
        false
      else
        if self.user == user
          ['new', 'saved', 'submitted', 'exception', 'reopened', 'returned'].include?(self.state) ? true : false
        else
          ['new', 'submitted', 'exception', 'reopened'].include?(self.state) ? true : false
        end
      end
    else
      if self.user_id == user.id
        ['new', 'saved', 'returned'].include?(self.state) ? true : false
      else
        false
      end
    end
  end

  def is_checked_out?
    !self.admin.blank?
  end

  def checkout!(user)
    if user.is_admin?
      self.update_attribute :admin_id, user.id
      #self.application_notes.create! :status => "Checked-out", :user => user
    end
  end

  def checkin!(user)
    if user.is_admin?
      self.update_attribute :admin_id, nil
      #self.application_notes.create! :status => "Checked-in", :user => user
    end
  end



  def persisted_passengers
    self.application_passengers.select(&:persisted?)
  end

  def persisted_outbound_flights
    self.outbound_flights.select(&:persisted?)
  end

  def persisted_return_flights
    self.return_flights.select(&:persisted?)
  end

  def set_group_defaults
    if self.group
      date_columns = Application.columns.select { |x| x.type == :date }.collect(&:name)
      self.group.fields.each do |k,v|
        if self.has_attribute? k
          if date_columns.include? k
            begin
              date = Date.parse v
            rescue
              date = nil
            end
            self.send "#{k}=", date
          else
            self.send "#{k}=", v
          end
        end
      end
    end
  end

  def set_cw_details
    self.case_worker_first_name = self.user.first_name
    self.case_worker_last_name = self.user.last_name
    self.case_worker_job_title = self.user.job_title
    self.case_worker_organization_id = self.user.organization_id
    self.case_worker_medical_center_id = self.user.medical_center_id
    self.case_worker_email = self.user.email
    self.case_worker_phone_1 = self.user.phone_1
    self.case_worker_phone_2 = self.user.phone_2
    self.case_worker_phone_3 = self.user.phone_3
  end

  def group_name
    if self.group.blank?
      '-'
    else
      self.group.name
    end
  end

  # TODO Handle exception validation gracefully for applications
  # with group templates(that have missing fields)
  def skip_validation?
    self.is_exception?
  end

  def highlight?
    if self.state == 'exception'
      true
    elsif self.departure_date && (Time.zone.now + AppSettings.first.highlight_hours.hours) > self.departure_date
      true
    else
      false
    end
  end

  def convert_to_csv
    arr = []
    a = self
    outbound = a.final_departure_date.blank? ? '' : a.final_departure_date.strftime("%-m/%-d/%Y")
    cities = ''
    if a.flight_type == 'One-way'
      ret = 'OW'
      unless a.final_depart_from_1.blank? || a.final_arrive_in_1.blank?
        cities = "#{a.final_depart_from_1.code}-#{a.final_arrive_in_1.code}"
      end
    else
      ret = a.final_return_date.blank? ? '' : a.final_return_date.strftime("%-m/%-d/%Y")
      unless a.final_depart_from_1.blank? || a.final_arrive_in_1.blank? || a.final_depart_from_2.blank? || a.final_arrive_in_2.blank?
        if a.final_depart_from_2 == a.final_arrive_in_1
          cities = "#{a.final_depart_from_1.code}-#{a.final_arrive_in_1.code}-#{a.final_arrive_in_2.code}"
        else
          cities = "#{a.final_depart_from_1.code}-#{a.final_arrive_in_1.code}-#{a.final_depart_from_2.code}-#{a.final_arrive_in_2.code}"
        end
      end
    end
    city = ''
    state = ''

    if a.application_passengers.first
      city = a.application_passengers.first.city.blank? ? '' : a.application_passengers.first.city
      state = a.application_passengers.first.state.blank? ? '' : a.application_passengers.first.state
    end

    date_booked = a.booked_at.blank? ? '' : a.booked_at.strftime("%-m/%-d/%Y")
    p_count = 1
    total_p = a.application_passengers.size
    a.application_passengers.each do |ap|
      arr << ["\"#{a.airline}\"", "\"#{a.record_locator}\"", outbound, ret, "\"#{cities}\"", date_booked,"\"#{ap.export_name}\"", "\"#{p_count.to_s}/#{total_p.to_s}\"", "\"#{ap.relation}\"", "\"#{a.medical_center_name}\"", "\"#{a.referrer_or_cw_org_name}\"", "\"#{a.refferer_or_cw_export_name}\"", a.mileage.nil?? '' : a.mileage.gsub(/\,/, "") , a.comparable_price.nil?? '' : a.comparable_price.gsub(/\,/, ""), "\"#{a.service_member_export_name}\"", "\"#{a.service_branch}\"", '', "\"#{city}\"", "\"#{state}\"", a.total_price.nil?? '' : a.total_price.gsub(/\,/, "")]
      p_count += 1
    end

    arr
  end

  # Take in a list of applications and return
  # the path of a csv tempfile
  def self.generate_passengers_csv(applications=[])
    csv_string = CSV.generate do |csv|
      csv << ["AIRLINE","RECORD", "OUTBOUND", "RETURN", "CITY PAIRS", "DATE BOOKED", "PASSENGER NAME", "#", "RELATIONSHIP", "MEDICAL CENTER", "ORGANIZATION REFERRED BY", "ORGANIZATION CONTACT", "MILEAGE", "PRICE", "SERVICE MEMBER", "BRANCH OF SERVICE", "SPECIAL SITUATIONS", "CITY", "STATE", "FEE"]
      applications.each do |a|
          arr = a.convert_to_csv
          arr.each do |ar|
            csv << ar
          end
      end
    end
    csv_string.gsub(/\"\"\"/, "\"")
  end

  def email_itinerary(file)
    # Unfortunate hack to get render_to_string working in the model
    # since rendering in the mailer has a bug:
    # https://rails.lighthouseapp.com/projects/8994/tickets/6623-render_to_string-in-mailer-causes-subsequent-render-to-fail
    emails = []
    emails << self.case_worker_email unless self.case_worker_email.blank? || (self.case_worker_email =~ /.+@.+\..+/).nil?

    emails << self.referrer_email unless self.referrer_email.blank? || (self.referrer_email =~ /.+@.+\..+/).nil?

    #emails << self.admin.email if (self.admin && self.admin.email && self.admin.receive_notifications)

    AppMailer.application_itinerary_email(emails, self, file).deliver
  end

  # Update the caseworker with the latest status
  def email_cw
    unless self.user == self.admin
      if self.user && self.user.email && self.user.is_active? && self.user.receive_notifications?
        AppMailer.email_cw(self.user, self).deliver
      end
    end
  end

  def email_admin_users
    if self.admin.blank?
      users = User.admin.active.receive_notifications
      users -= [self.user] if self.user.is_admin?
      AppMailer.email_admin_users(users, self).deliver
    else
      unless self.user == self.admin
        AppMailer.email_admin_users([self.admin], self).deliver
      end
    end
  end

  def check_for_missing_fields(reason, admin=false)
    message = "Explanation: #{reason}\n\n"

    # Case worker section - Service Member
    sm_fields_1 = [{:name=>"first_name", :label=>"First name", :admin=>false},  {:name=>"last_name", :label=>"Last name", :admin=>false}]

    sm_fields_2 = [{:name=>"service_branch", :label=>"Branch of Service", :admin=>false}, {:name=>"current_status", :label=>"Current status", :admin=>false}, {:name=>"is_inpatient", :label=>"Patient Status", :admin=>false, :bool => true},  {:name=>"injury_status", :label=>"Injury status", :admin=>false}, {:name=>"flight_type", :label=>"Flight type", :admin=>false}, {:name=>"travellers", :label=>"Who Will Be Travelling?", :admin=>false}]

    # Admin section
    admin_fields = [{:name=>"airline", :label=>"Airline", :admin=>true}, {:name=>"mileage", :label=>"Miles", :admin=>true}, {:name=>"record_locator", :label=>"Record locator", :admin=>true}, {:name=>"total_price", :label=>"Total price", :admin=>true}, {:name=>"comparable_price", :label=>"Comparable price", :admin=>true}]

    passenger_1_fields = [{:name=>"first_name", :label=>"First name", :admin=>false},  {:name=>"last_name", :label=>"Last name", :admin=>false}, {:name=>"date_of_birth", :label=>"Date of Birth", :admin=>false}, {:name=>"gender", :label=>"Gender", :admin=>false},{:name=>"relation", :label=>"Relation to Service Member", :admin=>false},{:name=>"city", :label=>"City", :admin=>false},{:name=>"state", :label=>"State", :admin=>false},{:name=>"phone_1", :label=>"Phone Number", :admin=>false},{:name=>"email", :label=>"Email", :admin=>false}]

    passenger_fields = [{:name=>"first_name", :label=>"First name", :admin=>false},  {:name=>"last_name", :label=>"Last name", :admin=>false}, {:name=>"date_of_birth", :label=>"Date of Birth", :admin=>false}, {:name=>"gender", :label=>"Gender", :admin=>false},{:name=>"relation", :label=>"Relation to Service Member", :admin=>false}]

    if admin
      message += "- Final Outbound Departure\n" if self.final_depart_from_1_id.blank?
      message += "- Final Outbound Arrival\n" if self.final_arrive_in_1_id.blank?
      message += "- Date of Departure\n" if self.final_departure_date.blank?
      message += "- Number of Outbound Flights\n" if self.outbound_flights.blank?

      self.outbound_flights.each_with_index do |o, index|
        message += "- Outbound #{index + 1} Flight #\n" if o.flight_no.blank?
        message += "- Outbound #{index + 1} Depart From\n" if o.depart_from.blank?
        message += "- Outbound #{index + 1} Departure Time\n" if o.depart_time.blank?
        message += "- Outbound #{index + 1} Arrive In\n" if o.arrive_in.blank?
        message += "- Outbound #{index + 1} Arrival Time\n" if o.arrive_time.blank?

        self.application_passengers.size.times do |n|
          if o.send("seat_#{n+1}").is_a?(String) && o.send("seat_#{n+1}").strip == ''
            message += "- Outbound #{index + 1} Seat #{n+1} is UNASSIGNED\n"
          end
        end
      end

      unless self.flight_type == 'One-way'
        message += "- Final Inbound Departure\n" if self.final_depart_from_2_id.blank?
        message += "- Final Inbound Arrival\n" if self.final_arrive_in_2_id.blank?
        message += "- Date of Return\n" if self.final_return_date.blank?
        message += "- Number of Return Flights\n" if self.return_flights.blank?

        self.return_flights.each_with_index do |o, index|
          message += "- Inbound #{index + 1} Flight #\n" if o.flight_no.blank?
          message += "- Inbound #{index + 1} Depart From\n" if o.depart_from.blank?
          message += "- Inbound #{index + 1} Departure Time\n" if o.depart_time.blank?
          message += "- Inbound #{index + 1} Arrive In\n" if o.arrive_in.blank?
          message += "- Inbound #{index + 1} Arrival Time\n" if o.arrive_time.blank?

          self.application_passengers.size.times do |n|
            if o.send("seat_#{n+1}").is_a?(String) && o.send("seat_#{n+1}").strip == ''
              message += "- Inbound #{index+1} Seat #{n+1} is UNASSIGNED\n"
            end
          end
        end
      end


      self.application_passengers.each_with_index do |ap, index|
        message += "- Passenger #{index + 1} Ticket Number\n" unless ap.is_lap_child? || !ap.ticket_no.blank?
      end

      admin_fields.each do |h|
        message += "- '#{h[:label]}'\n" if self.send(h[:name]).blank?
      end

      AppSettings.attachment_categories.select { |x| !x['cw'] && x['admin'] && x['required']}.each do |ac|
        attachment = self.admin_attachments.find_by_ref_id ac['id']
        message += "- '#{ac['name']}' attachment\n" if attachment.blank? || attachment.document.blank?
      end

    else
      missing_fields = []
      sm_fields_1.each do |h|
        if h[:bool]
          if self.send(h[:name]).nil?
            message += "- #{h[:label]}\n"
            missing_fields.push h
          end
        else
          if self.send(h[:name]).blank?
            message += "- #{h[:label]}\n"
            missing_fields.push h
          end
        end
      end

      if self.phone_1.blank? || self.phone_2.blank? || self.phone_3.blank?
        message += "- Phone Number\n"
        missing_fields.push :name => 'phone_1', :label => 'Phone Number'
      end

      sm_fields_2.each do |h|
        if h[:bool]
          if self.send(h[:name]).nil?
            message += "- #{h[:label]}\n"
            missing_fields.push h
          end
        else
          if self.send(h[:name]).blank?
            message += "- #{h[:label]}\n"
            missing_fields.push h
          end
        end
      end

      if self.application_passengers.blank?
        message += "- Total No. of Passengers\n"
        missing_fields.push :name => 'passenger_count', :label => 'Total No. of Passengers'
      end

      if self.medical_center.blank?
        unless self.dtr || self.funeral_assistance || self.memorial_service
          if ['None', nil].include?(self.medical_center_other)
            message += "- Medical Center\n"
            missing_fields.push :name => 'medical_center_id', :label => 'Medical Center'
          end
        end
      end

      if self.has_used_heromiles.nil?
        message += "- 'Has the Service Member Used Hero Miles Before?'\n"
        missing_fields.push :name => "has_used_heromiles", :label => 'Has the Service Member Used Hero Miles Before?'
      elsif self.has_used_heromiles? && self.last_use_date.blank?
        message += "- 'Date of Last Hero Miles Use'\n"
        missing_fields.push :name => "last_use_date", :label => 'Date of Last Hero Miles Use'
      end

      if self.information_collected_by.blank?
        message += "- 'Service member information was collected'\n"
        missing_fields.push :name => "information_collected_by",
                            :label => 'Service member information was collected'
      elsif self.information_collected_by == 'partly or entirely by someone else'

        referrer_fields = [{:name=>"referrer_first_name", :label=>"Referrer first name", :admin=>false}, {:name=>"referrer_last_name", :label=>"Referrer last name", :admin=>false}, {:name=>"referrer_job_title", :label=>"Referrer job title", :admin=>false}, {:name=>"referrer_email", :label=>"Referrer email", :admin=>false}, {:name=>"referrer_phone_1", :label=>"Referrer Phone Number", :admin=>false}]

        referrer_fields.each do |h|
          message += "- '#{h[:label]}'\n" if self.send(h[:name]).blank?
          missing_fields.push h
        end

      end

      if self.depart_from_1_id.blank?
        message += "- Outbound Departure\n"
        missing_fields.push :name => "depart_from_1_id", :label => 'Outbound Departure'
      end

      if self.arrive_in_1_id.blank?
        message += "- Outbound Arrival\n"
        missing_fields.push :name => "arrive_in_1_id", :label => 'Outbound Arrival'
      end

      if self.departure_date.blank?
        message += "- Requested Date of Departure\n"
        missing_fields.push :name => "departure_date", :label => 'Requested Date of Departure'
      end

      if self.flight_type == 'One-way'
        if self.flight_details.blank?
          message += "- Reason for One-way travel\n"
          missing_fields.push :name => "flight_details", :label => 'Reason for One-way travel'
        end
      else
        if self.depart_from_2_id.blank?
          message += "- Inbound Departure\n"
          missing_fields.push :name => "depart_from_2_id", :label => 'Inbound Departure'
        end

        if self.arrive_in_2_id.blank?
          message += "- Inbound Arrival\n"
          missing_fields.push :name => "arrive_in_2_id", :label => 'Inbound Arrival'
        end

        if self.return_date.blank?
          message += "- Requested Date of Return\n"
          missing_fields.push :name => "return_date", :label => 'Requested Date of Return'
        end
      end

      if self.has_special_request? && self.special_request.blank?
        message += "- Special Request\n"
        missing_fields.push :name => "special_request", :label => 'Special Request'
      end

      if self.user.is_admin?
        categories = AppSettings.attachment_categories.select { |x| x['cw'] && x['admin'] && x['required']}
      else
        categories = AppSettings.attachment_categories.select { |x| x['cw'] && !x['admin'] && x['required']}
      end
      categories.each do |ac|
        attachment = self.attachments.find_by_ref_id ac['id']
        if attachment.blank? || attachment.document.blank?
          message += "- '#{ac['name']}' attachment\n"
        end
      end

      if group
        message = ""

        required_fields = []
        other_fields = []

        missing_fields.each do |h|
          group.fields.keys.include?(h[:name]) ? required_fields << h : other_fields << h
        end

        if required_fields.blank?
          message = "#{group.name}: All template fields complete\n\n"
          unless other_fields.blank?
            message += "The following fields are excluded from the group template:\n\n"
            other_fields.each { |x| message += "- #{x[:label]}\n"}
          end
        else
          message = "The following fields are included in the group template but are incomplete:\n\n"
          required_fields.each { |x| message += "- #{x[:label]}\n"}
          unless other_fields.blank?
            message += "\nThe following fields are excluded from the group template:\n\n"
            other_fields.each { |x| message += "- #{x[:label]}\n"}
          end
        end
      end


      (self.application_passengers.size).times do |x|
        ap = self.application_passengers[x]
        if x == 0
          passenger_1_fields.each do |h|
            message += "- Passenger 1 #{h[:label]}\n" if ap.send(h[:name]).blank?
          end
        else
          passenger_fields.each do |h|
            message += "- Passenger #{x+1} #{h[:label]}\n" if ap.send(h[:name]).blank?
          end
        end
      end

    end

    message

  end

  def last_note_by_admin?
    begin
      self.application_notes.where(:is_action => false).last.user.is_admin?
    rescue
      ""
    end
  end

  def has_note?
    begin
      self.application_notes.where(:is_action => false).count > 0
    rescue
      ""
    end
  end

  protected

  # Hack to set the user so we know
  # whom to attribute an action to.
  # The user set here is picked up by event_fired.
  def set_user(params)
    @user = (params[:user].blank? ? nil : params[:user])
    @exception = params[:exception]
  end

  def set_cw_last_action(params)
    self.last_action_by_admin = false
  end

  def set_admin_last_action(params)
    self.last_action_by_admin = true
    self.last_admin_action_date = Time.zone.today
  end

  def set_completion_time(params)
    self.update_attribute :completed_at, Time.zone.now
    if self.booked_at.blank?
      self.update_attribute :booked_at, Time.zone.now
    end
  end

  def set_submission_time(params)
    self.update_attribute :submitted_at, Time.zone.now
  end

  def set_saved_time(params)
    self.update_attribute :saved_at, Time.zone.now
  end

  def set_returned_time(params)
    self.update_attribute :returned_at, Time.zone.now
  end

  def set_declined_time(params)
    self.update_attribute :declined_at, Time.zone.now
  end

  def remove_one_way_return_date
    self.return_date = nil if self.flight_type == 'One-way'
  end

  def set_admin_dates
    self.final_depart_from_1 = self.depart_from_1 if self.final_depart_from_1.blank?
    self.final_arrive_in_1 = self.arrive_in_1 if self.final_arrive_in_1.blank?
    self.final_depart_from_2 = self.depart_from_2 if self.final_depart_from_2.blank?
    self.final_arrive_in_2 = self.arrive_in_2 if self.final_arrive_in_2.blank?
    self.final_departure_date = self.departure_date if self.final_departure_date.blank?
    self.final_return_date = self.return_date if self.final_return_date.blank?
  end

  def set_default_attributes
    if self.user
      self.set_cw_details
    end
    set_admin_dates
    self.organization_id = self.user.organization_id if self.organization_id.blank?
  end

  def upcase_record_locator
    self.record_locator.upcase! unless self.record_locator.blank?
  end

  def available_medical_centers
  end
end
