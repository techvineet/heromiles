class User < ActiveRecord::Base
  extend Unscoped

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name,
                  :organization_id, :phone_1, :phone_2, :phone_3, :job_title, :other_org_name, :other_org_abbrev, :other_org_city,
                  :medical_center_id, :expires_at, :receive_notifications, :is_admin

  belongs_to :organization
  unscope :organization
  belongs_to :medical_center
  unscope :medical_center
  has_many :applications
  has_many :application_notes

  validates_presence_of :first_name, :last_name
  before_save :capitalize_account_info

  after_create :email_admins

  scope :active, where("is_active = ? and (expires_at is null or expires_at > ?)", true, Time.zone.now)
  scope :inactive, where(:is_active => false)
  scope :admin, where(:is_admin => true)
  scope :receive_notifications, where(:receive_notifications => true)

  define_index do
    indexes :email
    indexes :first_name
    indexes :last_name
    indexes :job_title
    indexes :phone_1
    indexes :phone_2
    indexes :phone_3

    indexes organization(:name), :as => :organization
    indexes medical_center(:name), :as => :medical_center

    has is_admin
    has is_active
    has organization_id
    has medical_center_id
  end

  def active_for_authentication?
    super && is_active? && !is_expired?
  end

  def inactive_message
    if !is_active?
      :not_approved
    elsif is_expired?
      :expired
    else
      super
    end
  end

  def is_expired?
    if expires_at
      Time.zone.now > expires_at
    else
      false
    end
  end

  def full_name
    "#{self.first_name} #{self.last_name}"
  end

  def account_type
    self.is_admin? ? 'Administrator' : 'Case Worker'
  end

  def activate!
    unless self.is_active?
      self.is_active = true and self.save
      AppMailer.activation_email(self).deliver
    end
  end

  def organization_name
    if self.organization.blank?
      self.other_org_name.blank? ? "" : self.other_org_name
    else
      self.organization.name
    end
  end

  def medical_center_name
    if self.medical_center.blank?
      ""
    else
      self.medical_center.name
    end
  end

  def org_or_med_center_name
    if self.organization.blank?
      if self.medical_center.blank?
        ""
      else
        self.medical_center.name
      end
    else
      self.organization.name
    end
  end

  def email_admins
    begin
      AppMailer.new_user_email(self).deliver
    rescue
      ""
    end
  end

  def notifications_count(days)
    count =[]
    count << 10
    count << self.applications.cw_notifications.where("departure_date > DATE(NOW() - INTERVAL #{days} DAY)").count
    count << self.applications.cw_notifications.where("updated_at > DATE(NOW() - INTERVAL #{days} DAY)").count
    count.max
  end

  protected

  def password_required?
    !persisted? || !password.blank? || !password_confirmation.blank?
  end

  def capitalize_account_info
    self.first_name = self.first_name.split.collect(&:capitalize).join(' ') unless self.first_name.blank?
    self.last_name = self.last_name.split.collect(&:capitalize).join(' ') unless self.last_name.blank?
    self.job_title = self.job_title.split.collect(&:capitalize).join(' ') unless self.job_title.blank?
  end

end
