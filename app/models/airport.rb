class Airport < ActiveRecord::Base
  default_scope where(:is_archived => false)

  scope :active, where(:is_archived => false)

  define_index do
    indexes :code
    indexes :name
    indexes :country
    indexes :location
    indexes :area

    has is_archived, :type => :boolean
  end

  def formatted
    if self.country != "United States"
      state_or_country = self.country
    else
      state_or_country = self.area
    end
    "#{self.code} - #{self.location}, #{state_or_country}"
  end

  def self.application_fields
    ["depart_from_1_id", "depart_from_2_id", "final_depart_from_1_id", "final_depart_from_2_id", "arrive_in_1_id", "arrive_in_2_id", "final_arrive_in_1_id", "final_arrive_in_2_id"]
  end

  def is_empty?
    query = Airport.application_fields.map { |x| "#{x} = :id"}.join ' OR '
    (Application.where(query, :id => self.id)).blank?
  end

  def self.application_airports(application_id)
    results = []
    app = Application.find_by_id application_id

    unless app.blank?
      airport_ids = application_fields.collect { |x| app.send x }
      results = self.where(:id => airport_ids).uniq
    end

    results
  end

end
