class AppMailer < ActionMailer::Base
  default from: "admin@fisherhouse.org"

  # TODO attach timestamp to files and pass filenames
  # to mailer methods to avoid race conditions.

  def activation_email(user)
    @user = user
    mail :to => @user.email,
         :subject => 'Hero Miles Account Activation'
  end

  def active_accounts_email(email)
    attachments['ActiveAccounts.pdf'] = File.read "#{Rails.root}/tmp/ActiveAccounts.pdf"
    mail :to => email,
         :subject => 'List of Active Hero Miles Accounts'
  end

  def pending_accounts_email(email)
    attachments['PendingAccounts.pdf'] = File.read "#{Rails.root}/tmp/PendingAccounts.pdf"
    mail :to => email,
         :subject => 'List of Pending Hero Miles Accounts'
  end

  def open_applications_email(email)
    attachments['OpenApplications.pdf'] = File.read "#{Rails.root}/tmp/OpenApplications.pdf"
    mail :to => email,
         :subject => 'List of Open Hero Miles Applications'
  end

  def notifications_email(email)
    attachments['Notifications.pdf'] = File.read "#{Rails.root}/tmp/Notifications.pdf"
    mail :to => email,
         :subject => 'List of Hero Miles Notifications'
  end

  # TODO Customize subject to display group name.
  def group_applications_email(email)
    attachments['GroupApplications.pdf'] = File.read "#{Rails.root}/tmp/GroupApplications.pdf"
    mail :to => email,
         :subject => 'List of Group Applications'
  end

  def application_export_email(email_ids, files)
    files.each { |name,file| attachments[name] = file.read}

    mail :to => email_ids,
         :subject => 'Hero Miles Application Export'
  end

  def application_itinerary_email(email_ids, application, file)
    @application = application
    attachments["HeroMilesItinerary-#{@application.record_locator}.pdf"] = file.read

    mail :to => email_ids, :subject => 'Hero Miles Application Itinerary'

  end

  def report_email(email_ids, files, subject)
    files.each { |name,path| attachments[name] = File.read(path)}

    mail :to => email_ids,
         :subject => 'Hero Miles Application Export'
  end

  def email_cw(user, application)
    @user = user
    @application = application

    mail :to => @user.email,
         :subject => 'Hero Miles Application Status'
  end

  def email_admin_users(users, application)
    @application = application

    mail :to => users.collect(&:email),
         :subject => 'Hero Miles Application Status'
  end

  def email_note_to_admin(emails, note)
    @note = note
    mail :to => emails,
         :subject => "Note added to Hero Miles Application"
  end

  def email_note_to_cw(note)
    @note = note

    mail :to => @note.application.user.email,
         :subject => "Note added to Hero Miles Application"
  end

  def email_notice_change(user, text, emails)
    @text = text
    @user = user

    mail :to => 'heromiles@fisherhouse.org',
         :bcc => emails,
         :subject => "Hero Miles system announcement updated"
  end

  def new_user_email(user)
    emails = User.admin.active.receive_notifications.collect(&:email)
    @user = user

    mail :to => emails,
         :subject => "A new user '#{@user.full_name}' has signed up"
  end

  def suspend_user_email(user)
    emails = User.admin.active.receive_notifications.collect(&:email)
    emails << user.email
    emails.uniq!
    mail :to => emails,
         :subject => "Hero Miles Account Security",
         :body => "The user account with login email #{user.email} has temporarily been set to an expired status because of 5 incorrect password entries. An administrator can make the account active again, and reset the password if needed."
  end

end
