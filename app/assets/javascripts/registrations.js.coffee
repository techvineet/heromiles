# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  if $('#registration-form').length

    $('select#user_organization_id').change ->
      if $(this).val() == 'Other' || $(this).val() == ''
        $('.other-org').removeClass('hidden').slideDown()
      else
        if $('.other-org').is(':visible')
          $('.other-org').slideUp()
          $('.other-org input').each ->
            $(this).val('')
