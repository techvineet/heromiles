$ ->

  unless $('.alert').html().replace(/\s+/,'') == ''
    $('.alert').removeClass('hidden').slideDown()

  unless $('.success').html().replace(/\s+/,'') == ''
    if $('.success').html().replace(/^\s+|\s+$/g, '')=="Thank you. You will be notified when your account is approved."
      $('.success').removeClass('hidden').slideDown()
    else
      $('.success').removeClass('hidden').slideDown().delay(5000).slideToggle()

  if $('form.validate').length
    #$("form.validate").validationEngine()
    $("form.validate").ttValidation()

  if $('select.select_autocomplete').length
    $('select.select_autocomplete').select_autocomplete()

  if $('.capitalize').length
    $('.capitalize').live 'blur', ->
      val = $(this).val().charAt(0).toUpperCase() + $(this).val().slice(1)
      $(this).val val

  if $('.sign_up_button').length
    unless $('.alert').hasClass('hidden') && $('.success').hasClass('hidden')
      $('.sign_up_button').css("margin-top", "35px")