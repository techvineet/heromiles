# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  $('.view-all-link').click ->
    $('#search_applications').submit()

  $('td.sorttable').click ->
    text = $(this).html()
    reverse = false
    text = text.replace(/^\s+|\s+$/g,"")
    if text.charCodeAt(text.length-1) == 9660
      reverse = true
    if text.charCodeAt(text.length-1) == 9660 || text.charCodeAt(text.length-1) == 9650
      text = text.substring(0, text.length-1)
    $('<input>').attr({type: 'hidden', id: 'sort',name: 'sort', value: text}).appendTo('form#search_applications')
    $('<input>').attr({type: 'hidden', id: 'reverse',name: 'reverse', value: reverse}).appendTo('form#search_applications')
    $('#search_applications').submit()

  $("#advanced-search-link").click (e) ->
    e.preventDefault()
    if $("#advanced_search").hasClass 'hidden'
      $("#advanced_search").removeClass('hidden').slideDown()
    else
      $("#advanced_search").removeClass('hidden').slideToggle()

  $("#search_status input:checkbox").change (e) ->
    status = []
    $("#search_status :checked").each ->
      status.push $("label[for='#{$(this).attr('id')}']").text()

    $("#status").val JSON.stringify(status)

  $("#search_restrict input:checkbox").change (e) ->
    restrict = []
    $("#search_restrict :checked").each ->
      restrict.push $("label[for='#{$(this).attr('id')}']").text()

    $("#restrict").val JSON.stringify(restrict)

  $("#search_dates select").change ->
    from = ""
    till = ""
    unless $("_search_from_2i").val() == '' || $("_search_from_1i").val() == '' || $("_search_from_3i").val() == ''
      from = "#{$("_search_from_1i").val()}-#{$("_search_from_2i").val()}-#{$("_search_from_1i").val()}"

    unless $("_search_till_2i").val() == '' || $("_search_till_1i").val() == '' || $("_search_till_3i").val() == ''
      from = "#{$("_search_till_1i").val()}-#{$("_search_till_2i").val()}-#{$("_search_till_1i").val()}"

    $("#from").val(from)
    $("#till").val(till)

  if $("#export-search-dialog").length
    $('#export-search-dialog').dialog {
      autoOpen: false,
      width: 600,
      modal: true,
      title: 'Print/Export Search Results'
    }

    $('#export-search-link').click (e) ->
      e.preventDefault()
      $('#export-search-dialog').dialog 'open'

    $('#view-export-link, #save-export-link, #save-all-data-link').click (e) ->
      e.preventDefault()
      $form = $('form').clone()
      $form.attr 'id', 'export-form'
      $form.addClass 'export-form hidden'
      if $(this).attr('id') == 'save-all-data-link'
        $form.prepend "<input type='hidden' value='csv' name='format' />"
      else
        $form.prepend "<input type='hidden' value='pdf' name='format' />"

      if $(this).attr('id') == 'view-export-link'
        $form.prepend "<input type='hidden' value='true' name='view' />"
      else
        $form.prepend "<input type='hidden' value='true' name='save' />"

      $('#export-search-dialog .export-form').replaceWith $form

      $('#export-search-dialog .export-form').submit()

      $('#export-search-dialog').dialog 'close'

