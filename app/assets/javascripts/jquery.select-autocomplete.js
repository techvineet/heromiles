(function($) {

	$.fn.select_autocomplete = function(options) {

		// make sure we have an options object
		options = options || {};

		// setup our defaults
		var defaults = {
			  minLength: 1
		};

		options = $.extend(defaults, options);

		return this.each(function() {

            if($(this).attr("id") == "application_medical_center_id"){
                var $this = $(this),
                    data = [],
                    $input = $('<input type="text" class="validate[required]" />');
            }
            else{
                var $this = $(this),
                    data = [],
                    $input = $('<input type="text" />');
            }


			//stick each of it's options in to an items array of objects with name and value attributes

			if (this.tagName.toLowerCase() != 'select') { return; }

			$this.children('option').each(function() {

				var $option = $(this);

				if ($option.val() != '') { //ignore empty value options

					data.push({
						  label: $option.html()
						, id : $option.val()
					});
				}
			});

			// insert the input after the select
			$this.after($input);

			// add it our data
			//options.data = data;

                        options.source = data;

                        options.select = function(event, ui) {
                          $this.find('option').removeAttr('selected');
                          $($this.find('option[value='+ ui.item.id + ']')[0]).attr('selected', true);
                          $this.trigger('change');
                        };

                        options.change = function(event, ui) {
                          if(!ui.item) {
                            if($(this).val().replace(/\s+/,'') === '') {
                              $this.val('');
                            } else {
                              $(this).val($this[0].options[$this[0].selectedIndex].text);
                            }
                          }
                        };

			//make the input box into an autocomplete for the select items
			$input.autocomplete(options);


                        // Hack to set input width based on rails date attribute names
                        if($this.attr('id').match(/_1i$/) != null) {
                          $input.addClass('ten-chars')
                        } else if ($this.attr('id').match(/_3i$/) != null) {
                          $input.addClass('five-chars')
                        }

                        //set the initial text value of the autocomplete input box to the text node of the selected item in the select control
                        $input.val($this[0].options[$this[0].selectedIndex].text);
                        $this.trigger('change');

                        //normally, you'd hide the select list but we won't for this demo
                        $this.hide();
                });
        };


})(jQuery);
