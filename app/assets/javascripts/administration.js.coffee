# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  $('.button').button()

  if $("#applications").length
    $("#export-open-applications").click ->
      $("#export-open-dialog").dialog 'open'

    $("#export-notifications").click ->
      $("#export-notifications-dialog").dialog 'open'

    $("#export-open-dialog").dialog {
      title: 'Open Applications - Print/Export Options',
      width: 570,
      modal: true,
      autoOpen: false
    }

    $("#export-notifications-dialog").dialog {
      title: 'Notifications - Print/Export Options',
      width: 570,
      modal: true,
      autoOpen: false
    }

    window.refreshApplications = ->
      if $("#open-applications").hasClass 'admin'
        $.getScript '/administration/applications/open'
        #$.getScript '/administration/applications/notifications'
      else
        $.getScript '/case_worker/applications/open'
        $.getScript '/case_worker/applications/notifications'

    window.setInterval 'refreshApplications()', 60000

  if $("#manage-accounts").length

    $("#export-pending-accounts").click ->
      $("#export-pending-dialog").dialog 'open'

    $("#export-active-accounts").click ->
      $("#export-active-dialog").dialog 'open'

    $("#export-pending-dialog").dialog {
      title: 'Pending accounts - Print/Export Options',
      width: 570,
      modal: true,
      autoOpen: false
    }

    $("#export-active-dialog").dialog {
      title: 'Accounts - Print/Export Options',
      width: 570,
      modal: true,
      autoOpen: false
    }

  if $("#applications").length

    $('#update-notice').click ->
      if $('.edit-notice').hasClass 'hidden'
        $('.edit-notice').removeClass('hidden').slideDown()
        $('#update-notice .ui-button-text').html 'Cancel'
      else
        $('.edit-notice').slideUp ->
          $(this).addClass 'hidden'
        $('#update-notice .ui-button-text').html 'Update'

    $('#checkout-dialog').dialog {
      title: 'Check Out Application',
      width: 350,
      modal: true,
      autoOpen: false
    }

    $('a.view-open').live 'click', (e) ->
      $tr = $(this).parents('tr')
      id = $tr.find('.id').html()
      if $tr.hasClass 'taken'
        #admin = $tr.find('.admin').html()
        #$("#checkout-dialog .admin-name").html admin
        #$('.checkout-button').attr 'href', "/applications/#{id}/checkin"
        window.location = "/administration/applications/#{id}?checkout=false"
        #$("#checkout-dialog").dialog 'open'
        e.preventDefault()


  if $("#application-form").length
    $("#application-tabs").tabs()

  if $("#manage-organizations").length
    resetFields =  ->
      $upd = $("#update_organization")
      $upd.find("#organization_abbreviation").val ''
      $upd.find("#organization_city").val ''
      $upd.find("#organization_state").val ''
      $upd.find("#organization_is_archived").removeAttr 'checked'

    resetFields()

    $("select#name").change ->
      i = $(this).val()
      $upd = $("#update_organization")
      if organizations[i]
        $upd.find("#organization_abbreviation").val organizations[i].abbrv
        $upd.find("#organization_city").val organizations[i].city
        $upd.find("#organization_state").val organizations[i].state
        if organizations[i].is_archived
          $upd.find("#organization_is_archived").attr 'checked', 'checked'
        if organizations[i].is_empty
          $upd.find("#destroy_link").show()
        else
          $upd.find("#destroy_link").hide()
      else
        resetFields()

    $("#destroy_link").click (e) ->
      e.preventDefault()
      $form = $("#update_organization form")
      id = $("#update_organization #name").val()
      $form.attr 'action', "/organizations/#{id}"
      $form.attr 'method', 'post'
      $form.find("input[name='_method']").val 'delete'
      $form.submit()

    $("#update_link").click (e) ->
      e.preventDefault()
      $form = $("#update_organization form")
      $form.attr 'method', 'post'
      id = $("#update_organization #name").val()
      $form.attr 'action', "/organizations/#{id}"
      $form.find("input[name='_method']").val 'put'
      $form.submit()

  if $("#manage-medical-centers").length
    resetFields =  ->
      $upd = $("#update_medical_center")
      $upd.find("#medical_center_abbreviation").val ''
      $upd.find("#medical_center_city").val ''
      $upd.find("#medical_center_state").val ''
      $upd.find("#medical_center_is_archived").removeAttr 'checked'

    resetFields()

    $("select#name").change ->
      i = $(this).val()
      $upd = $("#update_medical_center")
      if medical_centers[i]
        $upd.find("#medical_center_abbreviation").val medical_centers[i].abbrv
        $upd.find("#medical_center_city").val medical_centers[i].city
        $upd.find("#medical_center_state").val medical_centers[i].state
        if medical_centers[i].is_archived
          $upd.find("#medical_center_is_archived").attr 'checked', 'checked'
        if medical_centers[i].is_empty
          $upd.find("#destroy_link").show()
        else
          $upd.find("#destroy_link").hide()
      else
        resetFields()

    $("#destroy_link").click (e) ->
      e.preventDefault()
      $form = $("#update_medical_center form")
      id = $("#update_medical_center #name").val()
      $form.attr 'action', "/medical_centers/#{id}"
      $form.attr 'method', 'post'
      $form.find("input[name='_method']").val 'delete'
      $form.submit()

    $("#update_link").click (e) ->
      e.preventDefault()
      $form = $("#update_medical_center form")
      id = $("#update_medical_center #name").val()
      $form.attr 'action', "/medical_centers/#{id}"
      $form.attr 'method', 'post'
      $form.find("input[name='_method']").val 'put'
      $form.submit()


  if $("#manage-airlines").length
    resetFields =  ->
      $upd = $("#update_airline")
      $upd.find("#airline_units").val ''
      $upd.find("#airline_conversion").val ''
      $upd.find("#airline_is_archived").removeAttr 'checked'

    resetFields()

    $("select#name").change ->
      i = $(this).val()
      $upd = $("#update_airline")
      if airlines[i]
        $upd.find("#airline_units").val airlines[i].units
        $upd.find("#airline_conversion").val airlines[i].conversion
        if airlines[i].is_archived
          $upd.find("#airline_is_archived").attr 'checked', 'checked'

      else
        resetFields()

    $("#destroy_link").click (e) ->
      e.preventDefault()
      $form = $("#update_airline form")
      id = $("#update_airline #name").val()
      $form.attr 'action', "/airlines/#{id}"
      $form.attr 'method', 'post'
      $form.find("input[name='_method']").val 'delete'
      $form.submit()

    $("#update_link").click (e) ->
      e.preventDefault()
      $form = $("#update_airline form")
      id = $("#update_airline #name").val()
      $form.attr 'method', 'post'
      $form.attr 'action', "/airlines/#{id}"
      $form.find("input[name='_method']").val 'put'
      $form.submit()

  if $("#new_group").length || $("#edit-group").length
    $("#submit-group").click (e) ->
      e.preventDefault()

      fields = {}
      $('.template_check_box:checked').each ->
        $parent = $(this).parent()
        val = ''

        $parent.find('input:checkbox:not(".template_check_box")').each ->
          if $(this).is(':checked')
            val = 'true'

          fields[$(this).attr('name').replace(/(application\[|\]|\(.+\))/gi,'')] = val

        $parent.find('input:radio').each ->
          if $(this).is(':checked')
            val = $(this).val()

          fields[$(this).attr('name').replace(/(application\[|\]|\(.+\))/gi,'')] = val

        $parent.find('input:text, input[type=email], select:not(".date-select"),textarea, input:hidden.template').each ->
          fields[$(this).attr('name').replace(/(application\[|\])/gi,'')] = $(this).val()

        if $parent.find('.date-select').length
          name = $(this).attr('name').replace(/(application\[|\]|\(.+\))/gi,'')
          month = $parent.find("select[name*='#{name}(2i)']").val()
          day = $parent.find("select[name*='#{name}(3i)']").val()
          year = $parent.find("select[name*='#{name}(1i)']").val()
          unless day
            day = '01'
          if year && month
            date = "#{year}-#{month}-#{day}"
          else
            date = ""
          fields[$(this).attr('name').replace(/(application\[|\]|\(.+\))/gi,'')] = date

      $("#group_fields_data").val JSON.stringify(fields)
      $("#form form").submit()

  if $("#show-group").length
    $("#export-applications").click (e) ->
      e.preventDefault()
      $("#export-applications-dialog").dialog 'open'

    $("#export-applications-dialog").dialog {
      autoOpen: false,
      modal: true,
      title: 'Group Applications - Open/Export options',
      width: 570
    }

  if $("#reports").length

    $("#upload-blank-link").click (e) ->
      e.preventDefault()

      $("#upload-blank-dialog").dialog 'open'

    $("#summary-link").click (e) ->
      e.preventDefault()

      $("#summary-dialog").dialog 'open'

    $("#monthly-link").click (e) ->
      e.preventDefault()

      $("#monthly-dialog").dialog 'open'

    $("#branch-link").click (e) ->
      e.preventDefault()

      $("#branch-dialog").dialog 'open'

    $("#summary-dialog").dialog {
      title: 'Summary Report',
      width: 570,
      autoOpen: false,
      modal: true
    }

    $("#monthly-dialog").dialog {
      title: 'Monthly Report',
      width: 570,
      autoOpen: false,
      modal: true
    }

    $("#branch-dialog").dialog {
      title: 'Branch/Airline Reports',
      width: 570,
      autoOpen: false,
      modal: true
    }

    $("#upload-blank-dialog").dialog {
      title: 'Upload Blank Application',
      width: 570,
      autoOpen: false,
      modal: true
    }

  if $("#account-form").length
    $('select#user_organization_id').change ->
      if $(this).val() == 'Other' || $(this).val() == ''
        $('.other-org').removeClass('hidden').slideDown()
      else
        if $('.other-org').is(':visible')
          $('.other-org').slideUp()
          $('.other-org input').each ->
            $(this).val('')

  if $("#manage-groups").length
    $("#display_archived").change ->
      if $(this).is ':checked'
        $("tr.archived").each ->
          $(this).removeClass('hidden').fadeIn()
      else
        $("tr.archived").each ->
          $(this).fadeOut()

  if $("#update_attachment").length
    $('select#category_id').change ->
     if $(this).val() == ''
      $('#is_required, #is_admin, #is_cw').removeAttr 'checked'
     else
      cat = attachmentCategories[$(this).val()]
      if cat['required']
        $('#update_attachment #is_required').val 'true'
      else
        $('#update_attachment #is_required').val 'false'

      if cat['admin'] && cat['cw']
        $('#update_attachment #display_on').val 'cw_admin'
      else if cat['admin']
        $('#update_attachment #display_on').val 'admin'
      else
        $('#update_attachment #display_on').val 'cw'

      if cat['is_archived']
        $('#update_attachment #is_archived').attr 'checked', 'checked'
      else
        $('#update_attachment #is_archived').removeAttr 'checked'

      if cat['is_empty']
        $('#update_attachment #destroy_link').removeClass('hidden').show()
      else
        $('#update_attachment #destroy_link').hide()

    $("#update_attachment #destroy_link").click (e) ->
      e.preventDefault()
      cat = attachmentCategories[$('#update_attachment #category_id').val()]
      if cat && cat['id']
        $form = $("#update_attachment form")
        $form.attr 'action', "/app_settings/remove_category"
        $form.attr 'method', 'post'
        $form.find("input[name='_method']").val 'delete'
        $form.submit()

  if $('#summary-report, #branch-report, #monthly-report').length

    $('#mail-report-link').click (e) ->
      e.preventDefault()

      $('#mail-report-dialog').dialog 'open'

    $('#mail-report-dialog').dialog {
      title: 'Report - Print/Export Options',
      width: 570,
      modal: true,
      autoOpen: false
    }

  if $("#manage-airports").length
    resetFields =  ->
      $upd = $("#update_airport")
      $upd.find("input:text").val ''
      $upd.find('input:checkbox').removeAttr 'checked'


    resetFields()


    $("#destroy_link").click (e) ->
      e.preventDefault()
      $form = $("#update_airport form")
      $form.attr 'action', "/airports/#{window.selectedAirport}"
      $form.attr 'method', 'post'
      $form.find("input[name='_method']").val 'delete'
      if window.selectedAirport
        $form.submit()

    $("#update_link").click (e) ->
      e.preventDefault()
      $form = $("#update_airport form")
      $form.attr 'action', "/airports/#{window.selectedAirport}"
      $form.find("input[name='_method']").val 'put'
      $form.attr 'method', 'post'
      if window.selectedAirport
        $form.submit()

  if $("#assign_to").length
    $("#assign_to").change ->
      id = $(this).val()
      url = $("#assign_button").attr 'href'
      new_url = url.replace /\?user=.+$/, ''
      new_url += "?user=#{id}"
      $("#assign_button").attr 'href', new_url

  if $('#show-conversions').length
    $('#show-conversions').click (e) ->
      e.preventDefault()
      if $('#reward-conversions').is(':visible')
        $('#reward-conversions').slideUp()
      else
        $('#reward-conversions').removeClass('hidden').slideDown()

  if $('.time-select').length
    $('.time-select select').change ->
      hour = $(this).parent().find('.hour').val()
      minute = $(this).parent().find('.minute').val()
      ampm = $(this).parent().find('.ampm').val()
      $(this).siblings('input:hidden').val "#{hour}:#{minute} #{ampm}"

  $.fn.autoGrowInput = (o) ->
    o = $.extend(
      maxWidth: 460
      minWidth: 0
      comfortZone: 10
      , o)
    minWidth = o.minWidth or $(this).width()
    val = ""
    input = $(this)
    testSubject = $("<tester/>").css(
      position: "absolute"
      top: -9999
      left: -9999
      width: "auto"
      fontSize: input.css("fontSize")
      fontFamily: input.css("fontFamily")
      fontWeight: input.css("fontWeight")
      letterSpacing: input.css("letterSpacing")
      whiteSpace: "nowrap"
    )
    check = ->
      return  if val is (val = input.val())
      escaped = val.replace(/&/g, "&amp;").replace(/\s/g, "&nbsp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
      testSubject.html escaped
      testerWidth = testSubject.width()
      newWidth = (if (testerWidth + o.comfortZone) >= minWidth then testerWidth + o.comfortZone else minWidth)
      currentWidth = input.width()
      isValidWidthChange = (newWidth < currentWidth and newWidth >= minWidth) or (newWidth > minWidth and newWidth < o.maxWidth)
      input.width newWidth  if isValidWidthChange

    testSubject.insertAfter input
    $(this).bind "keyup keydown blur update", check

  $('#new_organization #organization_name').autoGrowInput()
  $('#new_medical_center #medical_center_name').autoGrowInput()
  $('#new_airport #airport_name').autoGrowInput()
