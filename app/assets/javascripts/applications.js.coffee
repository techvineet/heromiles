# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  if $("#no-back .hidden").length
    window.history.forward()

  if $("#application-form").length

    # Prevent enter from submitting form
    $(document).keypress (e) ->
      if e.keyCode == 13
        e.preventDefault()

    $('select#application_information_collected_by').change ->
      if $(this).val() == 'partly or entirely by someone else'
        $('.referrer').removeClass('hidden').slideDown()
      else
        $('.referrer').slideUp ->
          $(this).addClass 'hidden'

    $(".passenger-fname, .passenger-mname, .passenger-lname").change ->
      $parent = $(this).parents('.passenger')
      id = $parent.attr('id').replace('passenger-','')
      legal_name = "#{$parent.find('.passenger-fname').val()} #{$parent.find('.passenger-mname').val()} #{$parent.find('.passenger-lname').val()}"
      $("#admin-passenger-#{id} .legal_name").html legal_name

    $(".passenger .dob select").change ->
      id = $(this).parents('.passenger').attr('id').replace('passenger-','')
      parent = $(this).parents('.dob')
      text = ""
      parent.find('select').each ->
        text += "#{$(this).val()}/"

      text = text.replace(/\/$/,'')
      $("#admin-passenger-#{id} .dob").html text
      date = new Date(text)

      # If date of birth is less than 2 years ago
      # Show lap child fields
      # else hide them, if not already hidden

      if (new Date() - date) < 1000 * 60 * 60 * 24 * 365 * 2
        parent.siblings('p.lap_child').slideDown().removeClass 'hidden'
        parent.siblings('p.lap_child').find('select').val 'true'
      else
        parent.siblings('p.lap_child').slideUp().addClass 'hidden'
        parent.siblings('p.lap_child').find('select').val 'false'

    $(".passenger .gender select").change ->
      id = $(this).parents('.passenger').attr('id').replace('passenger-','')
      text = $(this).val()
      $("#admin-passenger-#{id} .gender").html text

    $(".passenger .city input:text").change ->
      id = $(this).parents('.passenger').attr('id').replace('passenger-','')
      text = $(this).val()
      $("#admin-passenger-#{id} .city").html text

    $(".passenger .state select").change ->
      id = $(this).parents('.passenger').attr('id').replace('passenger-','')
      text = $(this).val()
      $("#admin-passenger-#{id} .state").html text

    $(".passenger .phone input:text").change ->
      id = $(this).parents('.passenger').attr('id').replace('passenger-','')
      fields = $(this).parents('.phone').find('input:text')
      text = "(#{$(fields[0]).val()}) #{$(fields[1]).val()}-#{$(fields[2]).val()}"
      $("#admin-passenger-#{id} .phone").html text

    $(".passenger .email input").change ->
      id = $(this).parents('.passenger').attr('id').replace('passenger-','')
      text = $(this).val()
      $("#admin-passenger-#{id} .email").html text

    $("#application_first_name").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#passenger-1 .passenger-fname").val($(this).val()).trigger('change')

    $("#application_middle_name").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#passenger-1 .passenger-mname").val($(this).val()).trigger('change')

    $("#application_last_name").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#passenger-1 .passenger-lname").val($(this).val()).trigger('change')


    $("#application_phone_1").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#application_application_passengers_attributes_0_phone_1").val($(this).val()).trigger('change')

    $("#application_phone_2").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#application_application_passengers_attributes_0_phone_2").val($(this).val()).trigger('change')

    $("#application_phone_3").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#application_application_passengers_attributes_0_phone_3").val($(this).val()).trigger('change')

    $("#application_email").change ->
      if $("#passenger-1 .header").html().replace(/\s+/,'') == "Passenger1 Information (Service Member)"
        $("#application_application_passengers_attributes_0_email").val($(this).val()).trigger('change')

    $("input[name='application[travellers]']").click ->
      if $(this).val() == "Service Member"
        $("#passenger-count").slideUp ->
          $(this).addClass('hidden')
        $("#passenger-1 .header").html 'Passenger 1 Information (Service Member)'
        $("#passenger-1 .passenger-fname").val($("#application_first_name").val())
        $("#passenger-1 .passenger-lname").val($("#application_last_name").val())
        $("#passenger-1 .passenger-mname").val($("#application_middle_name").val())
        $("#passenger-1 .relation").val('Self')

        $("#application_application_passengers_attributes_0_email").val($("#application_email").val())

        $("#application_application_passengers_attributes_0_phone_1").val($("#application_phone_1").val())
        $("#application_application_passengers_attributes_0_phone_2").val($("#application_phone_2").val())
        $("#application_application_passengers_attributes_0_phone_3").val($("#application_phone_3").val())

        $("select#passenger_count").val(1).trigger('change')
      else if $(this).val() == "Family/Friend"
        $("#passenger-1 .header").html 'Passenger 1 Information'

        $("#passenger-1 .passenger-fname").val('')
        $("#passenger-1 .passenger-lname").val('')
        $("#passenger-1 .passenger-mname").val('')
        $("#passenger-1 .relation").val('')

        $("#application_application_passengers_attributes_0_email").val('')

        $("#application_application_passengers_attributes_0_phone_1").val('')
        $("#application_application_passengers_attributes_0_phone_2").val('')
        $("#application_application_passengers_attributes_0_phone_3").val('')


        if $("#passenger-count").hasClass('hidden')
          $("#passenger-count").removeClass('hidden').slideDown()
      else if $(this).val() == "Both"
        $("#passenger-1 .header").html 'Passenger 1 Information (Service Member)'
        $("#passenger-1 .passenger-fname").val($("#application_first_name").val())
        $("#passenger-1 .passenger-lname").val($("#application_last_name").val())
        $("#passenger-1 .passenger-mname").val($("#application_middle_name").val())
        $("#passenger-1 .relation").val('Self')
        $("#passenger_count option").removeAttr 'selected'
        $("#passenger_count option[value='2']").attr 'selected', 'selected'

        $("#application_application_passengers_attributes_0_email").val($("#application_email").val())

        $("#application_application_passengers_attributes_0_phone_1").val($("#application_phone_1").val())
        $("#application_application_passengers_attributes_0_phone_2").val($("#application_phone_2").val())
        $("#application_application_passengers_attributes_0_phone_3").val($("#application_phone_3").val())


        if $("#passenger-count").hasClass('hidden')
          $("#passenger-count").removeClass('hidden').slideDown()

        val = $("#passenger_count").val()
        maxVal = $("#passenger_count").find('option:last').val()
        if parseInt(val)
          for i in [1..(maxVal)]
            if val >= i
              if $("#passenger-#{i}").hasClass 'hidden'
                $("#passenger-#{i}").removeClass('hidden').slideDown()
                $("#passenger-#{i}").find('.destroy-passenger').val('false')
                $("#admin-passenger-#{i}").removeClass('hidden')
                $("#passenger-seat-#{i}").removeClass('hidden')
            else
              unless $("#passenger-#{i}").hasClass 'hidden'
                $("#admin-passenger-#{i}").addClass('hidden')
                $("#passenger-seat-#{i}").removeClass('hidden')
                $("#passenger-#{i}").slideUp ->
                  $(this).addClass 'hidden'
                  $(this).find('.destroy-passenger').val('true')


    unless $('select#passenger_count').is(':visible') || $('#application_travellers_service_member').is(':checked')
      $("select#passenger_count").val('')

    $("select#passenger_count").change ->
      val = parseInt($(this).val())
      maxVal = $(this).find('option:last').val()
      if parseInt(val)
        for i in [1..(maxVal)]
          if val >= i
            if $("#passenger-#{i}").hasClass 'hidden'
              $("#passenger-#{i}").removeClass('hidden').slideDown()
              $("#passenger-#{i}").find('.destroy-passenger').val('false')
              $("#admin-passenger-#{i}").removeClass('hidden')
              $("#passenger-seat-#{i}").removeClass('hidden')
          else
            unless $("#passenger-#{i}").hasClass 'hidden'
              $("#admin-passenger-#{i}").addClass('hidden')
              $("#passenger-seat-#{i}").removeClass('hidden')
              $("#passenger-#{i}").slideUp ->
                $(this).addClass 'hidden'
                $(this).find('.destroy-passenger').val('true')

      else
        $('.passenger').each ->
          $(this).slideUp ->
            $(this).addClass 'hidden'
            $(this).find('.destroy-passenger').val('true')
        $('.admin-passenger').addClass 'hidden'
        $(".passenger-seat").removeClass('hidden')

    $('.has_special_request').click ->
      if $(this).is(':checked')
        $('.special_request').removeClass('hidden').slideDown()
      else
        $('.special_request').slideUp()

    $("input[name='application[has_used_heromiles]']").change ->
      if $(this).val() == 'true'
        $('p.last_use_date').removeClass('hidden').slideDown()
      else
        $('p.last_use_date').slideUp()

    $("input[name='application[flight_type]']").change ->
      if $(this).val() == 'One-way'
        $('.return-date').slideUp()
        $('.return-airport').slideUp()
        $('.one_way_reason').removeClass('hidden').slideDown()
      else
        $('.return-date').removeClass('hidden').slideDown()
        $('.return-airport').removeClass('hidden').slideDown()
        if $('.one_way_reason').is(':visible')
          $('.one_way_reason').slideUp ->
            $(this).addClass 'hidden'


    if $('#exception-dialog').length
      listExceptions = ->
        $('#exception-list').html ''
        $('.validation-failed > label:not(".radio-label"):not(".dummy-label")').each ->
          $('#exception-list').append "<li>- #{$(this).text().replace(':', '')}</li>"

      validateFieldsFormat = ->
        errorFields = []
        $('.validation-failed').removeClass 'validation-failed'

        unless ($('.referrer_phone_1').is(':hidden')) ||
          ($('.referrer_phone_1').val().match(/\d{3}/) &&
          $('.referrer_phone_2').val().match(/\d{3}/) &&
          $('.referrer_phone_3').val().match(/\d{4}/)) ||
          ($('.referrer_phone_1').val().match(/^$/) &&
          $('.referrer_phone_2').val().match(/^$/) &&
          $('.referrer_phone_3').val().match(/^$/))
            errorFields.push 'Referrer Phone Number'
            $('.referrer_phone_1').parent().addClass('validation-failed')

        unless ($('.referrer_email').is(':hidden')) ||
          ($('.referrer_email').val().match(///^[A-Za-z0-9._%+-]+
          @[A-Za-z0-9.-]+\.[A-Za-z]+$///) ||
          ($('.referrer_email').val().match(/^$/)))
            errorFields.push 'Referrer Email Address'
            $('.referrer_email').parent().addClass('validation-failed')

        unless ($('#application_phone_1').val().match(/\d{3}/) &&
          $('#application_phone_2').val().match(/\d{3}/) &&
          $('#application_phone_3').val().match(/\d{4}/)) ||
          ($('#application_phone_1').val().match(/^$/) &&
          $('#application_phone_2').val().match(/^$/) &&
          $('#application_phone_3').val().match(/^$/))
            errorFields.push 'Phone Number'
            $('#application_phone_1').parent().addClass('validation-failed')

        unless ($('#application_alt_phone_1').val().match(/\d{3}/) &&
          $('#application_alt_phone_2').val().match(/\d{3}/) &&
          $('#application_alt_phone_3').val().match(/\d{4}/)) ||
          ($('#application_alt_phone_1').val().match(/^$/) &&
          $('#application_alt_phone_2').val().match(/^$/) &&
          $('#application_alt_phone_3').val().match(/^$/))
            errorFields.push 'Alternate Number'
            $('#application_alt_phone_1').parent().addClass('validation-failed')

        unless ($('#application_email').val().match(///^[A-Za-z0-9._%+-]+
          @[A-Za-z0-9.-]+\.[A-Za-z]+$///) ||
          ($('#application_email').val().match(/^$/)))
            errorFields.push 'Email Address'
            $('#application_email').parent().addClass('validation-failed')


        unless ($('.passenger_1_phone_1').is(':hidden')) ||
          ($('.passenger_1_phone_1').val().match(/\d{3}/) &&
          $('.passenger_1_phone_2').val().match(/\d{3}/) &&
          $('.passenger_1_phone_3').val().match(/\d{4}/)) ||
          ($('.passenger_1_phone_1').val().match(/^$/) &&
          $('.passenger_1_phone_2').val().match(/^$/) &&
          $('.passenger_1_phone_3').val().match(/^$/))
            errorFields.push 'Passenger 1 Phone Number'
            $('.passenger_1_phone_1').parent().addClass('validation-failed')


        unless ($('.passenger_1_email').is(':hidden')) ||
          ($('.passenger_1_email').val().match(///^[A-Za-z0-9._%+-]+
          @[A-Za-z0-9.-]+\.[A-Za-z]+$///) ||
          ($('.passenger_1_email').val().match(/^$/)))
            errorFields.push 'Passenger 1 Email Address'
            $('.passenger_1_email').parent().addClass('validation-failed')

        if errorFields.length == 0
          $('.alert').html('').slideUp()
          true
        else
          message = "The following fields are invalid. Please correct them or leave them empty:<br/>"
          for field in errorFields
            message += "- #{field}<br/>"
          $('.alert').html(message).removeClass('hidden').slideDown()
          $('html,body').animate {scrollTop: 0}, 300
          false

      #$('#application-form form').validationEngine('init')
      $('#application-form form').ttValidation({bindSubmit: false})

      $('#exception-dialog').dialog {
      autoOpen: false,
      width: 600,
      modal: true,
      title: 'Incomplete Application'
      }

      $('#complete-fields-button').click (e) ->
        $('#exception-dialog').dialog 'close'
        e.preventDefault()

      $('#request-exception-button').click (e) ->
        $('#exception-reason').removeClass('hidden').slideDown ->
          $('#exception_text').focus()
        e.preventDefault()

      $('#submit-exception-button').click (e) ->
        val = $("#exception_text").val()
        if val.replace(/\s+/, '') == ''
          $("#exception_text").validationEngine('showPrompt', '* This field is required', 'error')
        else
          $("#exception_reason").val($("#exception_text").val())
          if $('#complete-form').length
            $('#application-form form').append('<input type="hidden" name="commit" value="Complete" />').submit()
          else
            $('#application-form form').submit()
        e.preventDefault()

      $("#submit-button, .submit-button").click (e) ->
        if $('#application-tabs').length
          $('#application-tabs').tabs 'select', 0
        if validateFieldsFormat()
          if $('#application-form form').ttValidation('validate')
            $('#application-form').find('#submit-form').click()
          else
            listExceptions()
            $('#exception-dialog').dialog('open')
        e.preventDefault()

      $("#complete-button, .complete-button").click (e) ->
        $('#application-tabs').tabs 'select', 1
        if $('#application-form form').ttValidation('validate')
          $('#application-form').find('#complete-form').click()
        else
          listExceptions()

          $('#exception-dialog').dialog('open')

        e.preventDefault()

    $('#export-dialog').dialog {
    autoOpen: false,
    width: 600,
    modal: true,
    title: 'View/Print/Save Application'
    }

    $('#email-dialog').dialog {
    autoOpen: false,
    width: 600,
    modal: true,
    title: 'Email Application'
    }

    $('#export-button, .export-button').click (e) ->
      e.preventDefault()

      $('#export-dialog').dialog 'open'

    $('#email-button, .email-button').click (e) ->
      e.preventDefault()

      $('#email-dialog').dialog 'open'

    if $("input:checked[name='application[travellers]']").val() == 'Service Member'
      field = $("input:checked[name='application[travellers]']")
      field.removeAttr('checked').trigger('change').attr('checked','checked').trigger('change')

  if $("#outbound_flight_count.length")
    $("select#outbound_flight_count").change ->
      val = parseInt($(this).val())
      maxVal = $(this).find('option:last').val()
      if parseInt(val)
        for i in [1..(maxVal)]
          if val >= i
            if $("#outbound-flight-#{i}").hasClass 'hidden'
              $("#outbound-flight-#{i}").removeClass('hidden').slideDown()
              $("#outbound-flight-#{i}").find('.destroy-flight').val('false')
          else
            unless $("#outbound-flight-#{i}").hasClass 'hidden'
              $("#outbound-flight-#{i}").slideUp ->
                $(this).addClass 'hidden'
                $(this).find('.destroy-flight').val('true')

      else
        $('.outbound-flight').each ->
          $(this).slideUp ->
            $(this).addClass 'hidden'
            $(this).find('.destroy-flight').val('true')

  if $("#return_flight_count.length")
    $("select#return_flight_count").change ->
      val = parseInt($(this).val())
      maxVal = $(this).find('option:last').val()
      if parseInt(val)
        for i in [1..(maxVal)]
          if val >= i
            if $("#return-flight-#{i}").hasClass 'hidden'
              $("#return-flight-#{i}").removeClass('hidden').slideDown()
              $("#return-flight-#{i}").find('.destroy-flight').val('false')
          else
            unless $("#return-flight-#{i}").hasClass 'hidden'
              $("#return-flight-#{i}").slideUp ->
                $(this).addClass 'hidden'
                $(this).find('.destroy-flight').val('true')

      else
        $('.return-flight').each ->
          $(this).slideUp ->
            $(this).addClass 'hidden'
            $(this).find('.destroy-flight').val('true')

  if $("#add-note-dialog").length
    $("#add-note-dialog").dialog {
    autoOpen: false,
    modal: true,
    width: 430,
    title: 'Add a note'
    }

    $("#add-note-button, .add-note-button").click (e) ->
      $("#add-note-dialog").dialog 'open'
      e.preventDefault()

  if $('.cw-attachments').length

    $('.cw-attachment select').change ->
      $(this).parents('.cw-attachment').find('.ref_id').val $(this).val()

    $('.add-cw-attachment').click (e) ->
      e.preventDefault()

      if $('.cw-attachment:first').is(':visible')
        $clone = $('.cw-attachment:first').clone()
        $clone.find('select').val('')
        .attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][category]")
        $clone.find('input:text').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][description]").val("")
        $clone.find('input:file').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][document]").val("")
        $clone.find('.ref_id').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][ref_id]").val("")
        $('.cw-attachments tbody tr:visible:last').after $clone
        $('.cw-attachments tbody tr:visible:last').trigger 'change'
      else
        $('.cw-attachment').find('select').val('').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][category]")
        $('.cw-attachment').find('input:text').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][description]").val("")
        $('.cw-attachment').find('input:file').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][document]").val("")
        $('.cw-attachment').find('.ref_id').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][ref_id]").val("")
        $('.cw-attachment').removeClass 'hidden'
        $('.cw-attachment select').trigger 'change'


      window.cwAttachmentIndex += 1

    $('.cw-remove-attachment').live 'click', (e) ->
      e.preventDefault()
      if $('.cw-attachment').length < 2
        $(this).parents('.cw-attachment').addClass 'hidden'
        $(this).parents('.cw-attachment').find('select').attr 'name', ''
        $(this).parents('.cw-attachment').find('input:file').attr 'name', ''
      else
        $(this).parents('.cw-attachment').fadeOut().remove()

      window.cwAttachmentIndex -= 1

    $('.cw-remove-persisted-attachment').click (e) ->
      e.preventDefault()

      $(this).siblings('.destroy-attachment').val('true')
      $(this).parents('tr').fadeOut()

      clone = $(this).parents('tr').next('.cw-required-attachment.hidden')
      clone.find('input:text').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][description]").val("")
      clone.find('input:text').attr('id', "application_attachments_attributes_#{window.cwAttachmentIndex}_description")
      clone.find('input:file').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][document]").val("")
      clone.find('input:file').attr('id', "application_attachments_attributes_#{window.cwAttachmentIndex}_document")
      clone.find('.ref_id').attr('name', "application[attachments_attributes][#{window.cwAttachmentIndex}][ref_id]").val("")
      clone.find('.ref_id').attr('id', "application_attachments_attributes_#{window.cwAttachmentIndex}_ref_id")
      clone.removeClass('hidden').fadeIn()
      window.cwAttachmentIndex += 1

  if $('.admin-attachments').length

    $('.admin-attachment select').change ->
      $(this).parents('.admin-attachment').find('.ref_id').val $(this).val()

    $('.add-admin-attachment').click (e) ->
      e.preventDefault()

      if $('.admin-attachment:first').is(':visible')
        $clone = $('.admin-attachment:first').clone()
        $clone.find('select').val('')
        .attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][category]")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_category")
        $clone.find('input:text').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][description]").val("")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_description")
        $clone.find('input:file').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][document]").val("")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_document")
        $clone.find('.ref_id').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][ref_id]").val("")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_ref_id")
        $clone.find('.owned_by_admin').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][owned_by_admin]").val('true')
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_owned_by_admin")
        $('.admin-attachments tbody tr:visible:last').after $clone
        $('.admin-attachments tbody tr:visible:last').trigger 'change'
      else
        $('.admin-attachment').find('select').val('').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][category]")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_category")
        $('.admin-attachment').find('input:text').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][description]").val("")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_description")
        $('.admin-attachment').find('input:file').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][document]").val("")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_document")
        $('.admin-attachment').find('.ref_id').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][ref_id]").val("")
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_ref_id")
        $('.admin-attachment').find('.owned_by_admin').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][owned_by_admin]").val('true')
        .attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_owned_by_admin")
        $('.admin-attachment').removeClass 'hidden'
        $('.admin-attachment select').trigger 'change'


      window.adminAttachmentIndex += 1

    $('.admin-remove-attachment').live 'click', (e) ->
      e.preventDefault()
      if $('.admin-attachment').length < 2
        $(this).parents('.admin-attachment').addClass 'hidden'
        $(this).parents('.admin-attachment').find('select').attr 'name', ''
        $(this).parents('.admin-attachment').find('input:file').attr 'name', ''
      else
        $(this).parents('.admin-attachment').fadeOut().remove()

      window.adminAttachmentIndex -= 1

    $('.admin-remove-persisted-attachment').click (e) ->
      e.preventDefault()

      $(this).siblings('.destroy-attachment').val('true')
      $(this).parents('tr').fadeOut()
      clone = $(this).parents('tr').next('.admin-required-attachment.hidden')
      clone.find('input:text').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][description]").val("")
      clone.find('input:text').attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_description")
      clone.find('input:file').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][document]").val("")
      clone.find('input:file').attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_document")
      clone.find('.ref_id').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][ref_id]").val("")
      clone.find('.ref_id').attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_ref_id")
      clone.find('.owned_by_admin').attr('name', "application[admin_attachments_attributes][#{window.adminAttachmentIndex}][owned_by_admin]").val('true')
      clone.find('.owned_by_admin').attr('id', "application_admin_attachments_attributes_#{window.adminAttachmentIndex}_owned_by_admin")
      clone.removeClass('hidden').fadeIn()
      window.adminAttachmentIndex += 1

  if $('.airport-select').length
    airportCache = {}
    $('.airport-select').autocomplete {
    minLength: 2,
    source: (q,r) ->
      obj = {}
      t = q.term
      #         if t in airportCache
      #           r(airportCache[t])
      #           return

      obj.q = t
      if $("#update_airport").length || $('#show-application.admin').length
        obj.archived = 'true'
      if $("#update_airport").length
        obj.update_airport = 'true'
      lXhr = $.getJSON '/applications/airport',obj, (d,s,x) ->
        airportCache[t] = d
        if x == lXhr
          r d
    ,
    select: (e,ui) ->
      if $('#update_airport').length
        window.selectedAirport = ui.item.obj.id
        $('#update_airport .id').val ui.item.obj.id
        $('.code').val ui.item.obj.code
        $('.city').val ui.item.obj.city.replace(/\(.+\)/, '')
        $('.area').val ui.item.obj.area.replace(/\(.+\)/, '')
        $('.country').val ui.item.obj.country.replace(/\(.+\)/, '')
        if ui.item.obj.is_archived
          $('.is_archived').attr 'checked', 'checked'
        else
          $('.is_archived').removeAttr 'checked'

        if ui.item.obj.is_empty
          $("#destroy_link").show()
        else
          $("#destroy_link").hide()

      else if $(this).data('target')
        $("##{$(this).data('target')}").val ui.item.obj.id
    ,
    change: (e,ui) ->
      if !ui.item
        $(this).val ''
        if $(this).data('target').length
          $("##{$(this).data('target')}").val ''
          $(this).blur()
    }

  if $("#toggle-application-history").length
    $("#toggle-application-history").click (e) ->
      e.preventDefault()

      if $("#application-notes").hasClass 'hidden'
        $("#application-notes").removeClass('hidden').slideDown()
        $(this).html '[-] Hide History'
      else
        $("#application-notes").slideUp ->
          $(this).addClass 'hidden'
        $(this).html '[+] Show History'

  if $('.currency').length
    $('.currency').bind 'blur change', ->
      $(this).formatCurrency {symbol: ''}

  if $('input:checkbox#email_other').length
    $('input:checkbox#email_other').click ->
      if $(this).is ':checked'
        $('#email_other_address').removeAttr 'disabled'
      else
        $('#email_other_address').attr 'disabled', 'true'

  $("input[name='application[medical_center_other]']").change ->
    if $(this).val() == 'None'
      $('#medical_center').removeClass('hidden').slideDown()
    else
      $('#medical_center').slideUp()
