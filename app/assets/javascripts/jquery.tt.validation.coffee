$ = jQuery

methods = {
  init: (options) ->
    settings = {
      bindSubmit: true
    }

    this.each ->
      if options
        $.extend settings, options

      if settings.bindSubmit
        $(this).bind 'submit.ttvalidation', ->
          methods._validateForm $(this)


  ,
  validate: ->
    methods._validateForm $(this)
  ,
  _validateForm: (form) ->
    valid = true
    @form = form
    form.find('.validation-failed').removeClass 'validation-failed'

    form.find('[class*=validate]').not(':hidden').not(':disabled').each ->
      unless methods._validateField $(this)
        valid = false
      true

    if form.find('.validation-failed-message')
      if valid
        form.find('.validation-failed-message').addClass 'hidden'
      else
        form.find('.validation-failed-message').removeClass 'hidden'

    return valid
  ,
  _markParent: ($parent) ->
    $parent.addClass 'validation-failed'
    if $parent.hasClass 'validation-child'
      methods._markParent $parent.parent()

    @valid = false

  ,
  _validateField: (field) ->
    @valid = true

    rules = /validate\[(.*)\]/.exec(field.attr('class'))
    unless rules
      return true

    rules = rules[1].split /\[|,|\]/
    $.each rules, (k,v) =>
      if v == 'required'
        switch field.attr('type')
          when "radio", "checkbox"
            name = field.attr 'name'
            if $("input[name='#{name}']:checked").length < 1
              methods._markParent field.parent()

          else
            if field.is(':visible') && field.val().replace(/\s+/,'') == ''
              methods._markParent field.parent()

      else if v == 'email'
        unless field.val() == '' || field.val().match /.+@.+\..+/
          methods._markParent field.parent()

      else if v == 'phone'
        if field.attr('class').split(' ')[0] == 'phone-3'
          reg = new RegExp /^\d{3}$/
        else
          reg = new RegExp /^\d{4}$/

        unless reg.test field.attr 'value'
          field.parent().addClass 'validation-failed'
          @valid = false

      else if v.match /group-.+/
        filled = false
        $(".#{v}").each ->
          switch $(this).attr('type')
            when "radio", "checkbox"
              if $(this).is(':checked')
                filled = true
            else
              unless $(this).val().replace(/\s+/,'') == ''
                filled = true

        unless filled
          methods._markParent field.parent()

      else if v == 'password_required'
        usr_pass = @form.find('#user_password').attr("value")
        usr_pass_confirm = field.attr("value")

        if usr_pass.length<6 ||  usr_pass != usr_pass_confirm
          methods._markParent @form.find('#user_password').parent()
          methods._markParent field.parent()

    return @valid
}

$.fn.ttValidation = (method) ->

  if methods[method]
    methods[method].apply this, Array.prototype.slice.call(arguments,1)

  else if typeof method == 'object' || !method
    methods.init.apply this, arguments

  else
    $.error "Method #{method} does not exist on jQuery.ttValidation"
