  class HomeController < ApplicationController
  def index
    if current_user
      if current_user.is_admin?
        redirect_to admin_home_path
      else
        redirect_to case_worker_home_path
      end
    else
      redirect_to sign_in_path
    end
  end

  def sign_up_confirmation
    flash.keep
    redirect_to sign_in_path
  end

end
