class ApplicationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_admin!, :only => [:checkout, :checkin]

  def create
    begin
      @application = Application.process_new params, current_user
      if @application.state == 'saved'
        flash[:notice] = "Application saved successfully"
      elsif @application.state == 'submitted'
        flash[:notice] = "Application submitted successfully"
      elsif @application.state == 'exception'
        flash[:notice] = "Application submitted successfully"
      end
    rescue
      flash[:alert] = AppSettings.invalid_credentials_message
      @application = Application.find_all_by_user_id(current_user.id).last
    end

    if current_user.is_admin?
      redirect_to admin_show_application_path(@application)
    else
      redirect_to case_worker_show_application_path(@application)
    end
  end

  def update
    application_data = params[:application]
    passenger_count = params[:passenger_count].to_i
    passenger_count = 1 if application_data['travellers'] == 'Service Member'

    # Ensure empty passengers aren't created but at the same time
    # existing passengers are deleted when the no. of passengers is reduced.
    passenger_data = application_data.delete 'application_passengers_attributes'

    (0..(AppSettings.first.max_passengers-1)).each do |i|
      if i+1 > passenger_count && passenger_data[i.to_s]['_destroy'] != 'true'
        passenger_data.delete i.to_s
      end
    end

    application_data['application_passengers_attributes'] = passenger_data

    # Remove empty attachments
    unless application_data['attachments_attributes'].blank?
      attachments_data = application_data.delete 'attachments_attributes'
      attachments_data.reject! { |k,v| v['_destroy'] != 'true' && v['document'].blank? }
      application_data['attachments_attributes'] = attachments_data
    end


    if current_user.is_admin?
      # Ensure empty attachments aren't created
      unless application_data['admin_attachments_attributes'].blank?
        attachments_data = application_data.delete 'admin_attachments_attributes'
        attachments_data.reject! { |k,v| v['_destroy'] != 'true' && v['document'].blank? }
        application_data['admin_attachments_attributes'] = attachments_data
      end


      # Ensure empty flights aren't created but at the same time
      # existing flights are deleted when the no. of flights is reduced.
      outbound_flight_count = params[:outbound_flight_count].to_i
      outbound_flight_data = application_data.delete 'outbound_flights_attributes'

      if outbound_flight_data
        (0..(AppSettings.first.max_flights-1)).each do |i|
          if i+1 > outbound_flight_count && outbound_flight_data[i.to_s] && outbound_flight_data[i.to_s]['_destroy'] != 'true'
            outbound_flight_data.delete i.to_s
          end
        end
        application_data['outbound_flights_attributes'] = outbound_flight_data
        (0...outbound_flight_count).each do |i|
          application_data['outbound_flights_attributes'][i.to_s]["depart_time"] = ""  if application_data['outbound_flights_attributes'][i.to_s]["depart_time"].length < 7
          application_data['outbound_flights_attributes'][i.to_s]["arrive_time"] = "" if application_data['outbound_flights_attributes'][i.to_s]["arrive_time"].length < 7
        end
      end

      if outbound_flight_count >= 1
        application_data['final_departure_date(1i)'] = application_data['outbound_flights_attributes'][0.to_s]["departure_date(1i)"]
        application_data['final_departure_date(2i)'] = application_data['outbound_flights_attributes'][0.to_s]["departure_date(2i)"]
        application_data['final_departure_date(3i)'] = application_data['outbound_flights_attributes'][0.to_s]["departure_date(3i)"]
      end

      return_flight_count = params[:return_flight_count].to_i
      return_flight_data = application_data.delete 'return_flights_attributes'
      if return_flight_data
        (0..(AppSettings.first.max_flights-1)).each do |i|
          if i+1 > return_flight_count && return_flight_data[i.to_s] && return_flight_data[i.to_s]['_destroy'] != 'true'
            return_flight_data.delete i.to_s
          end
        end
        application_data['return_flights_attributes'] = return_flight_data
        (0...return_flight_count).each do |i|
          application_data['return_flights_attributes'][i.to_s]["depart_time"] = ""  if application_data['return_flights_attributes'][i.to_s]["depart_time"].length < 7
          application_data['return_flights_attributes'][i.to_s]["arrive_time"] = "" if application_data['return_flights_attributes'][i.to_s]["arrive_time"].length < 7
        end
      end

      if return_flight_count >= 1
        application_data['final_return_date(1i)'] = application_data['return_flights_attributes'][0.to_s]["departure_date(1i)"]
        application_data['final_return_date(2i)'] = application_data['return_flights_attributes'][0.to_s]["departure_date(2i)"]
        application_data['final_return_date(3i)'] = application_data['return_flights_attributes'][0.to_s]["departure_date(3i)"]
      end

      @application = Application.find_by_id params[:id]
      @application.assign_attributes application_data, :as => :admin
    else
      @application = current_user.applications.find_by_id params[:id]
      @application.assign_attributes application_data
    end

    begin
      if params[:commit] == 'Save'
        @application.is_exception = true unless @application.valid?
        @application.save && @application.save_application!(:user => current_user)
        flash[:notice] = "Application saved successfully"
      elsif current_user.is_admin?
        @application.save
        if params[:commit] == 'Update'
          flash[:notice] = "Application updated successfully"
        elsif params[:commit] == 'Submit'
          @application.submit!(:user => current_user)
          flash[:notice] = "Application submitted successfully"
        elsif params[:commit] == 'Return'
          @application.return!(:user => current_user)
          flash[:notice] = "Application returned successfully"
        elsif params[:commit] == 'Decline'
          @application.decline!(:user => current_user)
          flash[:notice] = "Application declined successfully"
        elsif params[:commit] == 'Complete'
          @application.complete!(:user => current_user, :exception => params[:exception_reason])

          ticket = render_to_string :pdf => 'TicketTopper',
                                    :layout => 'pdf',
                                    :template => 'applications/ticket_topper.pdf.haml',
                                    :margin => {:bottom => 30},
                                    :footer => {:html => {:template => 'applications/_ticket_topper_footer.pdf.haml'}}

          tf = Tempfile.new "Itinerary.pdf"
          tf.binmode
          tf << ticket
          tf.flush && tf.rewind

          begin
            @application.email_itinerary(tf)
          rescue
            flash[:alert] = "Could not deliver notification email to #{@application.created_by_admin?? "Administrator" : "Case Worker"} because of  blank #{@application.created_by_admin?? "Administrator" : "Case Worker"} E-mail address."
          end

          flash[:notice] = "Application completed successfully"
        elsif params[:commit] == 'Save Changes'
          flash[:notice] = "Application saved successfully"
        else
          @application.is_exception = false
          if @application.valid?
            @application.save && @application.submit!(:user => current_user)
          else
            @application.is_exception = true
            @application.save && @application.raise_exception!(:user => current_user, :exception => params[:exception_reason])
          end
          flash[:notice] = "Application submitted successfully"
        end
      else
        @application.is_exception = false
        if @application.valid?
          @application.save && @application.submit!(:user => current_user)
        else
          @application.is_exception = true
          @application.save && @application.raise_exception!(:user => current_user, :exception => params[:exception_reason])
        end
        flash[:notice] = "Application submitted successfully"
      end
      unless flash[:alert].nil?
        flash[:notice] = nil
      end
    rescue
      flash[:alert] = AppSettings.invalid_credentials_message
    end

    if current_user.is_admin?
      redirect_to admin_show_application_path(@application)
    else
      redirect_to case_worker_show_application_path(@application)
    end
  end

  def destroy
    @application = Application.find_by_id params[:id]
    redirect_to '/404' and return unless @application

    if @application.user == current_user && ['saved', 'returned'].include?(@application.state)
      @application.destroy
      flash[:notice] = "Application deleted successfully"
    end

    redirect_to( current_user.is_admin? ? application_management_path : case_worker_applications_path)
  end

  def checkout
    @application = Application.find_by_id params[:id]
    if params[:user].blank?
      @application.checkin!(current_user)
      flash[:notice] = "Application Unassigned successfully"
      redirect_to application_management_path and return
    else
      @user = User.find_by_id params[:user]
      @application.checkout!(@user) if @user.is_admin?
    end
    redirect_to '/404' and return unless @application
    redirect_to admin_show_application_path(@application)
  end

  def checkin
    @application = Application.find_by_id params[:id]
    redirect_to '/404' unless @application
    @application.checkin!(current_user)
    redirect_to admin_show_application_path(:id => @application.id, :checkout => 'false')
  end

  def reopen
    @application = Application.find_by_id params[:id]
    redirect_to '/404' unless @application
    begin
      @application.reopen!(:user => current_user)
      flash[:notice] = "Application reopened successfully"
    rescue
      flash[:alert] = AppSettings.invalid_credentials_message
    end

    redirect_to admin_show_application_path(@application)
  end

  def export
    @application = Application.find_by_id params[:id]
    redirect_to '/404' and return unless @application
    hash = rand(36**8).to_s(36)

    if params[:view]
      disposition = 'inline'
    else
      disposition = 'attachment'
    end

    if params[:cw_application] || params[:admin_application]
      if params[:cw_application]
        @hide_admin = true
        template = 'case_worker/show_application.pdf.haml'
      elsif params[:admin_application]
        if current_user.is_admin?
          @hide_cw = true
          template = 'administration/show_application.pdf.haml'
        end
      end

      application = render_to_string :pdf => 'Application',
                                    :layout => 'pdf',
                                    :template => template,
                                    :footer => { :line => true,
                                                 :right => '[page] of [topage]' }

      tf = Tempfile.new "Application-#{hash}.pdf"
      tf.binmode
      tf << application
      tf.flush && tf.rewind

      send_file tf, :filename => "Application-#{@application.record_locator}.pdf",
                    :disposition => disposition

    elsif params[:attachment] || params[:admin_attachment]
      if params[:attachment]
        attachment = @application.attachments.find_by_id params[:attachment]
      elsif params[:admin_attachment] && current_user.is_admin?
        attachment = @application.admin_attachments.find_by_id params[:admin_attachment]
      end
      if attachment && attachment.document_content_type == 'application/pdf'
        send_file "#{Rails.root}/public#{attachment.document.url.gsub(/\?\w+$/, '')}",
                  :filename => attachment.document_file_name,
                  :disposition => disposition
      end

    elsif params[:itinerary] && @application.state == 'completed'
      ticket = render_to_string :pdf => 'TicketTopper',
                                :layout => 'pdf',
                                :template => 'applications/ticket_topper.pdf.haml',
                                :margin => {:bottom => 30},
                                :footer => {:html => {:template => 'applications/_ticket_topper_footer.pdf.haml'}}

      tf = Tempfile.new "Itinerary-#{hash}.pdf"
      tf.binmode
      tf << ticket
      tf.flush && tf.rewind

      send_file tf, :filename => "Itinerary-#{@application.record_locator}.pdf",
                    :disposition => disposition
    end

  end

  def mail
    @application = Application.find_by_id params[:id]
    (redirect_to '/404' and return) unless @application
    files = {}
    email_ids = []

    email_ids << @application.case_worker_email if params[:email_caseworker]
    email_ids << @application.email if params[:email_service_member]
    email_ids << params[:email_other_address] if params[:email_other]

    email_ids.reject! { |x| (x =~ /.+@.+\..+/).nil? }


    if params[:cw_application]
      @hide_cw = false
      @hide_admin = true
      template = 'case_worker/show_application.pdf.haml'
      application = render_to_string :pdf => 'Application',
                                     :layout => 'pdf',
                                     :template => template,
                                     :header => { :center => "Hero Miles Application #{@application.record_locator}",
                                                  :line => true},
                                     :footer => { :line => true,
                                                  :right => '[page] of [topage]' }

      tf = Tempfile.new "Application-#{hash}.pdf"
      tf.binmode
      tf << application
      tf.flush && tf.rewind
      files["Application-#{@application.record_locator}-CaseWorker.pdf"] = tf
    end

    if params[:admin_application] && current_user.is_admin?
      @hide_admin = false
      @hide_cw = true
      template = 'administration/show_application.pdf.haml'
      application = render_to_string :pdf => 'Application',
                                     :layout => 'pdf',
                                     :template => template,
                                     :header => { :center => "Hero Miles Application #{@application.record_locator}",
                                                  :line => true,
                                                  :right => '[page] of [topage]' }

      tf = Tempfile.new "Application-#{hash}.pdf"
      tf.binmode
      tf << application
      tf.flush && tf.rewind
      files["Application-#{@application.record_locator}-Admin.pdf"] = tf
    end

    if params[:itinerary] && @application.state == 'completed'
      ticket = render_to_string :pdf => 'TicketTopper',
                                :layout => 'pdf',
                                :template => 'applications/ticket_topper.pdf.haml'

      tf = Tempfile.new "Itinerary-#{hash}.pdf"
      tf.binmode
      tf << ticket
      tf.flush && tf.rewind
      files["Itinerary-#{@application.record_locator}.pdf"] = tf
    end

    if params[:attachments]
      params[:attachments].each do |k,v|
        attachment = @application.attachments.find_by_id k
        if attachment.blank? && current_user.is_admin?
          attachment = @application.admin_attachments.find_by_id(k)
        end

        unless attachment.blank?
          if attachment.document_content_type == 'application/pdf'
            files["#{attachment.document_file_name}"] = File.open "#{Rails.root}/public#{attachment.document.url.gsub(/\?\w+$/, '')}"
          end
        end
      end

    end

    unless email_ids.blank? || files.blank?
      begin
        AppMailer.application_export_email(email_ids, files).deliver
        flash[:notice] = 'Email sent successfully.'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Could not deliver notification email because of a blank E-mail address."
    end

    if current_user.is_admin?
      redirect_to admin_show_application_path(@application)
    else
      redirect_to case_worker_show_application_path(@application)
    end

  end

  def airport
    results = []
    if params[:q]
      opts = {}
      unless params[:archived]
        opts[:is_archived] = false
      end

      matches = Airport.search "#{params[:q]}*", :match_mode => :extended, :with => opts

      matches.sort! { |x,y| x.code <=> y.code }
            #.sort_by! { |x| x.country == "United States" ? 0 : 1}
            #.sort_by! { |x| x.name =~ /international/i ? 0 : 1 }
      matches = matches.partition { |x| x.country == "United States" }.flatten
      matches = matches.partition { |x| x.name =~ /international/i }
      puts matches
      matches[0].partition { |x| x.country == "United States" }
      matches.flatten!

      matches.each { |x| if x.country != "United States"
                            state_or_country = x.country
                         else
                            state_or_country = x.area
                         end
                         results << {:value => params[:update_airport ]? x.name : "#{x.code} - #{x.location}, #{state_or_country}".gsub(/\(.+\)/,''),
                                     :label => "#{x.code} - #{x.location}, #{x.area}, #{x.country} - #{x.name}",
                                     :obj => {:name => "#{x.name}",
                                              :code => "#{x.code}",
                                              :city => "#{x.location}",
                                              :area => "#{x.area}",
                                              :country => "#{x.country}",
                                              :id => x.id,
                                              :is_archived => x.is_archived?,
                                              :is_empty => x.is_empty?
                                             }
                                    }
                   }
    end
    render :text => results.to_json
  end
end
