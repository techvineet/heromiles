class RegistrationsController < Devise::RegistrationsController

  def create
    build_resource
    if verify_recaptcha
      if resource.save
        flash[:notice] = "Thank you. You will be notified when your account is approved."
        respond_with resource, :location => after_sign_up_path_for(resource)
        #respond_with resource, :location => redirect_location(resource_name, resource)
      else
        clean_up_passwords(resource)
        if resource.errors.size > 0 and resource.errors[:email].include? "has already been taken"
          flash.now[:alert] = "An account with this email address already exists"
        end
        respond_with_navigational(resource) { render_with_scope :new }
      end
    else
      flash.now[:alert] = 'The verification words below did not match. Please enter them again.'
      clean_up_passwords(resource)
      respond_with_navigational(resource) { render_with_scope :new }
    end
  end

  def update
    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    if resource.update_attributes(params[resource_name])
      set_flash_message :notice, :updated if is_navigational_format?
      sign_in resource_name, resource, :bypass => true
      respond_with resource, :location => after_update_path_for(resource)
    else
      clean_up_passwords(resource)
      if resource.errors.size > 0
        if resource.errors[:email].include? "has already been taken"
          flash.now[:alert] = "An account with this email address already exists"
        elsif resource.errors[:password].include? "doesn't match confirmation"
          flash.now[:alert] = "Password doesn't match confirmation"
        end
      end
      respond_with_navigational(resource){ render_with_scope :edit }
    end
  end

  protected

  def after_sign_up_path_for(resource)
    '/sign_up_confirmation'
  end

  def after_inactive_sign_up_path_for(resource)
    '/sign_up_confirmation'
  end

  def after_update_path_for(resource)
    '/users/edit'
  end

end
