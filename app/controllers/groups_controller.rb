class GroupsController < ApplicationController
  before_filter :authenticate_admin!

  def create
    @group = Group.create params[:group]
    flash[:notice] = 'Group created successfully'
    redirect_to group_management_path
  end

  def update
    @group = Group.find_by_id params[:id]
    redirect_to '/404' and return unless @group
    @group.update_attributes params[:group]
    flash[:notice] = 'Group updated successfully'
    redirect_to group_management_path
  end

  def destroy
    @group = Group.find_by_id params[:id]
    redirect_to '/404' and return unless @group
    @group.destroy
    flash[:notice] = 'Group deleted successfully'
    redirect_to group_management_path
  end

  def archive
    @group = Group.find_by_id params[:id]
    redirect_to '/404' and return unless @group
    @group.update_attribute :is_archived, true
    flash[:notice] = 'Group archived successfully'
    redirect_to group_management_path
  end

  def unarchive
    @group = Group.find_by_id params[:id]
    redirect_to '/404' and return unless @group
    @group.update_attribute :is_archived, false
    flash[:notice] = 'Group unarchived successfully'
    redirect_to group_management_path
  end

end
