class AppSettingsController < ApplicationController
  before_filter :authenticate_admin!

  def update
    @as = AppSettings.first
    @as.update_attributes params[:app_settings]

    if params[:blank_application] == 'true'
      flash[:notice] = 'Blank Application uploaded successfully'
      redirect_to report_management_path
    else
      if (text = params[:app_settings]['applications_notice'])
        emails = (User.active.receive_notifications - [current_user]).collect(&:email)
        begin
          AppMailer.email_notice_change(current_user, text, emails).deliver
        rescue
          ""
        end
      end
      respond_to do |format|
        format.html { flash[:notice] = 'Settings saved successfully' and redirect_to settings_path}
        format.js
      end
    end
  end

  def new_category
    unless params[:category_name].blank?
      category = process_attachment params
      AppSettings.add_category category
      flash[:notice] = 'Category added successfully'
    end
    redirect_to attachments_path
  end

  def update_category
    unless params[:category_id].blank?
      category = process_attachment params
      AppSettings.update_category params[:category_id], category
      flash[:notice] = 'Category updated successfully'
    end
    redirect_to attachments_path
  end

  def remove_category
    unless params[:category_id].blank?
      AppSettings.remove_category(params[:category_id])
      flash[:notice] = 'Category deleted successfully'
    end
    redirect_to attachments_path
  end

  protected

  def process_attachment(params)
    category = {
        :name => params[:category_name],
        :required => params[:is_required] == 'true' ? true : false,
        :id => params[:category_id] || rand(36**5).to_s(36),
        :is_archived => params[:is_archived] || false
      }
    case params[:display_on]
      when 'cw_admin'
        category[:cw] = true
        category[:admin] = true
      when 'admin'
        category[:cw] = false
        category[:admin] = true
      else
        category[:cw] = true
        category[:admin] = false
    end
    category
  end

end
