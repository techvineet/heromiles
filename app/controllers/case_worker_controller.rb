class CaseWorkerController < ApplicationController
  before_filter :authenticate_user!

  def index
    redirect_to case_worker_applications_path
  end

  def applications
    @open = current_user.applications.open_applications.limit 10
    @notifications = current_user.applications.cw_notifications.order('updated_at desc').limit current_user.notifications_count(30)
  end

  def new_application
    @application = Application.new :user => current_user, :organization => current_user.organization
#     if current_user.organization
#       @group = current_user.organization.groups.find_by_id params[:group]
#     else
#       @group = nil
#     end
    @group = (params[:group] ? Group.find_by_id(params[:group]) : nil )
    if @group
      @application.group = @group
      @application.set_group_defaults
    end
    @application.set_cw_details
    AppSettings.first.max_passengers.times { |x| @application.application_passengers.build }
    AppSettings.attachment_categories.select { |x| !x['admin'] && x['cw'] && x['required'] && !x['is_archived'] }.each do |ac|
      @application.attachments.build :is_cw => true,
                                     :is_required => true,
                                     :category => ac['name'],
                                     :ref_id => ac['id']
    end
    @medical_centers = MedicalCenter.sorted
    @organizations = Organization.sorted
  end

  def show_application
    if current_user.organization_id || current_user.medical_center_id
      @application =  Application.find_by_id_and_case_worker_organization_id(params[:id], current_user.organization_id) if current_user.organization_id
      @application = Application.find_by_id_and_case_worker_medical_center_id(params[:id], current_user.medical_center_id) if current_user.medical_center_id
    else
      @application = current_user.applications.find_by_id params[:id]
    end
    redirect_to '/404' and return if @application.blank?
    if @application.is_editable?(current_user)
      (AppSettings.first.max_passengers - @application.application_passengers.size).times { |x| @application.application_passengers.build}
      AppSettings.attachment_categories.select { |x| !x['admin'] && x['cw'] && x['required'] && !x['is_archived'] }.each do |ac|
        attachment = @application.attachments.find_by_ref_id ac['id']
        if attachment.blank?
          @application.attachments.build :is_cw => true,
                                        :is_required => true,
                                        :category => ac['name'],
                                        :ref_id => ac['id']
        end
        end
    end

    @medical_centers = MedicalCenter.sorted
    @organizations = Organization.sorted

    respond_to do |f|
      f.html
      f.pdf { render :pdf => "Application-#{@application.id}", :layout => 'pdf'}
    end
  end

  def show_open
    @open = current_user.applications.open_applications.limit(10)
    respond_to do |f|
      f.html
      f.js
      f.pdf do
        if params[:view]
          render :pdf => "OpenApplications", :layout => 'pdf'
        else
          render :pdf => "OpenApplications", :layout => 'pdf', :disposition => 'attachment'
        end
      end
    end
  end

  def show_notifications
    @notifications = current_user.applications.cw_notifications.order('updated_at desc').limit current_user.notifications_count(30)
    respond_to do |f|
      f.html
      f.js
      f.pdf do
        if params[:view]
          render :pdf => "Notifications", :layout => 'pdf'
        else
          render :pdf => "Notifications", :layout => 'pdf', :disposition => 'attachment'
        end
      end
    end
  end

  def mail_open
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @open = current_user.applications.open_applications.limit(6)
      pdf = render_to_string :pdf => 'OpenApplications',
                             :layout => 'pdf',
                             :template => 'case_worker/show_open.pdf.haml'
      File.open(Rails.root.join('tmp', 'OpenApplications.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.open_applications_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Email not sent. Please enter a valid email"
    end
    redirect_to case_worker_applications_path
  end

  def mail_notifications
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @notifications = current_user.applications.cw_notifications.order('last_admin_note_date desc').limit(6)
      pdf = render_to_string :pdf => 'Notifications',
                             :layout => 'pdf',
                             :template => 'case_worker/show_notifications.pdf.haml'
      File.open(Rails.root.join('tmp', 'Notifications.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.notifications_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Email not sent. Please enter a valid email"
    end
    redirect_to case_worker_applications_path
  end

  def accounts
    redirect_to edit_user_registration_path
  end

  def reports
  end

  def groups
  end

  def show_group
    @group = Group.find_by_id params[:id]
    @applications = current_user.applications.where(:group_id => @group.id).order('created_at asc').page(params[:page]).per(11)
    respond_to do |f|
      f.html
      f.pdf do
        render :pdf => 'GroupApplications', :layout => 'pdf'
      end
    end
  end

  def mail_group
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @group = Group.find_by_id params[:id]
      @applications = current_user.applications.where(:group_id => @group.id).order('created_at asc').page(params[:page]).per(11)

      pdf = render_to_string :pdf => 'GroupApplications',
                             :layout => 'pdf',
                             :template => 'case_worker/show_group.pdf.haml'
      File.open(Rails.root.join('tmp', 'GroupApplications.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.group_applications_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = 'Email not sent. Please enter a valid email'
    end
    redirect_to case_worker_show_group_path(@group)
  end

end