class SessionsController < Devise::SessionsController

  def create
    if verify_recaptcha
      @user = User.find_by_email(params[:user][:email])
      if @user

        @failed_attempts = @user.failed_attempts

        if @failed_attempts < 5

          if @user.valid_password?(params[:user][:password])
            @user.failed_attempts = 0
          else
            @failed_attempts += 1
            @user.failed_attempts = @failed_attempts
            @user.expires_at = DateTime.now if @failed_attempts >= 5
          end
          @user.save

          if @failed_attempts >= 5
            begin
              AppMailer.suspend_user_email(@user).deliver
            rescue
              flash[:alert] = AppSettings.invalid_credentials_message
            end

            flash[:alert] = 'Your account has been temporarily suspended due to wrong password attempts.'
            redirect_to sign_in_path
            return
          end

        end
      end
      super
    else
      resource = build_resource
      flash[:alert] = 'The account credentials could not be found or the verification words did not match.  Please try again.'
      flash[:email] = params[:user]['email']
      redirect_to sign_in_path
    end
  end

end
