class AirlinesController < ApplicationController
  before_filter :authenticate_admin!

  def create
    @airline = Airline.create params[:airline]
    flash[:notice] = 'Airline created successfully.'
    redirect_to airline_management_path
  end

  def update
    @airline = Airline.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @airline
    @airline.update_attributes params[:airline]
    name = @airline.name
    flash[:notice] = "Airline '#{name}' updated successfully."
    redirect_to airline_management_path
  end

  def destroy
    @airline = Airline.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @airline
    name = @airline.name
    @airline.destroy
    flash[:notice] = "Airline '#{name}' deleted successfully."
    redirect_to airline_management_path
  end

  def archive
    @airline = Airline.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @airline

    @airline.update_attribute :is_archived, true
    flash[:notice] = "Airline '#{@airline.name}' archived successfully."
    redirect_to airline_management_path
  end

end
