class ReportsController < ApplicationController
  before_filter :authenticate_admin!

  def summary
    redirect_to '/404' and return if params[:year_from].blank? || params[:year_till].blank?

    @yearly = {}
    @total = {}
    app_opts = {}
    organization = nil
    medical_center = nil

    from_year = params[:year_from].to_i
    till_year = params[:year_till].to_i
    unless params[:organization].blank?
      organization = Organization.find_by_id params[:organization]
      app_opts[:organization_id] = organization.id unless organization.blank?
    end

    unless params[:medical_center].blank?
      medical_center = MedicalCenter.find_by_id params[:medical_center]
      app_opts[:medical_center_id] = medical_center.id unless medical_center.blank?
    end

    if from_year > till_year
      flash[:alert] = 'Start Year cannot be greater than End Year'
      redirect_to report_management_path
    end

    (from_year..till_year).each do |year|
      @yearly[year.to_s] = {}
      time = Time.strptime year.to_s, '%Y'
      year_period = time..(time + 1.year)
      opts = {}

      app_opts[:booked_at] = year_period

      opts[:application_id] = Application.where(app_opts).collect(&:id).flatten

      tickets = ApplicationPassenger.where opts

      @yearly[year.to_s]['tickets'] = tickets.size
      @yearly[year.to_s]['value'] = tickets.inject(0) {|x,y| x += (y.application.comparable_price.blank? ? 0 : y.application.comparable_price.sub(',','').to_f )}
      @yearly[year.to_s]['mileage'] = tickets.inject(0) {|x,y| x += ( y.application.mileage.blank? ? 0 : y.application.mileage.sub(',','').to_i)}

    end


    unless from_year == till_year
      @total['tickets'] = @yearly.inject(0) { |x,y| x += y[1]['tickets'].to_i}
      @total['value'] = @yearly.inject(0) { |x,y| x += y[1]['value'].to_f}
      @total['mileage'] = @yearly.inject(0) { |x,y| x += y[1]['mileage'].to_i}
    end

    if params[:mail]
      file = prep_pdf :template => 'reports/summary'
      begin
        AppMailer.report_email(params[:email], {'Summary Report' => file}, "Hero Miles Summary Report").deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      if params[:view]
        disposition = 'inline'
      else
        disposition = 'attachment'
      end
      respond_to do |f|
        f.html
        f.pdf { render :pdf => "Report", :layout => 'pdf'}
      end
    end

  end

  def monthly
    @results = {}
    @total = []
    @org = nil
    @med = nil
    app_opts = {}

    unless params[:org_monthly].blank?
      @org = Organization.find_by_id(params[:org_monthly])
      app_opts[:organization_id] = @org.id unless @org.blank?
    end

    unless params[:med_monthly].blank?
      @med = MedicalCenter.find_by_id(params[:med_monthly])
      app_opts[:medical_center_id] = @med.id unless @med.blank?
    end

    current_year = Time.zone.now.year

    2.downto(0).each do |n|
      year = current_year - n
      y = Time.strptime("#{year}","%Y")
      @results[year.to_s] = {}

      if year == current_year
        year_period = y.beginning_of_year..Time.zone.now.beginning_of_month
        range = 1..(Time.zone.now.month-1)
      else
        range = 1..12
        year_period = y.beginning_of_year..y.end_of_year
      end

      range.each do |m|
        t = Time.strptime("#{year} #{m}", '%Y %m')
        time_period = t.beginning_of_month..t.end_of_month
        opts = {}

        app_opts[:booked_at] = time_period
        @results[year.to_s][Date::MONTHNAMES[t.month]] = Application.where(app_opts).collect(&:application_passengers).flatten.size
      end

      unless (year == current_year && Time.zone.now.month == 1 )
        app_opts[:booked_at] = year_period
        applications = Application.where(app_opts)

        @results[year.to_s]['savings'] = applications.inject(0) { |x,y| x += ( y.comparable_price.blank? ? 0 : y.comparable_price.sub(',','').to_f) }
        @results[year.to_s]['miles'] = applications.inject(0) { |x,y| x += (y.mileage.blank? ? 0 : y.mileage.sub(',','').to_f) }
        @results[year.to_s]['tickets'] = applications.collect(&:application_passengers).flatten.size
      end
    end

    if params[:mail]
      file = prep_pdf :template => 'reports/monthly'
      begin
        AppMailer.report_email(params[:email], {'Monthly Report' => file}, "Hero Miles Monthly Report").deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      if params[:view]
        disposition = 'inline'
      else
        disposition = 'attachment'
      end
      respond_to do |f|
        f.html
        f.pdf { render :pdf => "Report", :layout => 'pdf'}
      end
    end

  end

  def branch
    redirect_to '/404' and return if params[:branch].blank? || params[:type].blank? || params[:year].blank?

    @results = {}
    @total = []
    app_opts = {}
    opts = {}
    @org = nil
    @med = nil
    unless params[:org].blank?
      @org = Organization.find_by_id(params[:org])
      app_opts[:organization_id] = @org.id unless @org.blank?
    end
    unless params[:med].blank?
      @med = MedicalCenter.find_by_id(params[:med])
      app_opts[:medical_center_id] = @med.id unless @med.blank?
    end

    tickets = []

    time = Time.strptime(params[:year], '%Y')
    year_period = time.beginning_of_year..time.end_of_year

    month_periods = {}

    (1..12).each do |m|
      t = Time.strptime("#{params[:year]} #{m}", '%Y %m')
      month_periods[Date::MONTHNAMES[t.month]] = t.beginning_of_month..t.end_of_month
    end


    case params[:branch]
      when 'airline'
        array = Airline.all.collect(&:name)
        filter = :airline
      else
        array = ['Air Force', 'Army', 'Coast Guard', 'Marine Corps', 'Navy']
        filter = :service_branch
    end

    month_periods.each do |k,v|
      @results[k] = {}
      array.each do |b|
        app_opts[filter] = b
        app_opts[:booked_at] = v
        app = Application.where(app_opts).collect(&:id).flatten

        tickets = ApplicationPassenger.where :application_id => app

        @results[k][b] = case params[:type]
          when 'ticket'
            tickets.size
          when 'savings'
            tickets.inject(0) {|x,y| x += (y.application.comparable_price.blank? ? 0 : y.application.comparable_price.sub(',','').to_f) }
          when 'miles'
            tickets.inject(0) {|x,y| x += (y.application.mileage.blank? ? 0 : y.application.mileage.sub(',','').to_i) }
          else
            0
        end
      end
    end

    index = 0
    @results.values.first.keys.each do |b|
      @total[index] = @results.inject(0) { |x,y| x += y[1][b].to_i}
      index += 1
    end

    if params[:mail]
      file = prep_pdf :template => 'reports/branch'
      begin
        AppMailer.report_email(params[:email],  {'Branch/Airline Report' => file}, "Hero Miles Branch/Airline Report").deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      if params[:view]
        disposition = 'inline'
      else
        disposition = 'attachment'
      end
      respond_to do |f|
        f.html
        f.pdf { render :pdf => "Report", :layout => 'pdf'}
      end
    end
  end

  protected

  # Generate a pdf of a report and send back a url
  def prep_pdf(opts= {})
    path = nil
    hash = rand(36**8).to_s(36)
    unless opts.blank?
      report = render_to_string :pdf => 'Report',
                                :layout => 'pdf',
                                :template => opts[:template]

      tf = Tempfile.new "Report-#{hash}.pdf"
      tf.binmode
      tf << report
      tf.flush
      path = tf.path
    end
    path
  end

end
