class ApplicationController < ActionController::Base
  protect_from_forgery

  def authenticate_admin!
    if user_signed_in?
      if current_user.is_admin?
        true
      else
        redirect_to '/404'
      end
    else
      redirect_to sign_in_path
    end
  end
end
