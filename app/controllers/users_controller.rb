class UsersController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :assign_user, :except => [:create]

  def create
    @user = User.new params[:user]
    @user.is_active = true
    if ['2', 2].include?(params[:account_type])
      @user.is_admin = true
    end
    if @user.save
      if @user.is_admin
        flash[:notice] = 'Administrator Account created successfully'
      else
        flash[:notice] = 'Case Worker Account created successfully'
      end
      redirect_to account_management_path
    else
      if @user.errors.size > 0
        if @user.errors[:email].include? "has already been taken"
          flash.now[:alert] = "An account with this email address already exists"
        end
      end
      render '/administration/new_account'
    end

  end

  def update

    user_data = params[:user]
    @password_change  = true
    if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
      user_data.delete(:password)
      user_data.delete(:password_confirmation)
      @password_change = false
    end
    if @user.update_attributes user_data
      if params[:commit] == 'Approve'
        begin
          @user.activate!
          flash[:notice] = 'Account approved successfully'
        rescue
          flash[:alert] = AppSettings.invalid_credentials_message
        end
      else
        if @user.expires_at.nil? || @user.expires_at > DateTime.now
          @user.failed_attempts = 0
          @user.save
        end
        sign_in(@user, :bypass => true) if @user == current_user && @password_change
        flash[:notice] = 'Account updated successfully'
      end
      redirect_to account_management_path
    else
        flash[:alert] = @user.errors.full_messages.to_sentence
        redirect_to account_path(@user)
    end
  end

  def destroy
    @user.destroy
    flash[:notice] = 'Account deleted successfully'
    redirect_to account_management_path
  end

  def approve
    begin
      @user.activate!
      flash[:notice] = 'Account approved successfully'
    rescue
      flash[:alert] = AppSettings.invalid_credentials_message
    end
    redirect_to account_management_path
  end


  protected

  def assign_user
    @user = User.find_by_id params[:id]
    redirect_to '/404' unless @user
  end

end
