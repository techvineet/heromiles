class OrganizationsController < ApplicationController
  before_filter :authenticate_admin!

  def create
    @organization = Organization.create params[:organization]
    flash[:notice] = 'Organization created successfully.'
    redirect_to organization_management_path
  end

  def update
    @organization = Organization.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @organization
    @organization.update_attributes params[:organization]
    name = @organization.name
    flash[:notice] = "Organization '#{name}' updated successfully."
    redirect_to organization_management_path
  end

  def destroy
    @organization = Organization.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @organization
    name = @organization.name

    if @organization.applications.blank?
      @organization.destroy
      flash[:notice] = "Organization '#{name}' deleted successfully."
    else
      @organization.update_attribute :is_archived, true
      flash[:notice] = "Organization '#{name}' archived successfully."
    end
    redirect_to organization_management_path
  end

  def archive
    @organization = Organization.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @organization

    @organization.update_attribute :is_archived, true
    flash[:notice] = "Organization '#{@organization.name}' archived successfully."
    redirect_to organization_management_path
  end

end
