class MedicalCentersController < ApplicationController
  before_filter :authenticate_admin!

  def create
    @medical_center = MedicalCenter.create params[:medical_center]
    flash[:notice] = 'Medical Center created successfully.'
    redirect_to medical_center_management_path
  end

  def update
    @medical_center = MedicalCenter.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @medical_center
    @medical_center.update_attributes params[:medical_center]
    name = @medical_center.name
    flash[:notice] = "Medical Center '#{name}' updated successfully."
    redirect_to medical_center_management_path
  end

  def destroy
    @medical_center = MedicalCenter.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @medical_center
    name = @medical_center.name

    if @medical_center.applications.blank?
      @medical_center.destroy
      flash[:notice] = "Medical Center '#{name}' deleted successfully."
    else
      @medical_center.update_attribute :is_archived, true
      flash[:notice] = "Medical Center '#{name}' archived successfully."
    end

    redirect_to medical_center_management_path
  end

  def archive
    @medical_center = MedicalCenter.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @medical_center

    @medical_center.update_attribute :is_archived, true
    flash[:notice] = "Medical Center '#{@medical_center.name}' archived successfully."
    redirect_to medical_center_management_path
  end

end
