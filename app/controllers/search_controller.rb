class SearchController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_admin!, :only => :accounts

  def applications
    @results = []
    options = {}
    raw_query = params[:q] || ""
    raw_query = raw_query.sub '@', '\@'
    query = raw_query
    @status = []

    # Parse status options
    begin
      status = JSON.parse params[:status]
      unless status.blank?
        @status = status
        options[:state] = []
        status.each do |s|
          if ['completed', 'declined', 'submitted', 'saved', 'returned', 'reopened'].include?(s.downcase)
            options[:state] << s.downcase.to_crc32
          elsif s.downcase == 'exceptions'
            options[:state] << 'exception'.to_crc32
          end
        end
        options[:state].uniq!
      end
    rescue
    end

    # Parse fields restrictions
    begin
      restrict = JSON.parse params[:restrict]
      unless restrict.blank? || raw_query.blank?
        query = ""
        if restrict.include? 'Service Member'
          query += "@(first_name,last_name,email,current_status) #{raw_query} "
        end

        if restrict.include? 'Passenger'
          query += "| " unless query.blank?
          query += "@(passenger_fname,passenger_lname,passenger_dob,passenger_gender,passenger_relation,passenger_city,passenger_state,passenger_email) #{raw_query} "
        end

        if restrict.include? 'Email Address'
          query += "| " unless query.blank?
          query += "@(email,passenger_email) #{raw_query}"
        end
      end
    rescue
    end


    options[:organization_id] = params[:organization_id] unless params[:organization_id].blank?
    options[:medical_center_id] = params[:medical_center_id] unless params[:medical_center_id].blank?
    options[:group_id] = params[:group_id] unless params[:group_id].blank?
    options[:service_branch] = params[:branch].to_crc32 unless params[:branch].blank?

    # Parse date restrictions
    begin
      from = Time.new(params['search_from(1i)'].to_i,params['search_from(2i)'].to_i,params['search_from(3i)'].to_i)
      @from = from
    rescue
      from = Time.zone.now - 10.years
      @from = nil
    end

    begin
      till = Time.new(params['search_till(1i)'].to_i,params['search_till(2i)'].to_i,params['search_till(3i)'].to_i)
      @till = till
    rescue
      if from > Time.zone.today
        till = from + 10.years
      else
        till = Time.zone.now + 10.years
      end
      @till = nil
    end

    begin
      case params[:date_for]
        when 'Requested Departure'
          options[:departure_date] = from..till
        when 'Actual Departure'
          options[:final_departure_date] = from..till
        when 'Requested Arrival'
          options[:return_date] = from..till
        when 'Actual Arrival'
          options[:final_return_date] = from..till
        when 'Completed'
          options[:completed_at] = from..till
        when 'Submitted'
          options[:submitted_at] = from..till
        when 'Exceptions'
          options[:submitted_at] = from..till
        when 'Declined'
          options[:declined_at] = from..till
        when 'Saved'
          options[:saved_at] = from..till
        when 'Returned'
          options[:returned_at] = from..till
        when 'Date Booked'
          options[:booked_at] = from..till
      else
      end

    rescue
    end
    @order = nil
    
    case params[:sort]
      when 'Departing'
        @order = 'final_departure_sort'
      when 'Service Member'
        @order = 'service_member_name'
      when 'Status'
        @order = 'state'
      when 'Created'
        @order = 'created_sort'
      when 'Submitted'
        @order = 'submitted_sort'
      when 'Medical Center'
        @order = 'medical_center_name'
      when 'Date Booked'
        @order = 'booked_sort'
      when 'Case Worker'
        @order = 'case_worker_name'
      else
        @order = 'updated_sort'
    end
    sort = params[:reverse].eql?('true')? 'ASC' : 'DESC'

    if current_user.is_admin?
      @results = Application.search(query,
                                    :with => options,
                                    :order => "#{@order} #{sort}",
                                    :max_matches => 25_000,
                                    :match_mode => :extended).page(params[:page]).per(11)
    else
      if params[:all_users]
        if current_user.organization_id
          options[:organization_id] = current_user.organization_id
        end
        if current_user.medical_center_id
          options[:medical_center_id] = current_user.medical_center_id
        end
      else

        if current_user.organization_id
          options[:case_worker_organization_id] = current_user.organization_id
        end
        if current_user.medical_center_id
          options[:case_worker_medical_center_id] = current_user.medical_center_id
        end

      end

      @results = Application.search(query,
                                    :with => options,
                                    :order => "#{@order} #{sort}",
                                    :max_matches => 25_000,
                                    :match_mode => :extended).page(params[:page]).per(11)
    end

    @results.compact!

    respond_to do |f|
      f.html
      f.pdf {
        if params[:save]
          disposition = 'attachment'
        else
          disposition = 'inline'
        end
        render :pdf => "SearchResults", :layout => 'pdf', :disposition => disposition
      }
      f.csv {
        require 'csv'
        if @results.total_entries > @results.size
          total = @results.total_entries
          @results = Application.search(query,
                                    :with => options,
                                    :max_matches => 25_000,
                                    :match_mode => :extended).page(nil).per(total)
        end
        csv = Application.generate_passengers_csv(@results)
        f = Tempfile.new 'csv'
        f << csv
        f.flush && f.rewind

        send_file f.path, :filename => 'SearchExport.csv'
      }
    end
  end

  def accounts
    @results = []
    options = {}
    raw_query = params[:q] || ""
    raw_query = raw_query.sub '@', '\@'
    query = raw_query
    @restrict = []

    begin
      @restrict = JSON.parse params[:restrict]
      restrict = @restrict
      unless restrict.blank? || raw_query.blank?
        query = ""
        if restrict.include?('Active Accounts')
          options[:is_active] = true
        end

        if restrict.include?('Name')
          query = "@(first_name,last_name) #{raw_query} "
        end

        if restrict.include?('Email Address')
          unless query.blank?
            query += "| "
          end
          query += "@(email) #{raw_query}"
        end

      end
    rescue
    end

    options[:medical_center_id] = params[:medical_center] unless params[:medical_center].blank?
    options[:organization_id] = params[:organization] unless params[:organization].blank?

    @results = User.search(query, :with => options, :match_mode => :extended).page(params[:page]).per(11)
    @results.compact!

    respond_to do |f|
      f.html
      f.pdf {
        if params[:save]
          disposition = 'attachment'
        else
          disposition = 'inline'
        end
        render :pdf => "SearchResults", :layout => 'pdf', :disposition => disposition
      }
      f.csv {
      }
    end
  end

end
