class ApplicationNotesController < ApplicationController
  before_filter :authenticate_user!

  def create
    @last = ApplicationNote.find_all_by_application_id(params[:application_note][:application_id]).sort_by!(&:created_at).last
    @an = ApplicationNote.new params[:application_note]
    @an.is_action = false
    @an.save
  end

end
