class AirportsController < ApplicationController
  before_filter :authenticate_admin!

  def create
    @airport = Airport.create params[:airport]
    flash[:notice] = 'Airport created successfully.'
    redirect_to airport_management_path
  end

  def update
    @airport = Airport.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @airport
    @airport.update_attributes params[:airport]
    name = @airport.name
    flash[:notice] = "Airport '#{name}' updated successfully."
    redirect_to airport_management_path
  end

  def destroy
    @airport = Airport.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @airport
    name = @airport.name
    @airport.destroy
    flash[:notice] = "Airport '#{name}' deleted successfully."
    redirect_to airport_management_path
  end

  def archive
    @airport = Airport.unscoped.find_by_id params[:id]
    redirect_to '/404' unless @airport

    @airport.update_attribute :is_archived, true
    flash[:notice] = "Airport '#{@airport.name}' archived successfully."
    redirect_to airport_management_path
  end

end
