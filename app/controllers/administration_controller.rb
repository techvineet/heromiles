class AdministrationController < ApplicationController
  before_filter :authenticate_admin!

  def index
    redirect_to application_management_path
  end

  def applications
    @open = Application.admin_open(current_user).order("-departure_date desc")
    @notifications = Application.admin_notification_messages(current_user).order("last_cw_note_date asc")
    #@notifications += Application.admin_notification_completed(current_user).order("updated_at desc").limit(6-@notifications.size)
    #@notifications.flatten!
  end

  def new_application
    if params[:group]
      @group = Group.find_by_id params[:group]
    end
    @application = Application.new :user => current_user, :organization => current_user.organization
    if @group
      @application.group = @group
      @application.set_group_defaults
    end
    @application.set_cw_details
    @application.created_by_admin = true
    AppSettings.first.max_passengers.times { |x| @application.application_passengers.build }
    AppSettings.first.max_flights.times { |x| @application.outbound_flights.build }
    AppSettings.first.max_flights.times { |x| @application.return_flights.build }
    AppSettings.attachment_categories.select { |x| x['cw'] && x['admin'] && x['required'] && !x['is_archived'] }.each do |ac|
      @application.attachments.build :is_cw => true,
                                     :is_admin => true,
                                     :is_required => true,
                                     :category => ac['name'],
                                     :ref_id => ac['id']
    end
    @medical_centers = MedicalCenter.sorted
    @organizations = Organization.sorted
  end

  def show_application
    @application = Application.find_by_id params[:id]
    (redirect_to '/404' and return) unless @application

    @editable = @application.is_editable?(current_user)
    unless params[:checkout] == 'false' || !@editable
      @application.checkout!(current_user) if @application.admin.blank?
    end

    if @editable
      (AppSettings.first.max_flights - @application.outbound_flights.size).times { |x| @application.outbound_flights.build}
      (AppSettings.first.max_flights - @application.return_flights.size).times { |x| @application.return_flights.build}
      (AppSettings.first.max_passengers - @application.application_passengers.size).times { |x| @application.application_passengers.build}
      AppSettings.attachment_categories.select { |x| x['cw'] && x['admin'] && x['required'] && !x['is_archived'] }.each do |ac|
        attachment = @application.attachments.find_by_ref_id ac['id']
        if attachment.blank? && !ac['is_archived']
          @application.attachments.build :is_cw => true,
                                         :is_admin => true,
                                         :is_required => true,
                                         :category => ac['name'],
                                         :ref_id => ac['id']
        end
      end

      AppSettings.attachment_categories.select { |x| !x['cw'] && x['admin'] && x['required'] && !x['is_archived'] }.each do |ac|
        attachment = @application.admin_attachments.find_by_ref_id ac['id']
        if attachment.blank? && !ac['is_archived']
          @application.admin_attachments.build :is_admin => true,
                                               :is_required => true,
                                               :category => ac['name'],
                                               :ref_id => ac['id'],
                                               :owned_by_admin => true
        end
      end
    end

    @medical_centers = ([MedicalCenter.unscoped.find_by_id(@application.medical_center_id)] + MedicalCenter.sorted).flatten.uniq.compact
    @organizations = ([Organization.unscoped.find_by_id(@application.organization_id)] + Organization.sorted).flatten.uniq.compact

    respond_to do |f|
      f.html
      f.pdf { render :pdf => "Application-#{@application.id}", :layout => 'pdf'}
    end
  end

  def accounts
    @active = User.active.order('first_name asc')
    @pending = User.inactive.order('first_name asc')
  end

  def show_account
    @user = User.find_by_id params[:id]
    (redirect_to '/404' and return) unless @user
  end

  def new_account
    @user = User.new
  end

  def show_pending
    @accounts = User.where(:expires_at => nil, :is_active => false).order('first_name asc')
    respond_to do |f|
      f.html
      f.pdf do
        render :pdf => "PendingAccounts", :layout => 'pdf'
      end
    end
  end

  def show_active
    @accounts = User.where(:is_active => true).order('first_name asc')
    respond_to do |f|
      f.html
      f.pdf do
        render :pdf => "ActiveAccounts", :layout => 'pdf'
      end
    end
  end

  def mail_pending
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @accounts = User.where(:expires_at => nil, :is_active => false).order('first_name asc')
      pdf = render_to_string :pdf => 'PendingAccounts',
                             :layout => 'pdf',
                             :template => 'administration/show_pending.pdf.haml'
      File.open(Rails.root.join('tmp', 'PendingAccounts.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.pending_accounts_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Email not sent. Please enter a valid email"
    end
    redirect_to account_management_path
  end

  def mail_active
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @accounts = User.where(:is_active => true).order('first_name asc')
      pdf = render_to_string :pdf => 'ActiveAccounts',
                             :layout => 'pdf',
                             :template => 'administration/show_active.pdf.haml'
      File.open(Rails.root.join('tmp', 'ActiveAccounts.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.active_accounts_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Email not sent. Please enter a valid email"
    end
    redirect_to account_management_path
  end

  def show_open
    @open = Application.admin_open(current_user).order("-departure_date desc")#.limit(6)
    @notifications = Application.admin_notification_messages(current_user).order("last_cw_note_date asc")
    respond_to do |f|
      f.html
      f.js
      f.pdf do
        if params[:view]
          render :pdf => "OpenApplications", :layout => 'pdf'
        else
          render :pdf => "OpenApplications", :layout => 'pdf', :disposition => 'attachment'
        end
      end
      f.csv do
        require 'csv'

        @open = Application.admin_open(current_user).order("-departure_date desc")
        csv = Application.generate_passengers_csv(@open)

        f = Tempfile.new 'csv'
        f << csv
        f.flush && f.rewind

        send_file f.path, :filename => 'OpenApplications.csv'
      end
    end
  end

  def show_notifications
    @notifications = Application.admin_notification_messages(current_user).order("last_cw_note_date asc").limit(6)
    @notifications += Application.admin_notification_completed(current_user).order("updated_at desc").limit(6-@notifications.size)
    @notifications.flatten!
    respond_to do |f|
      f.html
      f.js
      f.pdf do
        if params[:view]
          render :pdf => "Notifications", :layout => 'pdf'
        else
          render :pdf => "Notifications", :layout => 'pdf', :disposition => 'attachment'
        end
      end
      f.csv do
        require 'csv'

#         @notifications = Application.admin_notifications.order("last_cw_note_date asc")
        csv = Application.generate_passengers_csv(@notifications)

        f = Tempfile.new 'csv'
        f << csv
        f.flush && f.rewind

        send_file f.path, :filename => 'NotificationsExport.csv'
      end
    end
  end

  def mail_open
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @open = Application.submitted.order("updated_at asc").limit(6)
      pdf = render_to_string :pdf => 'OpenApplications',
                             :layout => 'pdf',
                             :template => 'administration/show_open.pdf.haml'
      File.open(Rails.root.join('tmp', 'OpenApplications.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.open_applications_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Email not sent. Please enter a valid email"
    end
    redirect_to application_management_path
  end

  def mail_notifications
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @notifications = Application.admin_notification_messages(current_user).order("last_cw_note_date asc").limit(6)
      @notifications += Application.admin_notification_completed(current_user).order("updated_at desc").limit(6-@notifications.size)
      @notifications.flatten!
      pdf = render_to_string :pdf => 'Notifications',
                             :layout => 'pdf',
                             :template => 'administration/show_notifications.pdf.haml'
      File.open(Rails.root.join('tmp', 'Notifications.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.notifications_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = "Email not sent. Please enter a valid email"
    end
    redirect_to application_management_path
  end

  def reports
  end

  def advanced
  end

  def medical_centers
    @medical_center = MedicalCenter.new
    @medical_centers = MedicalCenter.unscoped.reload
  end

  def organizations
    @organization = Organization.new
    @organizations = Organization.unscoped.reload
    puts @organizations.inspect
  end

  def groups
    @groups = Group.active
    @archived = Group.archived
  end

  def new_group
    (redirect_to '/404' and return) unless params[:group_name]
    @group = Group.new :name => params[:group_name], :organization_id => params[:organization_id]
    @group.set_default_fields
    @application = Application.new :user => current_user
    @application.application_passengers.build
    @medical_centers = MedicalCenter.sorted
    @organizations = Organization.sorted
  end

  def edit_group
    @group = Group.find_by_id params[:id]
    @application = Application.new :user => current_user
    @application.group = @group
    @application.set_group_defaults
    @application.application_passengers.build
    @medical_centers = MedicalCenter.sorted
    @organizations = Organization.sorted
  end

  def show_group
    @group = Group.find_by_id params[:id]
    @applications = Application.where(:group_id => @group.id).order('created_at asc')
                               .page(params[:page]).per(11)
    respond_to do |f|
      f.html
      f.pdf do
        render :pdf => 'GroupApplications', :layout => 'pdf'
      end
    end
  end

  def mail_group
    if params[:email] && !(params[:email] =~ /.+@.+\..+/).nil?
      @group = Group.find_by_id params[:id]
      @applications = Application.where(:group_id => @group.id).order('created_at asc')
      .page(params[:page]).per(11)

      pdf = render_to_string :pdf => 'GroupApplications',
                             :layout => 'pdf',
                             :template => 'administration/show_group.pdf.haml'
      File.open(Rails.root.join('tmp', 'GroupApplications.pdf'), 'wb') do |f|
        f << pdf
      end
      begin
        AppMailer.group_applications_email(params[:email]).deliver
        flash[:notice] = 'Email sent successfully'
      rescue
        flash[:alert] = AppSettings.invalid_credentials_message
      end
    else
      flash[:alert] = 'Email not sent. Please enter a valid email'
    end
    redirect_to show_group_path(@group)
  end

  def airports
  end

  def airlines
    @airlines = Airline.unscoped.reload
  end

  def settings
  end

  def attachments
  end

end
