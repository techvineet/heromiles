class CreateAirports < ActiveRecord::Migration
  def change
    create_table :airports do |t|
      t.string :code
      t.string :name
      t.string :location
      t.string :area
      t.string :country

      t.timestamps
    end

    add_index :airports, :code
  end
end
