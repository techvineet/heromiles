class AddNewApplicationFields < ActiveRecord::Migration
  def up
    add_column :applications, :dtr, :boolean, :default => false
    add_column :applications, :funeral_assistance, :boolean, :default => false
    add_column :applications, :memorial_service, :boolean, :default => false
  end

  def down
    remove_column :applications, :dtr
    remove_column :applications, :funeral_assistance
    remove_column :applications, :memorial_service
  end
end
