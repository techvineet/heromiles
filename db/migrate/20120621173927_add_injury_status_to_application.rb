class AddInjuryStatusToApplication < ActiveRecord::Migration
  def change
    add_column :applications, :injury_status, :string, :default => ""
  end
end
