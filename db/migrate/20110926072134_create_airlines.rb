class CreateAirlines < ActiveRecord::Migration
  def change
    create_table :airlines do |t|
      t.string :name
      t.string :units
      t.string :conversion
      t.timestamps
    end
  end
end
