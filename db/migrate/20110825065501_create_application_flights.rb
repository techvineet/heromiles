class CreateApplicationFlights < ActiveRecord::Migration
  def change
    create_table :application_flights do |t|

      t.string :flight_no

      t.string :depart_from
      t.time :depart_time

      t.string :arrive_in
      t.time :arrive_time

      t.string :seat_1
      t.string :seat_2
      t.string :seat_3
      t.string :seat_4
      t.string :seat_5
      t.string :seat_6

      t.references :application
      t.timestamps
    end

    add_index :application_flights, :application_id
  end
end
