class AddIsReturnToApplicationFlight < ActiveRecord::Migration
  def change
    add_column :application_flights, :is_return, :boolean, :default => false
  end
end
