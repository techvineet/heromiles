class CreateAppSettings < ActiveRecord::Migration
  def change
    create_table :app_settings do |t|
      t.text :applications_notice
      t.text :attachment_categories_data
      t.integer :max_passengers, :default => 6
      t.integer :max_flights, :default => 6

      t.string :blank_application_file_name
      t.string :blank_application_content_type
      t.string :blank_application_file_size
      t.string :blank_application_updated_at

      t.timestamps
    end
  end
end
