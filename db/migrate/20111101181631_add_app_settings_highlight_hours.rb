class AddAppSettingsHighlightHours < ActiveRecord::Migration
  def up
    add_column :app_settings, :highlight_hours, :integer, :default => 48
  end

  def down
    remove_column :app_settings, :highlight_hours
  end
end
