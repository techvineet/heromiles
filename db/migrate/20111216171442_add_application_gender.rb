class AddApplicationGender < ActiveRecord::Migration
  def up
    add_column :applications, :gender, :string
  end

  def down
    remove_column :applications, :gender
  end
end
