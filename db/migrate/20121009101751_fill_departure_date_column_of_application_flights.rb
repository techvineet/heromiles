class FillDepartureDateColumnOfApplicationFlights < ActiveRecord::Migration
  def up
    Application.all.each do |a|

      overnight_flights = 0
      previous_flight = nil
      a.outbound_flights.each do |af|
        overnight_flights += next_day_flight?(af, previous_flight)? 1:0
        af.update_column :departure_date, departure_date(af, overnight_flights)
        overnight_flights += overnight_flight?(af)? 1:0
        previous_flight = af
      end

      overnight_flights = 0
      previous_flight = nil
      a.return_flights.each do |af|
        overnight_flights += next_day_flight?(af, previous_flight)? 1:0
        af.update_column :departure_date, return_date(af, overnight_flights)
        overnight_flights += overnight_flight?(af)? 1:0
        previous_flight = af
      end

    end
  end

  def down
  end

  def overnight_flight?(s)
    begin
      (s.arrive_time < s.depart_time)? true : false
    rescue
      ''
    end
  end

  def next_day_flight?(s, previous_flight)
    unless previous_flight.nil?
      begin
        (s.depart_time < previous_flight.arrive_time)? true : false
      rescue
        ''
      end
    end
  end

  def departure_date(s, overnight_flights)
    begin
      (s.application.final_departure_date + overnight_flights.days)
    rescue
      ''
    end
  end

  def return_date(s, overnight_flights)
    begin
      (s.application.final_return_date + overnight_flights.days)
    rescue
      ''
    end
  end
end
