class AddAirlinesIsArchived < ActiveRecord::Migration
  def up
    add_column :airlines, :is_archived, :boolean, :default => false
  end

  def down
    remove_column :airlines, :is_archived
  end
end
