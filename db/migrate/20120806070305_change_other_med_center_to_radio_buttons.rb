class ChangeOtherMedCenterToRadioButtons < ActiveRecord::Migration
  def up
    add_column :applications, :medical_center_other, :string

    Application.all.each do |a|
      other = case
        when a.dtr
          'DTR'
        when a.funeral_assistance
          'Funeral Assistance'
        when a.memorial_service
          'Memorial Service'
        else
          ''
      end
      a.update_attribute :medical_center_other, other
    end

#     remove_column :applications, :dtr
#     remove_column :applications, :funeral_assistance
#     remove_column :applications, :memorial_service
  end

  def down
    remove_column :applications, :medical_center_other
    add_column :applications, :dtr, :boolean, :default => false
    add_column :applications, :funeral_assistance, :boolean, :default => false
    add_column :applications, :memorial_service, :boolean, :default => false
  end
end
