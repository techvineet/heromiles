class AddApplicationBookedAtDate < ActiveRecord::Migration
  def up
    add_column :applications, :booked_at, :date
  end

  def down
    remove_column :applications, :booked_at
  end
end
