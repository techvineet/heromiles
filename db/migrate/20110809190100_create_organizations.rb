class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|

      t.string :name
      t.string :abbreviation

      t.string :city
      t.string :state

      t.timestamps

    end

    add_index :organizations, :name
  end
end
