class AddMedicalCenterReferences < ActiveRecord::Migration
  def up
    add_column :users, :medical_center_id, :integer
    add_column :applications, :medical_center_id, :integer
    add_column :applications, :case_worker_medical_center_id, :integer
    add_column :applications, :referrer_medical_center_id, :integer

    add_index :users, :medical_center_id
    add_index :applications, :medical_center_id
  end

  def down
    remove_column :users, :medical_center_id
    remove_column :applications, :medical_center_id
    remove_column :applications, :case_worker_medical_center_id
    remove_column :applications, :referrer_medical_center_id

    remove_index :users, :medical_center_id
    remove_index :applications, :medical_center_id
  end
end
