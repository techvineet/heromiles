class AddApplicationPassengerLapChild < ActiveRecord::Migration
  def up
    add_column :application_passengers, :is_lap_child, :boolean, :default => false
  end

  def down
    remove_column :application_passengers, :is_lap_child
  end
end
