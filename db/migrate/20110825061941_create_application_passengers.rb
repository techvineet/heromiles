class CreateApplicationPassengers < ActiveRecord::Migration
  def change
    create_table :application_passengers do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name

      t.date :date_of_birth
      t.string :gender, :default => ''

      t.string :relation, :default => ''
      t.string :city
      t.string :state

      t.string :phone_1
      t.string :phone_2
      t.string :phone_3

      t.string :email
      t.string :ticket_no

      t.boolean :is_exception, :default => false

      t.references :application
      t.timestamps

    end

    add_index :application_passengers, :application_id
  end
end
