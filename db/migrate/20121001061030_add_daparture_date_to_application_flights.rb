class AddDapartureDateToApplicationFlights < ActiveRecord::Migration
  def change
    add_column :application_flights, :departure_date, :date
  end

  def down
    remove_column :application_flights, :departure_date
  end
end
