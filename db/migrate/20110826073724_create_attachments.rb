class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :document_file_name
      t.string :document_content_type
      t.string :document_file_size
      t.string :document_updated_at

      t.references :application
      t.string :category
      t.string :description
      t.string :ref_id

      t.boolean :is_required, :default => false
      t.boolean :is_admin, :default => false
      t.boolean :is_cw, :default => false

      t.boolean :owned_by_admin, :default => false

      t.timestamps
    end

    add_index :attachments, :application_id
  end
end
