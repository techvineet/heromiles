class CreateMedicalCenters < ActiveRecord::Migration
  def change
    create_table :medical_centers do |t|
      t.string :name
      t.string :abbreviation

      t.string :city
      t.string :state

      t.timestamps
    end

    add_index :medical_centers, :name
  end
end
