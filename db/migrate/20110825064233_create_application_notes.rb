class CreateApplicationNotes < ActiveRecord::Migration
  def change
    create_table :application_notes do |t|

      t.references :application
      t.references :user

      t.text :message
      t.boolean :is_action, :default => true
      t.string :status

      t.timestamps

    end

    add_index :application_notes, :application_id
  end
end
