class AddIsArchivedFlag < ActiveRecord::Migration

  def up
    add_column :organizations, :is_archived, :boolean, :default => false
    add_column :medical_centers, :is_archived, :boolean, :default => false
    add_column :airports, :is_archived, :boolean, :default => false
  end

  def down
    remove_column :organizations, :is_archived
    remove_column :medical_centers, :is_archived
    remove_column :airports, :is_archived
  end

end
