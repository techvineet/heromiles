class AddApplicationAuxiliaryFields < ActiveRecord::Migration
  def up
    add_column :applications, :has_special_request, :boolean, :default => false
    add_column :applications, :passenger_count, :integer

    # Migrate existing data
    Application.all.each do |a|
      attrs = {}
      attrs[:has_special_request] = true unless a.special_request.blank?
      attrs[:passenger_count] = a.application_passengers.count unless a.application_passengers.blank?

      a.update_attributes(attrs) unless attrs.blank?
    end
  end

  def down
    remove_column :applications, :has_special_request
    remove_column :applications, :passenger_count
  end
end
