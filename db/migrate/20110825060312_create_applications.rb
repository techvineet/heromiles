class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|

      t.string :first_name
      t.string :middle_name
      t.string :last_name

      t.date :date_of_birth

      t.string :phone_1
      t.string :phone_2
      t.string :phone_3
      t.string :alt_phone_1
      t.string :alt_phone_2
      t.string :alt_phone_3

      t.string :email

      #t.string :injury_location
      t.boolean :injury_after_911
      t.string :service_branch
      t.string :current_status

      t.boolean :is_inpatient
      t.boolean :has_used_heromiles
      t.date :last_use_date

      t.date :injury_date

      t.string :flight_type, :default => 'Round-trip'
      t.string :flight_details

      t.string :depart_from_1_id
      t.string :depart_from_2_id
      t.string :arrive_in_1_id
      t.string :arrive_in_2_id
      t.date :departure_date
      t.date :return_date

      t.text :special_request
      t.string :travellers

      t.string :airline
      t.string :mileage
      t.string :record_locator
      t.string :total_price
      t.string :comparable_price
      t.string :final_depart_from_1_id
      t.string :final_arrive_in_1_id
      t.string :final_depart_from_2_id
      t.string :final_arrive_in_2_id
      t.date :final_departure_date
      t.date :final_return_date

      t.string :case_worker_first_name
      t.string :case_worker_last_name
      t.string :case_worker_job_title
      t.integer :case_worker_organization_id

      t.string :case_worker_email
      t.string :case_worker_phone_1
      t.string :case_worker_phone_2
      t.string :case_worker_phone_3

      t.string :information_collected_by

      t.string :referrer_first_name
      t.string :referrer_last_name
      t.string :referrer_job_title
      t.integer :referrer_organization_id

      t.string :referrer_email
      t.string :referrer_phone_1
      t.string :referrer_phone_2
      t.string :referrer_phone_3

      t.boolean :created_by_admin, :default => false
      t.boolean :is_exception, :default => false
      t.boolean :is_locked

      t.references :user
      t.references :organization
      t.references :group
      t.integer :admin_id

      t.string :state
      t.boolean :last_action_by_admin
      t.date :last_admin_action_date
      t.date :last_admin_note_date
      t.date :last_cw_note_date

      t.datetime :submitted_at
      t.datetime :completed_at
      t.datetime :declined_at
      t.datetime :saved_at
      t.datetime :returned_at

      t.timestamps
    end

    add_index :applications, :organization_id
    add_index :applications, :user_id
    add_index :applications, :group_id
    add_index :applications, :admin_id
  end
end
