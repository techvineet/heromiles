# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120809064627) do

  create_table "airlines", :force => true do |t|
    t.string   "name"
    t.string   "units"
    t.string   "conversion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_archived", :default => false
  end

  create_table "airports", :force => true do |t|
    t.string   "code"
    t.string   "name"
    t.string   "location"
    t.string   "area"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_archived", :default => false
  end

  add_index "airports", ["code"], :name => "index_airports_on_code"

  create_table "app_settings", :force => true do |t|
    t.text     "applications_notice"
    t.text     "attachment_categories_data"
    t.integer  "max_passengers",                 :default => 6
    t.integer  "max_flights",                    :default => 6
    t.string   "blank_application_file_name"
    t.string   "blank_application_content_type"
    t.string   "blank_application_file_size"
    t.string   "blank_application_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "highlight_hours",                :default => 48
  end

  create_table "application_flights", :force => true do |t|
    t.string   "flight_no"
    t.string   "depart_from"
    t.time     "depart_time"
    t.string   "arrive_in"
    t.time     "arrive_time"
    t.string   "seat_1"
    t.string   "seat_2"
    t.string   "seat_3"
    t.string   "seat_4"
    t.string   "seat_5"
    t.string   "seat_6"
    t.integer  "application_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_return",      :default => false
  end

  add_index "application_flights", ["application_id"], :name => "index_application_flights_on_application_id"

  create_table "application_notes", :force => true do |t|
    t.integer  "application_id"
    t.integer  "user_id"
    t.text     "message"
    t.boolean  "is_action",      :default => true
    t.string   "status"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "application_notes", ["application_id"], :name => "index_application_notes_on_application_id"

  create_table "application_passengers", :force => true do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.string   "gender",         :default => ""
    t.string   "relation",       :default => ""
    t.string   "city"
    t.string   "state"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "phone_3"
    t.string   "email"
    t.string   "ticket_no"
    t.boolean  "is_exception",   :default => false
    t.integer  "application_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_lap_child",   :default => false
  end

  add_index "application_passengers", ["application_id"], :name => "index_application_passengers_on_application_id"

  create_table "applications", :force => true do |t|
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.date     "date_of_birth"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "phone_3"
    t.string   "alt_phone_1"
    t.string   "alt_phone_2"
    t.string   "alt_phone_3"
    t.string   "email"
    t.boolean  "injury_after_911"
    t.string   "service_branch"
    t.string   "current_status"
    t.boolean  "is_inpatient"
    t.boolean  "has_used_heromiles"
    t.date     "last_use_date"
    t.date     "injury_date"
    t.string   "flight_type",                   :default => "Round-trip"
    t.string   "flight_details"
    t.string   "depart_from_1_id"
    t.string   "depart_from_2_id"
    t.string   "arrive_in_1_id"
    t.string   "arrive_in_2_id"
    t.date     "departure_date"
    t.date     "return_date"
    t.text     "special_request"
    t.string   "travellers"
    t.string   "airline"
    t.string   "mileage"
    t.string   "record_locator"
    t.string   "total_price"
    t.string   "comparable_price"
    t.string   "final_depart_from_1_id"
    t.string   "final_arrive_in_1_id"
    t.string   "final_depart_from_2_id"
    t.string   "final_arrive_in_2_id"
    t.date     "final_departure_date"
    t.date     "final_return_date"
    t.string   "case_worker_first_name"
    t.string   "case_worker_last_name"
    t.string   "case_worker_job_title"
    t.integer  "case_worker_organization_id"
    t.string   "case_worker_email"
    t.string   "case_worker_phone_1"
    t.string   "case_worker_phone_2"
    t.string   "case_worker_phone_3"
    t.string   "information_collected_by"
    t.string   "referrer_first_name"
    t.string   "referrer_last_name"
    t.string   "referrer_job_title"
    t.integer  "referrer_organization_id"
    t.string   "referrer_email"
    t.string   "referrer_phone_1"
    t.string   "referrer_phone_2"
    t.string   "referrer_phone_3"
    t.boolean  "created_by_admin",              :default => false
    t.boolean  "is_exception",                  :default => false
    t.boolean  "is_locked"
    t.integer  "user_id"
    t.integer  "organization_id"
    t.integer  "group_id"
    t.integer  "admin_id"
    t.string   "state"
    t.boolean  "last_action_by_admin"
    t.date     "last_admin_action_date"
    t.date     "last_admin_note_date"
    t.date     "last_cw_note_date"
    t.datetime "submitted_at"
    t.datetime "completed_at"
    t.datetime "declined_at"
    t.datetime "saved_at"
    t.datetime "returned_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "medical_center_id"
    t.integer  "case_worker_medical_center_id"
    t.integer  "referrer_medical_center_id"
    t.string   "gender"
    t.date     "booked_at"
    t.string   "injury_status",                 :default => ""
    t.boolean  "dtr",                           :default => false
    t.boolean  "funeral_assistance",            :default => false
    t.boolean  "memorial_service",              :default => false
    t.string   "medical_center_other"
    t.boolean  "has_special_request",           :default => false
    t.integer  "passenger_count"
  end

  add_index "applications", ["admin_id"], :name => "index_applications_on_admin_id"
  add_index "applications", ["group_id"], :name => "index_applications_on_group_id"
  add_index "applications", ["medical_center_id"], :name => "index_applications_on_medical_center_id"
  add_index "applications", ["organization_id"], :name => "index_applications_on_organization_id"
  add_index "applications", ["user_id"], :name => "index_applications_on_user_id"

  create_table "attachments", :force => true do |t|
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.string   "document_file_size"
    t.string   "document_updated_at"
    t.integer  "application_id"
    t.string   "category"
    t.string   "description"
    t.string   "ref_id"
    t.boolean  "is_required",           :default => false
    t.boolean  "is_admin",              :default => false
    t.boolean  "is_cw",                 :default => false
    t.boolean  "owned_by_admin",        :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attachments", ["application_id"], :name => "index_attachments_on_application_id"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.text     "fields_data"
    t.boolean  "is_archived",     :default => false
    t.integer  "organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["organization_id"], :name => "index_groups_on_organization_id"

  create_table "medical_centers", :force => true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_archived",  :default => false
  end

  add_index "medical_centers", ["name"], :name => "index_medical_centers_on_name"

  create_table "organizations", :force => true do |t|
    t.string   "name"
    t.string   "abbreviation"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_archived",  :default => false
  end

  add_index "organizations", ["name"], :name => "index_organizations_on_name"

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "",    :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "job_title"
    t.integer  "organization_id"
    t.string   "other_org_name"
    t.string   "other_org_abbrev"
    t.string   "other_org_city"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "phone_3"
    t.boolean  "is_admin",                              :default => false
    t.boolean  "is_active",                             :default => false
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "medical_center_id"
    t.boolean  "receive_notifications",                 :default => true
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["medical_center_id"], :name => "index_users_on_medical_center_id"
  add_index "users", ["organization_id"], :name => "index_users_on_organization_id"
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
