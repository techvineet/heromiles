# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

org = Organization.create :name => 'Fisher House Foundation, Inc.',
                          :abbreviation => 'Fisher House Foundation'

admin = User.create :first_name => 'Admin',
                    :last_name => 'User',
                    :organization_id => org.id,
                    :phone_1 => '123',
                    :phone_2 => '456',
                    :phone_3 => '7890',
                    :email => 'heromiles@gladcraft.com',
                    :password => 'password'

admin.is_active = true
admin.is_admin = true
admin.save

cw = User.create :first_name => 'Case',
                 :last_name => 'Worker',
                 :organization_id => org.id,
                 :phone_1 => '123',
                 :phone_2 => '654',
                 :phone_3 => '0987',
                 :email => 'cw@fisherhouse.org',
                 :password => 'password'

cw.is_active = true
cw.save

airlines = [{:name => 'Airtran Airlines', :units => 'Credits', :conversion => 1},
            {:name => 'Alaska Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'American Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'Continental Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'Delta Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'Frontier Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'Southwest Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'United Airlines', :units => 'Miles', :conversion => 1},
            {:name => 'US Airways', :units => 'Miles', :conversion => 1},
            {:name => 'Northwest Airlines', :units => 'Miles', :conversion => 1, :is_archived => true},
            {:name => 'Midwest Airlines', :units => 'Miles', :conversion => 1, :is_archived => true},
            {:name => 'Alitalia Airlines', :units => 'Miles', :conversion => 1, :is_archived => true},
            {:name => 'America West Airlines', :units => 'Miles', :conversion => 1, :is_archived => true},
            {:name => 'Meridiana Airlines', :units => 'Miles', :conversion => 1, :is_archived => true}]

Airline.create! airlines

if File.exists? "#{Rails.root}/lib/HeroMilesApplication.pdf"
  application = File.open "#{Rails.root}/lib/HeroMilesApplication.pdf"
else
  application = nil
end

as = AppSettings.create! :blank_application => application
