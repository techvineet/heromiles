module ValidUserHelper
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = FactoryGirl.create(:user)
      sign_in user
    end
  end
end

module ValidUserRequestHelper
  def login_user
    before(:each) do
      user = FactoryGirl.create(:user)
      post_via_redirect user_session_path, 'user[email]' => user.email, 'user[password]' => user.password
    end
  end
end

RSpec.configure do |config|
  config.include Devise::TestHelpers, :type => :controller
  config.extend ValidUserHelper, :type => :controller
  config.extend ValidUserRequestHelper, :type => :request
end
