FactoryGirl.define do
  factory :group do |g|
    g.name 'Group'
    g.association :organization
    g.fields_data  "{\"information_collected_by\":\"\",\"referrer_first_name\":\"\",\"referrer_last_name\":\"\",\"referrer_job_title\":\"\",\"referrer_medical_center_id\":\"\",\"referrer_organization_id\":\"\",\"referrer_phone_1\":\"\",\"referrer_phone_2\":\"\",\"referrer_phone_3\":\"\",\"referrer_email\":\"\",\"first_name\":\"Vineet\",\"last_name\":\"Sethi\",\"gender\":\"Male\",\"phone_1\":\"\",\"phone_2\":\"\",\"phone_3\":\"\",\"alt_phone_1\":\"\",\"alt_phone_2\":\"\",\"alt_phone_3\":\"\",\"email\":\"\",\"injury_after_911\":\"\",\"service_branch\":\"\",\"current_status\":\"\",\"is_inpatient\":\"\",\"injury_date\":\"\",\"has_used_heromiles\":\"\",\"last_use_date\":\"\",\"flight_type\":\"\",\"depart_from_1_id\":\"\",\"depart_from_2_id\":\"\",\"arrive_in_1_id\":\"\",\"arrive_in_2_id\":\"\",\"depart_from_1_select\":\"\",\"depart_from_2_select\":\"\",\"arrive_in_1_select\":\"\",\"arrive_in_2_select\":\"\",\"departure_date\":\"\",\"return_date\":\"\",\"flight_details\":\"\",\"special_request\":\"\",\"travellers\":\"\"}"
  end
end
