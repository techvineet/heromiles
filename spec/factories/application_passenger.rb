FactoryGirl.define do
  factory :application_passenger do |a|
    a.first_name 'Vineet'
    a.last_name 'Sethi'
    a.phone_1 '123'
    a.phone_2 '456'
    a.phone_3 '7890'
    a.association :application
  end
end
