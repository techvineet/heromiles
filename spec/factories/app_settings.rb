FactoryGirl.define  do
  factory :app_settings do |app|
    app.max_passengers 6
    app.max_flights 6
    app.blank_application_file_name  'HeroMilesApplication.pdf'
    app.blank_application_content_type 'application/pdf'
    app.blank_application_file_size '471608'
    app.highlight_hours 48
  end
end
