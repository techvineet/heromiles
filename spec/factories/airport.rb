FactoryGirl.define do
  factory :airport do |a|
    a.code 'US'
    a.name 'us'
    a.area 'uuuu'
    a.location 'kkkkk'
    a.country 'United States'
    a.is_archived false
  end
end
