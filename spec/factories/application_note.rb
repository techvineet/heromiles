FactoryGirl.define do
  factory :application_note do |a|
    a.message 'hello'
    a.association :application
    a.association :user
  end
end
