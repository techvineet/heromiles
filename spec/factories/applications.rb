FactoryGirl.define do
  factory :application do |a|
    a.first_name 'Vineet'
    a.last_name 'Sethi'
    a.phone_1 '123'
    a.phone_2 '456'
    a.phone_3 '7890'
    a.service_branch 'Air Force'
    a.current_status 'Active Duty'
    a.injury_date '2010-12-6'
    a.depart_from_1_id '1'
    a.arrive_in_1_id '2'
    a.departure_date '2011-12-6'
    a.injury_after_911 true
    a.has_used_heromiles '0'
    a.is_inpatient '1'
    a.travellers 'Family/Friend'
    a.association :group
    a.association :user
    a.association :organization
    a.is_exception true
  end
end
