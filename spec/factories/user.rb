FactoryGirl.define do
  factory :user do |u|
    u.first_name 'Lester'
    u.last_name 'Freamon'
    u.phone_1 '123'
    u.phone_2 '345'
    u.phone_3 '4232'
    u.association :organization
    u.email 'lester.freamon@email.com'
    u.password 'password'
  end
end
