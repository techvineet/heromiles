Feature: User Authentication
  In order to use the HeroMiles Application System
  Registered case-workers and administrators must be able to login

  Scenario: Empty Email
    Given I am on the sign in page
    When I fill in "user_email" with ""
    And I press "Sign in"
    Then I should see "We were unable to find an account with those credentials. Please try again."
    And I am on the sign in page

  Scenario: Invalid Email format
    Given I am on the sign in page
    When I fill in "user_email" with "email"
    And I press "Sign in"
    Then I should see "We were unable to find an account with those credentials. Please try again."
    And I am on the sign in page  

  Scenario: Invalid Account
    Given I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    Then I should see "We were unable to find an account with those credentials. Please try again."
    And I am on the sign in page 

  Scenario: Invalid Password
    Given I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    Then I should see "We were unable to find an account with those credentials. Please try again."
    And I am on the sign in page 

  Scenario: Valid Details but pending approval
    Given a user exists with email "test@user.com" and password "password"
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    Then I should see "Your account has not been activated yet"
    And I am on the sign in page 

  Scenario: Valid Details but expired account
    Given a user exists with email "test@user.com" and password "password"
    And the user account is approved
    And the user account is expired
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    Then I should see "Your account has expired"
    And I am on the sign in page 

  Scenario: Valid Details - Case worker
    Given a user exists with email "test@user.com" and password "password"
    And the user account is approved
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    And I am on the case worker applications page 

  Scenario: Valid Details - Admin
    Given the user "admin" has register with email "test@user.com"
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    And I am on the application management page
 
  Scenario: Failed Attempt with 5 times wrong credentials 
    Given a user exists with email "test@user.com" and password "password"
    And the user account is approved
    Given the user account with failed attempts 4
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "p"
    And I press "Sign in"
    Then I should see "Your account has been temporarily suspended due to wrong password attempts."
    And I am on the sign in page 

  Scenario: Expried Account Message after Failed Attempt
    Given a user exists with email "test@user.com" and password "password"
    And the user account is approved
    Given the user account with failed attempts 5
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    And I fill in "user_password" with "password"
    And I press "Sign in"
    Then I should see "Your account has expired"
    And I am on the sign in page 

  Scenario: Checking Mail For failed attempt functionality
    Given the user "admin" has register with email "admin@user.com"
    Given a user exists with email "test@user.com" and password "password"
    And the user account is approved
    Given the user account with failed attempts 4
    And I am on the sign in page
    When I fill in "user_email" with "test@user.com"
    When I fill in "user_password" with "t"
    And I press "Sign in"
    Then I should see "Your account has been temporarily suspended due to wrong password attempts."
    When "test@user.com" opens the email with subject "Hero Miles Account Security"
    When "admin@user.com" opens the email with subject "Hero Miles Account Security"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "The user account with login email test@user.com has temporarily been set to an expired status because of 5 incorrect password entries. An administrator can make the account active again, and reset the password if needed." in the email body
