Given /^I should see "(.*?)" to "(.*?)" Monthly Wise report for each Year$/ do |mon1, mon2|
  if page.respond_to? :should
    page.should have_content(mon1)
    page.should have_content(mon1)
  else
    assert page.has_content?(mon1)
    assert page.has_content?(mon2)
  end
end

Given /^I should see "(.*?)" to "(.*?)" Monthly Wise Airline report for the Year 2009$/ do |mon1, mon2|
  if page.respond_to? :should
    page.should have_content(mon1)
    page.should have_content(mon1)
  else
    assert page.has_content?(mon1)
    assert page.has_content?(mon2)
  end
end

Given /^I should see "(.*?)" for each Year$/ do |text|
  if page.respond_to? :should
    page.should have_content(text)
  else
    assert page.has_content?(text)
  end
end

Given /^I should see "(.*?)" grand total of all "(.*?)"$/ do |field1, field2|
  if page.respond_to? :should
    page.should have_content(field1)
    page.should have_content(field2)
  else
    assert page.has_content?(field1)
    assert page.has_content?(field2)
  end
end


Given /^the summary report page$/ do 
	visit summary_report_path(:year_from => "2009" , :year_till => "2011")
end

Given /^the branch report page$/ do 
	visit  branch_report_path(:year => "2009" , :type=>"ticket" , :branch =>"branch")
end

Then /^I should get a download with the filename "([^\"]*)"$/ do |filename|
  page.response_headers['Content-Disposition'].should include("filename=\"#{filename}\"")
end
