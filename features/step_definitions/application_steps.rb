Given /^there is an administrative notice "([^"]*)"$/ do |text|
  settings = AppSettings.find_or_create_by_id 1
  settings.update_attribute :applications_notice, text
end

Given /^there are no groups$/ do
  Group.delete_all
end

Given /^an existing group "([^"]*)"$/ do |gname|
  @group = FactoryGirl.create :group, :name => gname
end

Given /^there are ([\d]+) applications in the group$/ do |num|
  num.to_i.times do |i|
    FactoryGirl.create(:application, :user_id => @user.id, :group_id => @group.id)
  end
end

Given /^there is an open "([^\"]*)" application for "([^\"]*)"$/ do |state, user|
  fname =  user.split(" ")[0]
  lname =  user.split(" ")[1]
  user = FactoryGirl.create(:user, :first_name => user.split(" ")[0], :last_name => user.split(" ")[1])
  application = FactoryGirl.create(:application, :state => state, :user_id => @user.id, :first_name => fname, :last_name => lname, :email => "#{fname}app0@gmail.com",:last_action_by_admin=>0)
end

Given /^there are ([\d]+) open "([^\"]*)" applications "([^\"]*)" in the group$/ do |num,state, user|
  fname =  user.split(" ")[0]
  lname =  user.split(" ")[1]
  num.to_i.times do |i|
    application = FactoryGirl.create(:application, :state => state, :first_name => fname, :last_name => lname,
                         :email => "#{fname}app#{i}@gmail.com", 
                         :user_id=>@user.id, :service_branch=>"Army",:organization_id=>@user.organization_id,
                          :is_inpatient=>0, :has_used_heromiles=>0, :last_action_by_admin=>1, :flight_type=>"One-                way", :depart_from_1_id=>"1", :arrive_in_1_id=>"2", :departure_date=>"2012-06-06", :return_date=>"2012-07-07")
  end
end

def handle_js_confirm(accept=true)
  page.evaluate_script "window.original_confirm_function = window.confirm"
  page.evaluate_script "window.confirm = function(msg) { return #{!!accept}; }"
  yield
  ensure
  page.evaluate_script "window.confirm = window.original_confirm_function"
end

When /^(.*) and (\S*) popup?$/ do |step_as,confirm_dismiss|
    handle_js_confirm(confirm_dismiss == "confirm") { step step_as }
end

When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end

When /^I click view option on application for "([^"]*)"$/ do |email|
  @application = Application.find_by_email email
    within "#application_#{@application.id}" do
      click_link "view"
    end
end

Given 'the Sphinx indexes are updated' do
  # Update all indexes
  ThinkingSphinx::Test.index
  sleep(0.25) # Wait for Sphinx to catch up
end

When /^I type in "([^\"]*)" into autocomplete list "([^\"]*)" and I choose "([^\"]*)"$/ do |typed, input_name,should_select|
   page.driver.browser.execute_script %Q{ $('input[data-autocomplete]').trigger("focus") }
   fill_in("#{input_name}",:with => typed)
   page.driver.browser.execute_script %Q{ $('input[data-autocomplete]').trigger("keydown") }
   sleep 1
   page.driver.browser.execute_script %Q{ $('.ui-menu-item a:contains("#{should_select}")').trigger("mouseenter").trigger("click"); }
end

Given /^I am in application page for "([^"]*)"$/ do |email|
  @application = Application.find_by_email email
  visit case_worker_show_application_path(@application)
end

Then /^Fill the valid application fields for "([^\"]*)"$/ do |user|
  fname =  user.split(" ")[0]
  lname =  user.split(" ")[1]
    steps %Q{
     And I choose "Service Member"
     When I choose "One-way"
     And I fill in "depart_from_1_select" with "UK"
     And I fill in "arrive_in_1_select" with "US"
     And I fill in "application_email" with "#{fname}app1@gmail.com"
     When I fill in the following:
     | application_first_name | fname       |
     | application_last_name    | lname |
     | application_phone_1           | 123   |
     | application_phone_2   | 123    |
     | application_phone_3   | 123    |
     | application_flight_details   | kk    |
     | application_application_passengers_attributes_0_city   | le    |
     | application_application_passengers_attributes_0_email   | Mal@gmail.com   |
     When I select in the following:
     | application_information_collected_by | entirely by myself       |
     | application_service_branch    | Army |
     | application_current_status           | Active Duty   |
     | application_departure_date_2i   | June    |
     | application_departure_date_3i   | 6    |
     | application_departure_date_1i   | 2012    |
     | application_return_date_2i   | July    |
     | application_return_date_3i   | 6    |
     | application_return_date_1i   | 2012    |
     | application_application_passengers_attributes_0_date_of_birth_2i   | June    |
     | application_application_passengers_attributes_0_date_of_birth_3i   | 12    |
     | application_application_passengers_attributes_0_date_of_birth_1i   | 1988    |
     | application_application_passengers_attributes_0_gender   | Male    |
     | application_application_passengers_attributes_0_state   | AL    |
  
    And I choose "DTR"
    And I choose "Inpatient"
    And I choose "Wounded"
    And I choose "application_has_used_heromiles_false"
}
end

Given /^there is user "([^\"]*)" with email "([^\"]*)" has an "([^\"]*)" application for "([^\"]*)" with note "([^\"]*)"$/ do |admin,email,state, user,msg|

  admin_flag = (admin=="admin")? true : false
  FactoryGirl.create :user, :email => email, :password => 'password', :is_admin => admin_flag, :is_active => true, :receive_notifications=>1
  FactoryGirl.create :app_settings
  fname =  user.split(" ")[0]
  lname =  user.split(" ")[1]
  user = FactoryGirl.create(:user, :first_name => user.split(" ")[0], :last_name => user.split(" ")[1])
  application = FactoryGirl.create(:application, :state => state, :user_id => @user.id, :admin_id=>user.id, :first_name => fname, :last_name => lname, :email => "#{fname}app0@gmail.com",:last_action_by_admin=>0)
  application_note = FactoryGirl.create(:application_note, :user_id => @user.id, :application_id=>application.id, :is_action=>0)
end

Given /^Modify "([^\"]*)" of first ([\d]+) applications with ([\d]+) days ago from now and change application name "([^\"]*)"$/ do |field,lmt,day,name| 
   fname = name.split(" ")[0]
   lname =  name.split(" ")[1]
   apps = Application.limit(lmt)
   Application.update_all("departure_date = DATE(NOW() - INTERVAL #{day.to_i}+1 DAY),updated_at = DATE(NOW() - INTERVAL #{day.to_i}+1 DAY)")
   apps.update_all("first_name = '#{fname}',last_name = '#{lname}', #{field} = DATE(NOW() - INTERVAL #{day.to_i} DAY)") 
end

Given /^Modify "([^\"]*)" for this application with "([^\"]*)"$/ do |field,value|
   apps = Application.last
   apps.update_attribute("#{field}","#{value}") 
end


