Given /^there is an medical center "([^"]*)"$/ do |org|
  @org = FactoryGirl.create :medical_center, :name => org
end
