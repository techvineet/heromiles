Given /^there is a pending (.+) account "([^"]*)"$/ do |type, email|
  @user = FactoryGirl.create :user, :email => email, :job_title => "title"
  if type == 'admin' || type == 'administrator'
    @user.is_admin = true and @user.save
  end
end

Given /^there is an active (.+) account "([^"]*)"$/ do |type, email|
  @user = FactoryGirl.create :user, :email => email, :job_title => "title"
  @user.is_active = true and @user.save

  if type == 'admin' || type == 'administrator'
    @user.is_admin = true and @user.save
  end
end

When /^I click view (.+) account "([^"]*)"$/ do |type, email|
  @user = User.find_by_email email
  within "##{type}-accounts" do
    within "#user_#{@user.id}" do
      click_link "view"
    end
  end
end

Then /^I should be in the account page for "([^"]*)"$/ do |email|
  @user = User.find_by_email email
  current_path.should == account_path(@user)
end

Given /^I am in account page for "([^"]*)"$/ do |email|
  @user = User.find_by_email email
  visit account_path(@user)
end

Given /^I visit account page for "([^"]*)"$/ do |email|
  @user = User.find_by_email email
  visit account_path(@user)
end

When /^I click 'PRINT\/EXPORT' (.+) accounts$/ do |type|
  within "##{type}-accounts" do
    click_link 'PRINT/EXPORT'
  end
end

Then /^I should download a PDF file$/ do

end
