Given /^there is an organisation "([^"]*)"$/ do |org|
  @org = FactoryGirl.create :organization, :name => org
end
