Given /^a user exists with email "([^"]*)" and password "([^"]*)"$/ do |email, password|
  @user = FactoryGirl.create :user, :email => email, :password => password
end

Given /^the user account is (.+)$/ do |status|
  @user ||= FactoryGirl.create :user, :email => email, :password => password
  case status
    when 'expired'
      @user.expires_at = Time.zone.now - 1.minute and @user.save
    when 'approved'
      @user.is_active = true and @user.save
    else
  end
end

Given /^the active user name is (.+)$/ do |name|
  @user = FactoryGirl.create :user, :email => "#{name}@gmail.com", :first_name=>name.gsub!(/\A"|"\Z/, '')

  @user.is_active = true and @user.save
end

Given /^there are ([\d]+) active accounts with name (.+)$/ do |num, name|
    first_name = name.gsub!(/\A"|"\Z/, '') 
    num.to_i.times do |n|
      @user = FactoryGirl.create :user, :first_name=>first_name, :email => "email#{n}@gmail.com", :phone_1=>"#{n}11",
 :phone_2=>"#{n}22", :phone_3=>"#{n}333"
      @user.is_active = true and @user.save
    end
end

Given /^the user is an administrator$/ do
  @user ||= FactoryGirl.create :user, :email => email, :password => password
  @user.is_admin = true and @user.save
end

Given /^the admin has logged in$/ do 
  @user = FactoryGirl.create :user, :email => 'admin@heromiles.com', :password => 'password', :is_admin => true, :is_active => true, :receive_notifications=>1
  FactoryGirl.create :app_settings
  
  visit sign_in_path
  fill_in('user_email', :with => 'admin@heromiles.com')
  fill_in('user_password', :with => 'password')
  click_button("Sign in")
end

Given /^the user has logged out$/ do
  visit sign_out_path
end

Given /^the user "([^"]*)" has register with email "([^"]*)"$/ do |admin, email|
  admin_flag = (admin=="admin")? true : false
  FactoryGirl.create :user, :email => email, :password => 'password', :is_admin => admin_flag, :is_active => true, :receive_notifications=>1
  FactoryGirl.create :app_settings
end

Given /^the user account with failed attempts (.+)$/ do |count|
  @user ||= FactoryGirl.create :user, :email => email, :password => password
  @user.failed_attempts = count 
  @user.expires_at = Time.zone.now - 1.minute
  @user.save
end

Given /^the user is no receive notification$/ do
  @user ||= FactoryGirl.create :user, :email => email, :password => password
  @user.receive_notifications = false and @user.save
end
