@javascript
Feature: Account Management
  In order to create, edit, view and delete users in the HeroMiles Application System
  Administrators must be able to manage accounts

  Scenario: View a pending account
    Given the admin has logged in
    Given there is a pending case-worker account "pending@test.com"
    And I am on the account management page
    When I click view pending account "pending@test.com"
    Then I should be in the account page for "pending@test.com"


  Scenario: Update a pending account
    Given the admin has logged in
    Given there is a pending case-worker account "pending@test.com"
    And I visit account page for "pending@test.com"
    When I fill in "user_email" with "user2@test.com"
    And I press "Update"
    Then I should see "Account updated successfully"

  Scenario: Approve a pending account
    Given the admin has logged in
    Given there is a pending case-worker account "pending@test.com"
    And I am in account page for "pending@test.com"
    When I press "Approve"
    Then I should see "Account approved successfully"

  Scenario: Delete a pending account
    Given the admin has logged in
    Given there is a pending case-worker account "pending@test.com"
    And I am in account page for "pending@test.com"
    When I follow "Delete"
    Then I should see "Account deleted successfully"

  Scenario: View an active account
    Given the admin has logged in
    Given there is an active case-worker account "active@test.com"
    And I am on the account management page
    When I click view active account "active@test.com"
    Then I should be in the account page for "active@test.com"

  Scenario: Update an active account
    Given the admin has logged in
    Given there is an active admin account "active@test.com"
    And I visit account page for "active@test.com"
    When I fill in "user_email" with "admin2@test.com"
    And I press "Update"
    Then I should see "Account updated successfully"
    
  Scenario: Delete an active account
    Given the admin has logged in
    Given there is an active case-worker account "active@test.com"
    And I am in account page for "active@test.com"
    When I follow "Delete"
    Then I should see "Account deleted successfully"

  Scenario: Create a case-worker account
    Given the admin has logged in
    Given there is an organisation "Bayside Health"
    And I am on the account management page
    When I follow "Create New Account"
    Then I should be on the new account page
    When I select "Case Worker" from "user_is_admin"
    And I select "Bayside Health" from "user_organization_id"
    And I fill in "user_first_name" with "Marlo"
    And I fill in "user_last_name" with "Stanfield"
    And I fill in "user_job_title" with "Lt."
    And I fill in "user_phone_1" with "129"
    And I fill in "user_phone_2" with "343"
    And I fill in "user_phone_3" with "2332"
    And I fill in "user_email" with "marlo@stanfield.org"
    And I fill in "user_password" with "password"
    And I fill in "user_password_confirmation" with "password"
    And I press "Create!"
    Then I should see "Case Worker Account created successfully"

  Scenario: Create an administrator account
    Given the admin has logged in
    Given there is an organisation "Bayside Health"
    And I am on the account management page
    When I follow "Create New Account"
    Then I should be on the new account page
    When I select "Administrator" from "user_is_admin"
    And I select "Bayside Health" from "user_organization_id"
    And I fill in "user_first_name" with "James"
    And I fill in "user_last_name" with "McNulty"
    And I fill in "user_job_title" with "Maj. Gen."
    And I fill in "user_phone_1" with "198"
    And I fill in "user_phone_2" with "765"
    And I fill in "user_phone_3" with "4328"
    And I fill in "user_email" with "jimmy@fh.org"
    And I fill in "user_password" with "password"
    And I fill in "user_password_confirmation" with "password"
    And I press "Create!"
    Then I should see "Administrator Account created successfully"

  Scenario: Download Pending accounts list as PDF
    Given the admin has logged in
    Given I am on the account management page
    And there is a pending case-worker account "pending@test.com"
    When I click 'PRINT/EXPORT' pending accounts
    Then I should see "Pending accounts - Print/Export Options"
    When I follow "Download as PDF"
    Then I should get a download with the filename "PendingAccounts.pdf"

  Scenario: Download Active accounts list as PDF
    Given the admin has logged in
    Given I am on the account management page
    And there is an active admin account "active@test.com"
    When I click 'PRINT/EXPORT' active accounts
    Then I should see "Accounts - Print/Export Options"
    When I follow "Download as PDF"
    Then I should get a download with the filename "ActiveAccounts.pdf"

  Scenario: Email Existing Active accounts list as PDF
    Given the admin has logged in
    Given I am on the account management page
    And there is an active admin account "active@test.com"
    When I click 'PRINT/EXPORT' active accounts
    Then I should see "Accounts - Print/Export Options"
    Then I fill in "pdf_email_p_" with "test@test.com"
    When I press "Email as PDF"
    Then I should see "Email sent successfully"

 Scenario: Not email sent for empty email as Email as PDF
    Given the admin has logged in
    Given I am on the account management page
    And there is an active admin account "active@test.com"
    When I click 'PRINT/EXPORT' active accounts
    Then I should see "Accounts - Print/Export Options"
    Then I fill in "pdf_email_p_" with ""
    When I press "Email as PDF"
    Then I should not see "Email sent successfully"
 
  Scenario: Email Existing Pending accounts list as PDF
    Given the admin has logged in
    Given I am on the account management page
    And there is a pending case-worker account "pending@test.com"
    When I click 'PRINT/EXPORT' pending accounts
    Then I should see "Pending accounts - Print/Export Options"
    Then I fill in "pdf_email_p_" with "test@test.com"
    When I press "Email as PDF"
    Then I should see "Email sent successfully"
 
  Scenario: Change accounts password with valid confirmation
    Given the admin has logged in
    Given there is an active admin account "active@test.com"
    And I visit account page for "active@test.com"
    When I fill in "user_password" with "123456"
    When I fill in "user_password_confirmation" with "123456"
    And I press "Update"
    Then I should see "Account updated successfully"

  Scenario: Change accounts password with invalid confirmation
    Given the admin has logged in
    Given there is an active admin account "active@test.com"
    And I visit account page for "active@test.com"
    When I fill in "user_password" with "123456"
    When I fill in "user_password_confirmation" with "123457"
    And I press "Update"
    Then I should see "Password doesn't match confirmation"

  Scenario: Change accounts password with blank password
    Given the admin has logged in
    Given there is an active admin account "active@test.com"
    And I visit account page for "active@test.com"
    When I fill in "user_password_confirmation" with "123457"
    And I press "Update"
    Then I should see "Password can't be blank and Password doesn't match confirmation"

  Scenario: Change accounts password 
    Given the admin has logged in
    Given there is an active admin account "active@test.com"
    And I visit account page for "active@test.com"
    When I fill in "user_password" with "123457"
    And I press "Update"
    Then I should see "Password doesn't match confirmation"

  Scenario: Fields For listing of records
    Given the admin has logged in
    Given there is a pending case-worker account "pending@test.com"
    And I am on the account management page
    Then I should see "Name"
    Then I should see "Medical Center / Organization"
    Then I should see "Email"
    Then I should see "Type"
    Then I should see "Expires On"

  Scenario: Update Information page for admin
    Given the admin has logged in
    And I am on the account management page
    Then I should see "Update Information"
    Then I follow "Update Information"
    Then I should see "Administrator Information"
    Then I fill in "user_job_title" with "job" 
    And I press "Update"
    Then I should see "Your account has been updated successfully." within "div.success"

  Scenario: Sign Out for admin
    Given the admin has logged in
    And I am on the account management page
    Then I should see "Sign Out"
    When I follow "Sign Out"
    Then I am on the sign in page

  Scenario: Update Information page for case worker
    Given the case_worker has logged in
    Then I should see "ACCOUNT"
    When I follow "ACCOUNT" 
    Then I should see "Update Information"
    Then I follow "Update Information"
    Then I should see "Case Worker Information"
    Then I fill in "user_job_title" with "jobk" 
    And I press "Update"
    Then I should see "Your account has been updated successfully." within "div.success"

  Scenario: Sign Out for case worker
    Given the case_worker has logged in
    When I follow "ACCOUNT" 
    Then I should see "Sign Out"
    When I follow "Sign Out"
    Then I am on the sign in page  

  Scenario: Checking Mail for activation email
    Given the admin has logged in
    Given there is a pending case-worker account "pending@test.com"
    And I am in account page for "pending@test.com"
    When I press "Approve"
    Then I should see "Account approved successfully"
    When "pending@test.com" opens the email with subject "Hero Miles Account Activation"
    Then I should see "We are pleased to confirm that your Hero Miles account has been approved and is now active." in the email body
    Then I should see the email delivered from "admin@fisherhouse.org"  

  Scenario: checking Mail for active accounts email
        Given the admin has logged in
	Given I am on the account management page
	And there is an active admin account "active@test.com"
	When I click 'PRINT/EXPORT' active accounts
	Then I fill in "pdf_email_p_" with "test@test.com"
	When I press "Email as PDF"
        Then I should see "Email sent successfully"
        When "test@test.com" opens the email with subject "List of Active Hero Miles Accounts"
        Then I should see it is a multi-part email
        Then I should see the email delivered from "admin@fisherhouse.org"
        Then attachment 1 should be named "ActiveAccounts.pdf"  
      
  Scenario: Checking Mail for pending accounts email
        Given the admin has logged in
	Given I am on the account management page
	And there is a pending case-worker account "pending@test.com"
	When I click 'PRINT/EXPORT' pending accounts
	Then I should see "Pending accounts - Print/Export Options"
	Then I fill in "pdf_email_p_" with "test@test.com"
	When I press "Email as PDF"
        Then I should see "Email sent successfully"
	When "test@test.com" opens the email with subject "List of Pending Hero Miles Accounts"
        Then I should see it is a multi-part email
        Then I should see the email delivered from "admin@fisherhouse.org"
        Then attachment 1 should be named "PendingAccounts.pdf"

  Scenario: Accounts should be ordered alphabetically by name
       Given the admin has logged in
       Given there are 1 active accounts with name "Tom"
       Given I am on the account management page
       Then I should see table "table#active-accounts-table tbody tr" with 2
       Then I should see "Lester Freamon" within "table#active-accounts-table tbody tr:nth-child(1)"
       Then I should see "Tom" within "table#active-accounts-table tbody tr:nth-child(2)" 
