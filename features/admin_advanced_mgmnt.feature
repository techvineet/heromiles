@javascript

Feature: Admin Advanced Management
	  In order to manage organisations/medical centers/airlines/airports/configurations
	  As an administrator
	  He/she should be able to create/update/delete them

	Background:
  		Given the admin has logged in
  
	Scenario: Organizations Management Page
		Given I am on the organization management page
		Then I should see "Manage Organizations"
		And I should see "Add an Organization"
		And I should see "Update or Delete an Organization"
		And I should see "Add"
		And I should see "Update"
		And I should see "Delete" 
		
	Scenario: Add Organization
		Given I am on the organization management page
		When I fill in "organization_name" with "Nestle Inc."
		And I fill in "organization_abbreviation" with "HM"
		And I fill in "organization_city" with "Vikas Puri"
		And I select "GA | Georgia" from "organization_state"
		And I press "Add"
		Then I should see "Organization created successfully."
		And I should see "Nestle Inc." within "div#update_organization"
		
	Scenario: Edit Organization
		Given I am on the organization management page
		When I fill in "organization_name" with "Nestle Inc."
		And I fill in "organization_abbreviation" with "HM"
		And I fill in "organization_city" with "Vikas Puri"
		And I select "GA | Georgia" from "organization_state"
		And I press "Add"
		And I select "Nestle Inc." from "name"
		Then the "organization_abbreviation" field should contain "HM" within "div#update_organization"
		And the "organization_city" field should contain "Vikas Puri" within "div#update_organization"
		And I should see "GA | Georgia" within "div#update_organization"
                And the "organization_is_archived" checkbox should not be checked within "div#update_organization"
		
	Scenario: Update Organization
		Given I am on the organization management page
		When I fill in "organization_name" with "Nestle Inc."
		And I fill in "organization_abbreviation" with "HM"
		And I fill in "organization_city" with "Vikas Puri"
		And I select "GA | Georgia" from "organization_state"
		And I press "Add"
		And I select "Nestle Inc." from "name"
		And I fill in "organization_abbreviation" with "KM" within "div#update_organization"
		And I fill in "organization_city" with "Tilak Nagar" within "div#update_organization"
                Then I check "organization_is_archived"
		And I follow "update_link"
		And I select "Nestle Inc." from "name"
		Then I should see "Organization 'Nestle Inc.' updated successfully."
		And the "organization_abbreviation" field should contain "KM" within "div#update_organization"
		And the "organization_city" field should contain "Tilak Nagar" within "div#update_organization"
                And the "organization_is_archived" checkbox should be checked within "div#update_organization"
		
	Scenario: Delete Organization
		Given I am on the organization management page
		When I fill in "organization_name" with "Nestle Inc."
		And I fill in "organization_abbreviation" with "HM"
		And I fill in "organization_city" with "Vikas Puri"
		And I select "GA | Georgia" from "organization_state"
		And I press "Add"
		And I select "Nestle Inc." from "name"
		And I follow "destroy_link"
		Then I should see "Organization 'Nestle Inc.' deleted successfully."
		And I should not see "Nestle Inc." within "div#update_organization"
		
	Scenario: Medical Center Management Page
		Given I am on the medical center management page
		Then I should see "Manage Medical Centers"
		And I should see "Add a Medical Center"
		And I should see "Update or Delete a Medical Center"
		And I should see "Add"
		And I should see "Update"
		And I should see "Delete" 
		
	Scenario: Add Medical Center 
		Given I am on the medical center management page
		When I fill in "medical_center_name" with "JF Kennedy MC"
		And I fill in "medical_center_abbreviation" with "JK"
		And I fill in "medical_center_city" with "Vikas Puri"
		And I select "GA | Georgia" from "medical_center_state"
		And I press "Add"
		Then I should see "Medical Center created successfully."
		And I should see "JF Kennedy MC" within "div#update_medical_center"
		
	Scenario: Edit Medical Center 
		Given I am on the medical center management page
		When I fill in "medical_center_name" with "JF Kennedy MC"
		And I fill in "medical_center_abbreviation" with "JK"
		And I fill in "medical_center_city" with "Vikas Puri"
		And I select "GA | Georgia" from "medical_center_state"
		And I press "Add"
		And I select "JF Kennedy MC" from "name"
		Then the "medical_center_abbreviation" field should contain "JK" within "div#update_medical_center"
		And the "medical_center_city" field should contain "Vikas Puri" within "div#update_medical_center"
		And I should see "GA | Georgia" within "div#update_medical_center"
                And the "medical_center_is_archived" checkbox should not be checked within "div#update_medical_center"
		
	Scenario: Update Medical Center
		Given I am on the medical center management page
		When I fill in "medical_center_name" with "JF Kennedy MC"
		And I fill in "medical_center_abbreviation" with "JK"
		And I fill in "medical_center_city" with "Vikas Puri"
		And I select "GA | Georgia" from "medical_center_state"
		And I press "Add"
		And I select "JF Kennedy MC" from "name"
		And I fill in "medical_center_abbreviation" with "KM" within "div#update_medical_center"
		And I fill in "medical_center_city" with "Tilak Nagar" within "div#update_medical_center"
                Then I check "medical_center_is_archived"
		And I follow "update_link"
		And I select "JF Kennedy MC" from "name"
		Then I should see "Medical Center 'JF Kennedy MC' updated successfully."
		And the "medical_center_abbreviation" field should contain "KM" within "div#update_medical_center"
		And the "medical_center_city" field should contain "Tilak Nagar" within "div#update_medical_center"
		And the "medical_center_is_archived" checkbox should be checked within "div#update_medical_center"

	Scenario: Delete Medical center
		Given I am on the medical center management page
		When I fill in "medical_center_name" with "JF Kennedy MC"
		And I fill in "medical_center_abbreviation" with "JK"
		And I fill in "medical_center_city" with "Vikas Puri"
		And I select "GA | Georgia" from "medical_center_state"
		And I press "Add"
		And I select "JF Kennedy MC" from "name"
		And I follow "destroy_link"
		Then I should see "Medical Center 'JF Kennedy MC' deleted successfully."
		And I should not see "JF Kennedy MC" within "div#update_medical_center"
		
	Scenario: Airlines Management Page
		Given I am on the airline management page
		Then I should see "Manage Airline List"
		And I should see "Add an Airline"
		And I should see "Update or Delete an Airline"
		And I should see "Add"
		And I should see "Update"
		And I should see "Delete" 
		
	Scenario: Add Airline
		Given I am on the airline management page
		When I fill in "airline_name" with "Kingfisher Airlines"
		And I fill in "airline_units" with "20"
		And I fill in "airline_conversion" with "26"
		And I press "Add"
		Then I should see "Airline created successfully."
		And I should see "Kingfisher Airlines" within "div#update_airline"
		
	Scenario: Edit Airline
		Given I am on the airline management page
		When I fill in "airline_name" with "Kingfisher Airlines"
		And I fill in "airline_units" with "20"
		And I fill in "airline_conversion" with "26"
		And I press "Add"
		And I select "Kingfisher Airlines" from "name"
		Then the "airline_units" field should contain "20" within "div#update_airline"
		And the "airline_conversion" field should contain "26" within "div#update_airline"
                And the "airline_is_archived" checkbox should not be checked within "div#update_airline"
		
	Scenario: Update Airline
		Given I am on the airline management page
		When I fill in "airline_name" with "Kingfisher Airlines"
		And I fill in "airline_units" with "20"
		And I fill in "airline_conversion" with "26"
		And I press "Add"
		And I select "Kingfisher Airlines" from "name"
		And I fill in "airline_units" with "10" within "div#update_airline"
		And I fill in "airline_conversion" with "15" within "div#update_airline"
                And I check "airline_is_archived"
		And I follow "update_link"
		And I select "Kingfisher Airlines" from "name"
		Then I should see "Airline 'Kingfisher Airlines' updated successfully."
		And the "airline_units" field should contain "10" within "div#update_airline"
		And the "airline_conversion" field should contain "15" within "div#update_airline"
                And the "airline_is_archived" checkbox should be checked within "div#update_airline"
		
	Scenario: Delete Airline
		Given I am on the airline management page
		When I fill in "airline_name" with "Kingfisher Airlines"
		And I fill in "airline_units" with "20"
		And I fill in "airline_conversion" with "26"
		And I press "Add"
		And I select "Kingfisher Airlines" from "name"
		And I follow "destroy_link"
		Then I should see "Airline 'Kingfisher Airlines' deleted successfully."
		And I should not see "Kingfisher Airlines" within "div#update_airline"
		
	Scenario: Airport Management Page
		Given I am on the airport management page
		Then I should see "Manage Airport List"
		And I should see "Add an Airport"
		And I should see "Update or Delete an Airport"
		And I should see "Add"
		And I should see "Update"
		And I should see "Delete" 
		
	Scenario: Add Airport
		Given I am on the airport management page
		When I fill in "airport_name" with "IGI Airport"
		And I fill in "airport_code" with "20"
		And I fill in "airport_location" with "Vikas Puri"
		And I fill in "airport_area" with "New Delhi"
		And I fill in "airport_country" with "India"
		And I press "Add"
		Then I should see "Airport created successfully."
		And I should see "Update or Delete an Airport" within "div#update_airport"
		
	Scenario: Edit Airport
		Given I am on the airport management page
                When I fill in "airport_name" with "UK"
		And I fill in "airport_code" with "UK"
		And I fill in "airport_location" with "UK"
		And I fill in "airport_area" with "UK"
		And I fill in "airport_country" with "UK"
		And I press "Add"
                Given the Sphinx indexes are updated  
                Given I am on the airport management page 
                When I type in "UK" into autocomplete list "airport_name" and I choose "UK - UK, UK, UK - UK"
                And I wait for 2 seconds
                #And the "airport_code" field should contain "UK" within "div#update_airport" 
		
	Scenario: Settings Page
		Given I am on the settings page
		Then I should see "Application Settings"
		And I should see "Maximum No. of Passengers per application:"
                And I should see "Maximum No. of Flights per application:"
                And I should see "Highlight upcoming departures"
		
	Scenario: Update application settings
		Given I am on the settings page
		When I fill in "app_settings_max_passengers" with "1"
		And I fill in "app_settings_max_flights" with "2"
		And I fill in "app_settings_highlight_hours" with "26"
		And I press "Save"
		Then the "app_settings_max_passengers" field should contain "1"
		And the "app_settings_max_flights" field should contain "2"
		And the "app_settings_highlight_hours" field should contain "26"
		
	Scenario: Attachment Page
                Given I am on the attachments page
		And I should see "Add an Attachment Category"
                And I should see "Update or Delete an Attachment Category"

        Scenario: Add new category with no checkbox
		Given I am on the attachments page
		When I fill in "category_name" with "soya souce"
                And I select "Admin tab" from "display_on"
                And I select "Yes" from "is_required"
		And I press "Add Category"
		Then I should see "Category added successfully"
		And I should see "soya souce" within "div#update_attachment"
		
	Scenario: Add invalid category
		Given I am on the attachments page
		When I fill in "category_name" with ""
		And I press "Add Category"
		Then I should see CSS "p.validation-failed input#category_name"
			
	Scenario: Edit category
		Given I am on the attachments page
		When I fill in "category_name" with "soya souce"
                And I select "Admin tab" from "display_on"
                And I select "Yes" from "is_required"
		And I press "Add Category"
		And I select "soya souce" from "category_id"
		And I should see "Admin tab" within "div#update_attachment"
                And I should see "Yes" within "div#update_attachment"  
                And the "is_archived" checkbox should not be checked within "div#update_attachment"
		
	Scenario: Update category
		Given I am on the attachments page
		When I fill in "category_name" with "soya souce"
                And I select "Admin tab" from "display_on"
                And I select "Yes" from "is_required"
		And I press "Add Category"
		And I select "soya souce" from "category_id"
		And I select "No" from "is_required"
                Then I check "is_archived"
		And I press "Update Category"
		And I select "soya souce" from "category_id"
		Then I should see "Category updated successfully"
		And I should see "Admin tab" within "div#update_attachment"
                And I should see "No" within "div#update_attachment"  
                And the "is_archived" checkbox should be checked within "div#update_attachment"
		
	Scenario: Delete category
		Given I am on the attachments page
		When I fill in "category_name" with "soya souce"
                And I select "Admin tab" from "display_on"
                And I select "Yes" from "is_required"
		And I press "Add Category"
		And I select "soya souce" from "category_id"
		And I follow "destroy_link"
		Then I should see "Category deleted successfully"
		And I should not see "soya souce" within "div#update_attachment"
		
		
		

