@javascript

Feature: Case Worker Groups
	  In order to get groups 
	  As an case worker
	 

	Background:
  		Given the case_worker has logged in
  		
            Scenario: Case Worker Group Management Page
		Given I am on the case_worker_groups page
		Then I should see "Group Management"
		And I should see "Available Groups"
		And I should see "Options"
		And I should see "Group Name"
		And I should see "Created"

            Scenario: Listing group application on Case Worker Group Management Page
                Given an existing group "Group"
                And there are 2 applications in the group
                Given I am on the case_worker_groups page
                Then I should see "applications" 
                Then I should see "Group" 
          	   
	    Scenario: Case Worker Group Applications Page
                Given an existing group "Group"
                And there are 1 applications in the group
                Given I am on the case_worker_groups page
                And I follow "applications"
	  	Then I should see "Application Entry"
	        And I should see "Departing"
	        And I should see "Service Member"
	        And I should see "Status"
	        And I should see "Created"
	        And I should see "Submitted"
	        And I should see "Completed"
	        And I should see "PRINT/EXPORT"
	        And I should see "view"
	        And I should see Button "Add an Application"

           Scenario: Group application template
		Given an existing group "Group1"
		And an existing group "Group2"
		Given I am on the case_worker_groups page
		When I click "applications" under "tr#group_2"
		And I follow "Add an Application"
		Then I should see "New Application"
		And the "application_first_name" field should contain "Vineet"
		And the "application_last_name" field should contain "Sethi"

	   Scenario: Group add application to see applications list
		Given an existing group "Group1"
		And an existing group "Group2"
		Given I am on the case_worker_groups page
		When I click "applications" under "tr#group_2"
		And I follow "Add an Application"
		And I press "Save"
		Given I am on the case_worker_groups page
		And I click "applications" under "tr#group_2"
		Then I should see "Sethi, Vineet"
		And I should see "Saved"
		And I should see "view"

	   Scenario: View application for a group
		Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the case_worker_groups page
		When I click "applications" under "tr#group_2"
		And I follow "Add an Application"
		And I press "Save"
		And I am on the case_worker_groups page
		And I click "applications" under "tr#group_2"
		And I follow "view"
		Then I should see "View Application"
		And I should see "Case Worker Information"
		And I should see "Service Member Information"
		And I should see "Flight Information"
		And I should see "Passenger Information"
		And I should see "Terms of Use"

	   Scenario: Paginate applications
                Given an existing group "Group"
                And there are 12 applications in the group
                And I am on the case_worker_groups page
                When I click "applications" under "tr#group_1"
                Then I should see "1 2 Next › Last"
                And I follow "2"
                Then I should see "view" within "tr#application_12"
                And I should see "Sethi, Vineet" within "tr#application_12"
        
           Scenario: View Monthly Report In PDF Format
		Given an existing group "Group"
                And there are 2 applications in the group
		And I am on the case_worker_groups page
		And I click "applications" under "tr#group_1"
		And I follow "PRINT/EXPORT"
	  	Then I should see Button "Print Application"
	  	Then I should see Button "Download as PDF"
	  	Then I should see Button "Email as PDF"
	  	Then I should see "Email"
		
           Scenario: Download Applications list as PDF
		Given an existing group "Group1"
		And I am on the case_worker_groups page
		When I click "applications" under "tr#group_1"
		And I follow "Add an Application"
		And I press "Save"
		And I am on the case_worker_groups page
		And I click "applications" under "tr#group_1"
		And I follow "PRINT/EXPORT"
		Then I should see "Group Applications - Open/Export options"
	        When I follow "Download as PDF"
	        Then I should get a download with the filename "GroupApplications.pdf"

           Scenario: Email Applications list as PDF for valid email address
	        Given an existing group "Group"
                And there are 2 applications in the group
                And I am on the case_worker_groups page
                When I click "applications" under "tr#group_1"
                And I follow "PRINT/EXPORT"
                Then I fill in "pdf_email_p_" with "aa@gmail.com"
                When I press "Email as PDF"
                Then I should see "Email sent successfully"
 
           Scenario: Email Applications list as PDF for invalid email address
	        Given an existing group "Group"
                And there are 2 applications in the group
                And I am on the case_worker_groups page
                When I click "applications" under "tr#group_1"
                And I follow "PRINT/EXPORT"
                Then I fill in "pdf_email_p_" with "aa"
                When I press "Email as PDF"
                Then I should see CSS "div.validation-failed"
		
	   Scenario: Checking Mail Applications for group application
	        Given an existing group "Group"
                And there are 2 applications in the group
                And I am on the case_worker_groups page
                When I click "applications" under "tr#group_1"
                And I follow "PRINT/EXPORT"
                Then I fill in "pdf_email_p_" with "aa@gmail.com"
                When I press "Email as PDF"
                Then I should see "Email sent successfully"
                When "aa@gmail.com" opens the email with subject "List of Group Applications"
        	Then I should see it is a multi-part email
        	Then I should see the email delivered from "admin@fisherhouse.org"
        	Then attachment 1 should be named "GroupApplications.pdf"
		
		
