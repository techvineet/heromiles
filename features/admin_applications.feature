Feature: Applications Administration
  In order to administer submitted Hero Miles applications
  Administrators must be able to view, modify and change the status of submitted applications

  
  Scenario: Show details on application page
    Given the admin has logged in
    Given I am on the application management page
    Then I should see "Search" within "div#search"
    Then I should see Button "New Application"
    Then I should see Button "Update"
    Then I should see "Open Applications" within "div#open-applications"

  Scenario: Set Notice
    Given the admin has logged in
    Given I am on the application management page
    When I follow "Update"
    And I fill in "app_settings_applications_notice" with "Lorem Ipsum Dolor Sit Amet"
    And I press "Save"
    And I go to the case worker applications page
    Then I should see "Lorem Ipsum Dolor Sit Amet"
  
  Scenario: View Open Applications details with no application
    Given the admin has logged in
    Given I am on the application management page
    And I should see "Departure Date"
    And I should see "Service Member"
    And I should see "Status"
    And I should see "Group"
    And I should see "Admin"
    And I should see "Medical Center / Organization"
    Then I should see table "table#open-applications-table tbody tr" with 0

  Scenario: View Open Applications listing with applications
    Given the admin has logged in
    Given there are 2 open "exception" applications "Alexander Mattos" in the group
    And I am on the application management page
    Then I should see "Mattos, Alexander" within "div#open-applications"
    Then I should see table "table#open-applications-table tbody tr" with 2

  Scenario: Click View option in open applications to show application 
    Given the admin has logged in
    Given there are 1 open "exception" applications "Alexander Mattos" in the group
    And I am on the application management page
    Then I should see "Mattos, Alexander" within "div#open-applications"
    Then I should see "view" within "div#open-applications"
    When I follow "view"
    Then I should see "View Application"

  Scenario: Download Open Applications for all data csv 
    Given the admin has logged in
    Given there are 1 open "exception" applications "Alexander Mattos" in the group
    And I am on the application management page
    When I follow "PRINT/EXPORT"
    Then I should see "Open Applications (PDF)"
    When I follow "save-all-data-link"
    Then I should get a download with the filename "OpenApplications.csv"

  Scenario: View Notifications details 
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    When I am on the case worker applications page
    When I follow "view"
    Then I should see Button "Add a note"
    Then I follow "Add a Note"
    Then I fill in "application_note_message" with "value added by user"  
    Then I follow "Add"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    And I should see "Departure Date" within "div#notifications"
    And I should see "Service Member" within "div#notifications"
    And I should see "Status" within "div#notifications"
    And I should see "Group" within "div#notifications"
    And I should see "Admin" within "div#notifications"
    And I should see "Medical Center / Organization" within "div#notifications"
    And I should see "Note Sent" within "div#notifications"
  
  Scenario: Check List of case worker inquiry and comment
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    When I am on the case worker applications page
    When I follow "view"
    Then I should see Button "Add a note"
    Then I follow "Add a Note"
    Then I fill in "application_note_message" with "value added by user"  
    Then I follow "Add"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    Then I should see "Case Worker Inquiries/Comments" within "div#notifications"
    Then I should see "Mattos, Alexander" within "div#notifications"
    Then I should see table "table#notifications-table tbody tr" with 1

 Scenario: Click View option in Notification to show application 
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    When I am on the case worker applications page
    When I follow "view"
    Then I should see Button "Add a note"
    Then I follow "Add a Note"
    Then I fill in "application_note_message" with "value added by user"  
    Then I follow "Add"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    Then I should see "Mattos, Alexander" within "div#notifications"
    Then I should see "view" within "div#notifications"
    When I follow "view"
    Then I should see "View Application"

  Scenario: Download Notifications for all data csv 
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    When I am on the case worker applications page
    When I follow "view"
    Then I should see Button "Add a note"
    Then I follow "Add a Note"
    Then I fill in "application_note_message" with "value added by user"  
    Then I follow "Add"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    Then I should see "Mattos, Alexander" within "div#notifications"
    When I follow "PRINT/EXPORT" under "div#notifications"
    Then I should see "Notifications (PDF)"
    When I follow "save-all-data-link" under "div#export-notifications-dialog"
    Then I should get a download with the filename "NotificationsExport.csv"
 
  Scenario: Checking Mail for Set Notice
    Given the admin has logged in
    Given I am on the application management page
    When I follow "Update"
    And I fill in "app_settings_applications_notice" with "Lorem Ipsum Dolor Sit Amet"
    And I press "Save"
    When "heromiles@fisherhouse.org" opens the email with subject "Hero Miles system announcement updated"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "has updated the Hero Miles system announcement:" in the email body

  Scenario: Checking Mail for Completed case worker application state for Itinerary mail
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    When I follow "view"
    Then I press "Complete"
    Then I should see "Application completed successfully"
    When "rahulindia25@gmail.com" opens the email with subject "Hero Miles Application Itinerary"
    Then I should see "A copy of the Hero Miles application itinerary is attached" in the email body
    Then I should see it is a multi-part email
    Then attachment 1 should be named "HeroMilesItinerary-.pdf" 

  Scenario: Checking Mail for Returned the application of case worker
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    When I follow "view"
    Then I press "Return"
    Then I should see "Application returned successfully"
    When "rahulindia25@gmail.com" opens the email with subject "Hero Miles Application Status"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "Your Hero Miles application for Alexander Mattos has been Returned." in the email body

  Scenario: Checking Mail for Decline the application of case worker
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    When I follow "view"
    Then I press "Decline"
    Then I should see "Application declined successfully"
    When "rahulindia25@gmail.com" opens the email with subject "Hero Miles Application Status"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "Your Hero Miles application for Alexander Mattos has been Declined." in the email body

  Scenario: Checking Mail for Complete the application of case worker
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    When I follow "view"
    Then I press "Complete"
    Then I should see "Application completed successfully"
    When "rahulindia25@gmail.com" opens the email with subject "Hero Miles Application Status"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "Your Hero Miles application for Alexander Mattos has been Completed." in the email body

  Scenario: Checking Mail for case worker user for Note added by admin
    Given the admin has logged in
    Given there is user "case" with email "test@test.com" has an "submitted" application for "Alexander Mattos" with note "hello"
    When "admin@heromiles.com" opens the email with subject "Note added to Hero Miles Application"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "Alexander Mattos has added a note to your Hero Miles Application:" in the email body

  Scenario: Case Worker applications are listed in admin application page
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    Then I should see table "table#open-applications-table tbody tr" with 1
    Then I should see "Mattos, Alexander"

  Scenario: Case Worker applications are returned by admin not listed in admin application page
    Given the case_worker has logged in
    Given there is an open "submitted" application for "Alexander Mattos"
    Given the user has logged out
    Given the admin has logged in
    And I am on the application management page
    Then I should see table "table#open-applications-table tbody tr" with 1
    When I follow "view"
    Then I press "Return"
    Then I should see "Application returned successfully"
    And I am on the application management page
    Then I should see table "table#open-applications-table tbody tr" with 0
   
