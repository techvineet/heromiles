@javascript

Feature: Admin Search
	In order to search the users and applications in Hero Miles application for admin

	Background:
  		Given the admin has logged in
  	
  	#TODO: THERE SHOULD BE SCENARIO FOR EMPTY QUERY ALSO.
  	#TODO: ADD SCENARIO TO CHECK THAT ADVANCED SEARCH OPTION DOES NOT SHOW UP INITIALLY WHEN THE PAGE IS LOADED. AND CLICKING
  	#IT BACK AGAIN HIDES THE SEARCH OPTIONS.
  	#TODO: EVERY SCENARIO WHICH IS SHOWING UP "SEARCH RESULTS" SHOULD VERIFY SEARCH RESULTS (INCLUDING NO RESULT) ALSO.
  	#TODO: FOR ADVANCED USER SEARCH OPTION THE RESULTS SHOULD BE VERIFIED FOR COMBINATION OF CHECKBOX OPTIONS.
  	#TODO: WHENEVER THE MEDICAL CENTRE OR ORGANISATION IS CREATED WE SHOULD VERIFY THAT THEY ARE VISIBLE UNDER USER ADVANCED SERACH
  	#TODO: WHENEVER THE MEDICAL CENTRE/ORGANISATION/GROUP IS CREATED WE SHOULD VERIFY THAT THEY ARE VISIBLE UNDER ADVANCED APPLICATION SERACH
  	#TODO: ADD SCENARIO FOR VERIFYING THE PDF WHEN EXPORTED. CHECK THIS http://blog.bitcrowd.net/test-rails-pdf-output-with-cucumber/
  	#TODO: ADD SCENARIO TO VERIFY THE RESULTS WHEN PRINTED.
  	#TODO: ADD SCENARIO TO VERIFY PAGINATION FOR SEARCH RESULTS.  
  	
  	  
  	Scenario: Search button display on account management page
		Given I am on the account management page
		Then I should see "Search" 
		And I should see Button "Go!"

	Scenario: No result found from search query in account management page
 		Given the active user name is "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Search" 
		And I should see Button "Go!"
		When I fill in "q" with "kkkk"
		And I press "Go!"
		Then I should see "No Results found"
	
        Scenario: Searching particular user in account management page
		Given the active user name is "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
                Then I should see "Search"
                And I should see Button "Go!"
                When I fill in "q" with "Jom"
                And I press "Go!"
                Then I should see "Search Results for 'Jom'"
		Then I should see "Jom"
                Then I should see "view"
        
         Scenario: Unsearched user not found in searched list in account management page
		Given the active user name is "Jom"
                Given the active user name is "Dom"
                Given the Sphinx indexes are updated
                And I am on the account management page 
                Then I should see "Search" 
		And I should see Button "Go!"
                When I fill in "q" with "Jom"
		And I press "Go!"
		Then I should not see "Search Results for 'Dom'"
                Then I should not see "Dom"
         
         Scenario: Searching with blank will display all active user in account management page
 		Given there are 2 active accounts with name "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Search" 
		And I should see Button "Go!"
		When I fill in "q" with ""
		And I press "Go!"
		Then I should see "Displaying all 3 users"
	
         Scenario: Paginated Lists for searched result in account management page
                Given there are 13 active accounts with name "new"
                Given the Sphinx indexes are updated
                And I am on the account management page 
                Then I should see "Search" 
		And I should see Button "Go!"
                When I fill in "q" with "new"
                And I press "Go!"
                Then I should see "Displaying users 1–11 of 13 in total"
                Then I should see "Next"
                Then I should see "Last"
                When I follow "2"
                Then I should see "Displaying users 12–13 of 13 in total"
             
         Scenario: Advanced Searching options are in account management page
		Given I am on the account management page
		Then I should see "Advanced search" 
		When  I follow "advanced-search-link"
		Then I should see "Restrict searched fields to"
		And I should see "Medical Center"
		And I should see "Organization"
         
         Scenario: Advanced Search div class are hidden on page load
                Given I am on the account management page
                And I should see CSS "div#advanced_search.hidden"
                When I follow "advanced-search-link"
                And I should not see CSS "div#advanced_search.hidden" 
                
         Scenario: Searching the users using advance search option with restrict name
		Given there are 2 active accounts with name "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Advanced search" 
		Then I should see "Restrict searched fields to"
		When I check "restrict_name"
                When I fill in "q" with "Jom"
                When I press "Go!"
		Then I should see "Displaying all 2 users"
                Then I should see "Jom"
                
	 Scenario: Searching the users by other field when advance search option set to restrict name
		Given there are 2 active accounts with name "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Advanced search" 
		Then I should see "Restrict searched fields to"
		When I check "restrict_name"
                When I fill in "q" with "email0@gmail.com"
                When I press "Go!"
		Then I should see "No Results found"
                Then I should not see "Jom"
         
	Scenario: Searching the users using advance search option set to restrict email
		Given there are 2 active accounts with name "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Advanced search" 
		Then I should see "Restrict searched fields to"
		When I check "restrict_email"
                When I fill in "q" with "email0@gmail.com"
                When I press "Go!"
		Then I should see "Displaying 1 user"
                Then I should see "Jom"
                Then I should see "email0@gmail.com"
                Then I should not see "email1@gmail.com"
	
        Scenario: Searching the users by other fields advance search option set to restrict email
		Given there are 2 active accounts with name "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Advanced search" 
		Then I should see "Restrict searched fields to"
		When I check "restrict_email"
                When I fill in "q" with "011-022-0333"
                When I press "Go!"
		Then I should see "No Results found"

        Scenario: Searching the users using advance search set to more options
		Given there are 2 active accounts with name "Jom"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Advanced search" 
		Then I should see "Restrict searched fields to"
		When I check "restrict_email"
                When I check "restrict_name"
                When I check "restrict_phone"
                When I fill in "q" with "Jom"
                When I press "Go!"
		Then I should see "Displaying all 2 users"
                When I fill in "q" with "email0@gmail.com"
                When I press "Go!" 
                Then I should see "Displaying 1 user"
	        When I fill in "q" with "011"
                When I press "Go!" 
                Then I should see "Displaying 1 user"
	
         Scenario: Searching the user using advance search option restrict to active users
		Given there is a pending case-worker account "pending@test.com"
                Given the Sphinx indexes are updated
                And I am on the account management page
		Then I should see "Advanced search" 
		Then I should see "Restrict searched fields to"
                When I check "restrict_active"
		When I check "restrict_email"
                When I fill in "q" with "pending@test.com"
                When I press "Go!"
		Then I should see "No Results found"
          
          Scenario: Searching the user using advance search option with selected organization field
                Given there is an organisation "Bayside Health"
		Given there is an active case-worker account "active@test.com"
                Given the Sphinx indexes are updated
                And I visit account page for "active@test.com"
                And I select "Bayside Health" from "user_organization_id"
                And I press "Update"
                
                And I am on the account management page
		When I follow "advanced-search-link"
	        Then I should see "Bayside Health"
                When I check "restrict_email" 
                And I select "Bayside Health" from "organization"
                When I fill in "q" with "active@test.com"
                When I press "Go!"
		Then I should see "Displaying 1 user"
          
           Scenario: Searching the user using advance search option with selected medical center
                Given there is an medical center "Marry Center"
		Given there is an active case-worker account "active@test.com"
                Given the Sphinx indexes are updated
                And I visit account page for "active@test.com"
                And I select "Marry Center" from "user_medical_center_id"
                And I press "Update"
                
                And I am on the account management page
		When I follow "advanced-search-link"
	        Then I should see "Marry Center"
                When I check "restrict_email" 
                And I select "Marry Center" from "medical_center"
                When I fill in "q" with "active@test.com"
                When I press "Go!"
		Then I should see "Displaying 1 user"


           Scenario: Paginated Lists for advanced serach option in account management page
                Given there are 13 active accounts with name "new"
                Given the Sphinx indexes are updated
                And I am on the account management page 
                Then I should see "Advanced search" 
		When I check "restrict_email"
                When I check "restrict_name"
                Then I should see "Search" 
		And I should see Button "Go!"
                When I fill in "q" with "new"
                And I press "Go!"
                Then I should see "Displaying users 1–11 of 13 in total"
                Then I should see "Next"
                Then I should see "Last"
                When I follow "2"
                Then I should see "Displaying users 12–13 of 13 in total"
               
           Scenario: Export the users search results
		Given the active user name is "Jom"
                Given the Sphinx indexes are updated
                And I am on the accounts search page
		Then I should see "Search Results"
		And I should see "PRINT/EXPORT" 
		When  I follow "export-search-link"
		Then I should see "Print/Export Search Results"
		And I should see "view"
		And I should see "save"
		When I follow "save-export-link"
                Then I should get a download with the filename "SearchResults.pdf"
          
           Scenario: Print the users search results
		Given there is an active case-worker account "active@test.com"
                Given the Sphinx indexes are updated
                And I am on the account management page 
                When I check "restrict_email"
                When I fill in "q" with "active@test.com"
                And I press "Go!"
		Then I should see "Search Results"
		When  I follow "export-search-link"
		Then I should see "Print/Export Search Results"
		And I should see "view"
		When I follow "view-export-link"
                Then I am in account page for "active@test.com"
                

          Scenario: Search button display on application management page
		Given I am on the application management page
		Then I should see "Search"
                Then I should see "Advanced search"  
		And I should see Button "Go!"

	  Scenario: No result found from search query in application management page
                Given there are 1 open "saved" applications "Alexander Mattos" in the group
 		Given the Sphinx indexes are updated
                And I am on the application management page
		Then I should see "Search" 
		And I should see Button "Go!"
		When I fill in "q" with "kkkk"
		And I press "Go!"
		Then I should see "No Results found"
           
          Scenario: Searching particular application in application management page
		Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                Then I should see "Search"
                When I fill in "q" with "Alexander"
                And I press "Go!"
                Then I should see "Alexander"
		Then I should see "view"
        
          Scenario: Unsearched application not found in searched list in application management page
                Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given there are 1 open "saved" applications "app user" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                Then I should see "Search" 
		When I fill in "q" with "app"
		And I press "Go!"
		Then I should not see "Search Results for 'Alexander'"
                Then I should not see "Alexander"
         
         Scenario: Searching with blank will display all applications in application management page
 		Given there are 2 open "completed" applications "app1" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
		Then I should see "Search" 
		And I should see Button "Go!"
		When I fill in "q" with ""
		And I press "Go!"
		Then I should see "Displaying all 2 applications"
	
         Scenario: Paginated Lists for searched result in application management page
                Given there are 13 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                Then I should see "Search" 
		When I fill in "q" with "app"
                And I press "Go!"
                Then I should see "Displaying applications 1–11 of 13 in total"
                Then I should see "Next"
                Then I should see "Last"
                When I follow "2"
                Then I should see "Displaying applications 12–13 of 13 in total"

         Scenario: Export the applications search results
		Given there are 2 open "completed" applications "app1" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
		When I fill in "q" with ""
		And I press "Go!"
		And I should see "PRINT/EXPORT" 
		When I follow "export-search-link"
		Then I should see "Print/Export Search Results"
		And I should see "save"
		When I follow "save-all-data-link"
                Then I should get a download with the filename "SearchExport.csv"
          
         Scenario: Check Default setting for Advanced Search options are in application management page
		Given I am on the application management page
		Then I should see "Advanced search" 
		When  I follow "advanced-search-link"
		Then I should see "Restrict by Medical Center"
		And I should see "Restrict by Organization"
		And I should see "Restrict by Group"
		And I should see "Restrict by Branch of Service"
                
		And I should see "Restrict searched status to"
                Then the "completed" checkbox should be checked
                Then the "submitted" checkbox should be checked
                Then the "exceptions" checkbox should be checked
                Then the "returned" checkbox should not be checked
		Then the "reopened" checkbox should not be checked                
		Then the "declined" checkbox should not be checked  
		Then the "saved" checkbox should not be checked

		And I should see "Restrict searched fields to"
		And I should see "Restrict searched dates to"
		And I should see "Fields to Display"
                Then the "field_departing" checkbox should be checked
                Then the "field_service_member" checkbox should be checked
                Then the "field_status" checkbox should be checked
                Then the "field_created" checkbox should be checked
                Then the "field_completed" checkbox should be checked
                Then the "field_case_worker" checkbox should be checked
                Then the "field_medical_center" checkbox should be checked 

          Scenario: Searching applications in application management page with restrict to other serached status
		Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given there are 1 open "saved" applications "Alex Matt" in the group
                Given there are 1 open "declined" applications "Axander Mttos" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I check "declined"
                When I uncheck "exceptions"
                When I fill in "q" with ""
                And I press "Go!"
                Then I should see "Displaying all 2 applications"
		Then I should see "Saved"
                Then I should see "Declined"
        
          Scenario: Searching applications in application management page with searched restrict to
		Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given there are 1 open "completed" applications "Alex Matt" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "service_member"
                When I check "email"
                When I fill in "q" with "Alexander"
                And I press "Go!"
                Then I should see "Exception"
                When I fill in "q" with "Alexapp0@gmail.com" 
		And I press "Go!"
                Then I should see "Completed"

           Scenario: Searching applications in application management page with restrict to branch of service   
                Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I select "Air Force" from "branch"
                When I fill in "q" with "Alexander"
                And I press "Go!"
                Then I should see "No Results found"
                
                When I select "Army" from "branch"
                When I fill in "q" with "Alexander"
                And I press "Go!"
                Then I should see "Displaying 1 application"
                
            Scenario: Searching applications in application management page with fields to be displayed
                Given there are 1 open "submitted" applications "Alexander Mattos" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When I follow "advanced-search-link"
                When I uncheck "field_service_member" 
                When I uncheck "field_status"
                When I fill in "q" with "Alexander"
                And I press "Go!"
                Then I should see "Displaying 1 application"
                And I should see "Submitted"
                
           Scenario: Paginated Lists for searched result in application management page using advanced search combination
                Given there are 13 open "saved" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "service_member"
                When I check "email"
                When I check "saved"
                When I select "Army" from "branch"
		When I fill in "q" with "app"
                And I press "Go!"
                Then I should see "Displaying applications 1–11 of 13 in total"
                Then I should see "Next"
                Then I should see "Last"
                When I follow "2"
                Then I should see "Displaying applications 12–13 of 13 in total"

           Scenario: Searching the applications using advance search option with selected organization field
		Given there are 1 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
		When I follow "advanced-search-link"
	        Then I should see "Baltimore General"
                When I select "Baltimore General" from "organization_id" 
                When I fill in "q" with "app"
                When I press "Go!"
		Then I should see "Displaying 1 application"

           Scenario: Searching applications in application management page with searched for request departure for restrict date to
		Given there are 1 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When I follow "advanced-search-link"
                When I select "June" from "_search_from_2i"
                When I select "2" from "_search_from_3i"
		When I select "2012" from "_search_from_1i"
		When I select "June" from "_search_till_2i"
                When I select "10" from "_search_till_3i"
		When I select "2012" from "_search_till_1i"
                When I select "Requested Departure" from "date_for"
                When I fill in "q" with "app"
                When I press "Go!"
                Then I should see "Displaying 1 application"

           Scenario: Searching applications in application management page with searched for arrival for restrict date to
		Given there are 1 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When I follow "advanced-search-link"
                When I select "July" from "_search_from_2i"
                When I select "1" from "_search_from_3i"
		When I select "2012" from "_search_from_1i"
		When I select "July" from "_search_till_2i"
                When I select "20" from "_search_till_3i"
		When I select "2012" from "_search_till_1i"
                When I select "Requested Arrival" from "date_for"
                When I fill in "q" with "app"
                When I press "Go!"
                Then I should see "Displaying 1 application"

          Scenario: Searching applications in application management page with check service member clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "AJACK AZIMMERMAN" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on service member name
                When I clicking sortable link "td:nth-child(2)" under "table thead"
                When I clicking sortable link "td:nth-child(3)" under "table thead" 
                Then I should see "ZIMMERMAN, JEREMY" within "table tbody tr:nth-child(1)"
                Then I should see "ZIMMERMAN, JACK" within "table tbody tr:nth-child(2)"
                Then I should see "AZIMMERMAN, AJACK" within "table tbody tr:nth-child(4)"
                When I clicking sortable link "td:nth-child(3)" under "table thead" 
                Then I should see "AZIMMERMAN, AJACK" within "table tbody tr:nth-child(1)"
                Then I should see "ZIMMERMAN, JEREMY" within "table tbody tr:nth-child(4)" 
                    
          Scenario: Searching applications in application management page with check departing clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "final_departure_date" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "final_departure_date" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "final_departure_date" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on departing date
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(2)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                When I clicking sortable link "td:nth-child(2)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)" 

         Scenario: Searching applications in application management page with check case worker name column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "case_worker_last_name" for this application with "User"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "case_worker_last_name" for this application with "Ore"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "case_worker_last_name" for this application with "Voull"
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on case worker name
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(8)" under "table thead"
                Then I should see "Lester Voull" within "table tbody tr:nth-child(1)"
                Then I should see "Lester User" within "table tbody tr:nth-child(2)"
                Then I should see "Lester Ore" within "table tbody tr:nth-child(3)"
                When I clicking sortable link "td:nth-child(8)" under "table thead"
                Then I should see "Lester Ore" within "table tbody tr:nth-child(1)"
                Then I should see "Lester Voull" within "table tbody tr:nth-child(3)"

         Scenario: Searching applications in application management page with check status clickable column
		Given there are 1 open "exception" applications "JACK ZIMMERMAN" in the group
                Given there are 1 open "saved" applications "JEREMY ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on status
                When I clicking sortable link "td:nth-child(3)" under "table thead" 
                When I clicking sortable link "td:nth-child(4)" under "table thead"
                Then I should see "Saved" within "table tbody tr:nth-child(1)"
                Then I should see "Exception" within "table tbody tr:nth-child(2)"
                When I clicking sortable link "td:nth-child(4)" under "table thead"
                Then I should see "Completed" within "table tbody tr:nth-child(1)"
                Then I should see "Exception" within "table tbody tr:nth-child(2)"

         Scenario: Searching applications in application management page with check created_at clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "created_at" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "created_at" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "created_at" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on created_at
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(5)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                 When I clicking sortable link "td:nth-child(5)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)" 

        Scenario: Searching applications in application management page with check booked_at clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "booked_at" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "booked_at" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "booked_at" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on booked_at
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(7)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                 When I clicking sortable link "td:nth-child(7)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)"

        Scenario: Searching applications in application management page with check submitted_at clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "submitted_at" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "submitted_at" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "submitted_at" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the application management page
                When  I follow "advanced-search-link"
                When I check "field_submitted"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on booked_at
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(6)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                 When I clicking sortable link "td:nth-child(6)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)"


