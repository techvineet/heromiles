@javascript
Feature: Case-Worker Application Management
  In order to participate and track applications under Hero Miles
  Registered case-workers must be able to track submitted applications

	Background:
  		Given the case_worker has logged in
  		
  Scenario: View Notice
    Given there is an administrative notice "All applications must be submitted by Thursday (7/7) if they are to be processed before 7/11!"
    When I go to the case worker applications page
    Then I should see "All applications must be submitted by Thursday (7/7) if they are to be processed before 7/11!"
  
  Scenario: Show details on application page
    When I am on the case worker applications page
    Then I should see "Search" within "div#search"
    Then I should see Button "New Application"
    Then I should see "Open Applications" within "div#open-applications"
    Then I should see "Notifications" within "div#notifications"

  Scenario: View Open Applications details with no application for case worker
    Given I am on the case worker applications page
    And I should see "Departure Date"
    And I should see "Service Member"
    And I should see "Status"
    And I should see "Created"
    Then I should see table "table#open-applications-table tbody tr" with 0

  Scenario: View Open Applications listing with case worker applications
    Given there is an open "saved" application for "Alexander Mattos"
    When I am on the case worker applications page
    Then I should see "Mattos, Alexander" within "div#open-applications"
    Then I should see table "table#open-applications-table tbody tr" with 1

  Scenario: Download Open Applications list as PDF
    Given there is an open "saved" application for "Alexander Mattos"
    And I am on the case worker applications page
    When I follow "PRINT/EXPORT" 
    Then I should see "Open Applications - Print/Export Options"
    When I follow "save-export-link" under "div#export-open-dialog"
    Then I should get a download with the filename "OpenApplications.pdf"
  
  Scenario: Click View option in open applications to show application 
    Given there is an open "saved" application for "Alexander Mattos"
    When I am on the case worker applications page
    Then I should see "Mattos, Alexander" within "div#open-applications"
    Then I should see "view" within "div#open-applications"
    When I follow "view"
    Then I should see "View Application"

  Scenario: View Notifications details with no application notification for case worker
    Given I am on the case worker applications page
    And I should see "Departure Date" within "div#notifications"
    And I should see "Service Member" within "div#notifications"
    And I should see "Status" within "div#notifications"
    And I should see "Admin Note" within "div#notifications"
    Then I should see table "table#notifications-table tbody tr" with 0
   
  Scenario: View Notifications listing 
    Given there are 1 open "exception" applications "Alexander Mattos" in the group
    When I am on the case worker applications page
    Then I should see "Mattos, Alexander" within "div#notifications"
    Then I should see table "table#notifications-table tbody tr" with 1

  Scenario: Click View option in notifications to show application 
    Given there are 1 open "exception" applications "Alexander Mattos" in the group
    When I am on the case worker applications page
    Then I should see "Mattos, Alexander" within "div#notifications"
    Then I should see "view" within "div#notifications"
    When I follow "view"
    Then I should see "View Application"

  Scenario: Download Notification list as PDF
    Given there are 1 open "exception" applications "Alexander Mattos" in the group
    And I am on the case worker applications page
    When I follow "PRINT/EXPORT" under "div#notifications"
    Then I should see "Notifications - Print/Export Options"
    When I follow "save-export-link" under "div#export-notifications-dialog"
    Then I should get a download with the filename "Notifications.pdf"
  
  Scenario: Deleted application not shown on listing page
    Given there are 1 open "saved" applications "Alexander Mattos" in the group
    When I am on the case worker applications page
    Then I should see table "table#open-applications-table tbody tr" with 1
    When I am in application page for "Alexanderapp0@gmail.com"
    Then I should see Button "Delete"
    Then I follow "Delete"
    Then I should see "Application deleted successfully"
    When I am on the case worker applications page
    Then I should see table "table#open-applications-table tbody tr" with 0

  Scenario: Answered/Unanswered Note
    When I am on the case worker applications page
    Then I should see "Unanswered Note" within "div#open-applications"
    Then I should see "Answered Note" within "div#open-applications"
    Then I should see "Unanswered Note" within "div#notifications"
    Then I should see "Answered Note" within "div#notifications"

  Scenario: Answered/Unanswered Note and coloured css listing in notification list 
    Given there are 1 open "exception" applications "Alexander Mattos" in the group
    When I am on the case worker applications page
    Then I should see "Mattos, Alexander" within "div#notifications"
    Then I should see "view" within "div#notifications"
    When I follow "view"
    Then I follow "Add a Note"
    Then I should see "Note" 
    Then I fill in "application_note_message" with "value added by user"  
    Then I press "Add"
    When I am on the case worker applications page
    Then I should see "Unanswered Note" within "div#notifications"
    Then I should see "Answered Note" within "div#notifications"
    Then I should see table "table#notifications-table tbody tr" with 1
    Then I should see CSS "table#notifications-table tbody tr.case-worker-note" 
     
  Scenario: Checking Mail for admin user for Note added by user
    Given there is user "admin" with email "test@test.com" has an "submitted" application for "Alexander Mattos" with note "hello"
    When "lester.freamon@email.com" opens the email with subject "Note added to Hero Miles Application"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "Lester Freamon has added a note to their Hero Miles Application:" in the email body

  Scenario: Check Notification box limit is 10 when updated at and departure date limit less than 10
    Given there are 12 open "exception" applications "Alexander Mattos" in the group
    Given Modify "departure_date" of first 2 applications with 29 days ago from now and change application name "test user"
    When I am on the case worker applications page
    Then I should see table "table#notifications-table tbody tr" with 10
    Then I should see "user, test" within "table#notifications-table tbody tr:nth-child(1)"
    Then I should see "user, test" within "table#notifications-table tbody tr:nth-child(2)"
    Then I should see "Mattos, Alexander" within "table#notifications-table tbody tr:nth-child(3)"

  Scenario: Check Notification box limit is departure date limit size 
    Given there are 12 open "exception" applications "Alexander Mattos" in the group
    Given Modify "departure_date" of first 11 applications with 29 days ago from now and change application name "test user"
    When I am on the case worker applications page
    Then I should see table "table#notifications-table tbody tr" with 11
    Then I should not see "Mattos, Alexander" within "div#notifications"
    Then I should see "user, test" within "div#notifications"

  Scenario: Check Notification box limit is updated at limit size 
    Given there are 15 open "exception" applications "Alexander Mattos" in the group
    Given Modify "updated_at" of first 13 applications with 29 days ago from now and change application name "test user"
    When I am on the case worker applications page
    Then I should see table "table#notifications-table tbody tr" with 13
    Then I should not see "Mattos, Alexander" within "div#notifications"
    Then I should see "user, test" within "div#notifications"

 Scenario: Check Open application box limit is 10
    Given there are 15 open "saved" applications "Alexander Mattos" in the group
    When I am on the case worker applications page
    Then I should see table "table#open-applications-table tbody tr" with 10

 
