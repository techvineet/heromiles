@javascript
Feature: Case-Worker Application Submission
  In order to participate and track applications under Hero Miles
  Registered case-workers must be able to submit new applictions

	Background:
  		Given the case_worker has logged in
  		
  	# all scenarios are failing in case worker	
  Scenario: Get to New Application page
     Given I am on the case worker applications page
     When I follow "New Application"
     Then I should be on the case worker new application page
     And I should see "New Application"

  Scenario: Empty Legal First Name
    Given I am on the case worker new application page
    When I fill in "application_first_name" with ""
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "First name" within "div.user-notice"

  Scenario: Empty Legal Last Name
    Given I am on the case worker new application page
    When I fill in "application_last_name" with ""
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Last name" within "div.user-notice"

  Scenario: Empty Phone No
    Given I am on the case worker new application page
    When I fill in "application_phone_1" with ""
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Phone" within "div.user-notice"
  
  Scenario: Empty Service member information was collected
    Given I am on the case worker new application page
    When I select "" from "application_information_collected_by"
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Service member information was collected" within "div.user-notice"

  Scenario: Choose referrer from Service member information was collected
    Given I am on the case worker new application page
    When I select "partly or entirely by someone else" from "application_information_collected_by"
    When I fill in "application_referrer_first_name" with ""
    When I fill in "application_referrer_job_title" with ""
    When I fill in "application_referrer_phone_1" with ""
    When I fill in "application_referrer_email" with ""
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Referrer first name" within "div.user-notice"
    Then I should see "Referrer job title" within "div.user-notice"
    Then I should see "Referrer Phone" within "div.user-notice"
    Then I should see "Referrer email" within "div.user-notice"
 
  Scenario: Empty Branch of Service
    Given I am on the case worker new application page
    When I select "" from "application_service_branch"
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Branch of Service" within "div.user-notice"

  Scenario: Empty Current Status
    Given I am on the case worker new application page
    When I select "" from "application_current_status"
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Current status" within "div.user-notice"

  Scenario: Empty Medical Center/Organization
    Given I am on the case worker new application page
    When I select "" from "application_medical_center_id"
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Medical Center" within "div.user-notice"

  Scenario: No choice selected on new application form
    Given I am on the case worker new application page
    And I press "Submit"
    Then I should see "Submitted as an exception"
    Then I should see "Patient Status" within "div.user-notice"
    Then I should see "Has the Service Member Used Hero Miles Before" within "div.user-notice"
    Then I should see "Injury status" within "div.user-notice"
    Then I should see "Who Will Be Travelling" within "div.user-notice"

  Scenario: Choose both for travelling
    Given I am on the case worker new application page
    When I choose "Both"
    Then I should see "2" within "select#passenger_count"
    
  Scenario: Passenger field required
    Given I am on the case worker new application page
    When I choose "Family/Friend"
    When I select "1" from "passenger_count" 
    And I press "Submit"
    Then I should see "Passenger 1 First name" within "div.user-notice"
    Then I should see "Passenger 1 Date of Birth" within "div.user-notice"
    Then I should see "Passenger 1 Gender" within "div.user-notice"
    Then I should see "Passenger 1 Phone" within "div.user-notice"
    Then I should see "Passenger 1 Email" within "div.user-notice"
    Then I should see "Passenger 1 Relation to Service Member" within "div.user-notice"

  Scenario: Change trip type to One-way
    Given I am on the case worker new application page
    When I choose "One-way"
    And I press "Submit"
    Then I should not see "Requested Date of Return" within "div.user-notice"

  Scenario: Change trip type to Round-trip
    Given I am on the case worker new application page
    When I choose "Round-trip"
    Then I should see "Outbound Flight"
    And I should see "Return Flight"
    And I should see "Requested Date of Return"

  Scenario: Valid Application - One-way
    Given there is an airport "UK" with "United States"
    Given there is an airport "US" with "United States"
    Given the Sphinx indexes are updated
    Given I am on the case worker new application page
    And I choose "Service Member"
    When I choose "One-way"
    And I fill in "depart_from_1_select" with "UK"
    And I fill in "arrive_in_1_select" with "US"

    When I fill in the following:
     | application_first_name | appfirst       |
     | application_last_name    | applast |
     | application_phone_1           | 123   |
     | application_phone_2   | 123    |
     | application_phone_3   | 123    |
     | application_flight_details   | kk    |
     | application_application_passengers_attributes_0_city   | le    |
     | application_application_passengers_attributes_0_email   | Mal@gmail.com   |
     
    When I select in the following:
     | application_information_collected_by | entirely by myself       |
     | application_service_branch    | Army |
     | application_current_status           | Active Duty   |
     | application_departure_date_2i   | June    |
     | application_departure_date_3i   | 6    |
     | application_departure_date_1i   | 2012    |
     | application_return_date_2i   | July    |
     | application_return_date_3i   | 6    |
     | application_return_date_1i   | 2012    |
     | application_application_passengers_attributes_0_date_of_birth_2i   | June    |
     | application_application_passengers_attributes_0_date_of_birth_3i   | 12    |
     | application_application_passengers_attributes_0_date_of_birth_1i   | 1988    |
     | application_application_passengers_attributes_0_gender   | Male    |
     | application_application_passengers_attributes_0_state   | AL    |
     
    And I choose "DTR"
    And I choose "Inpatient"
    And I choose "Wounded"
    And I choose "application_has_used_heromiles_false"
    And I press "Submit"
    Then I should see "Application submitted successfully"

  Scenario: Valid Application - Round Trip
    Given there is an airport "UK" with "United States"
    Given there is an airport "US" with "United States"
    Given the Sphinx indexes are updated  
    Given I am on the case worker new application page
    And I choose "Service Member"
    When I choose "Round-trip"
    And I fill in "depart_from_1_select" with "UK"
    And I fill in "arrive_in_1_select" with "US"
    And I fill in "depart_from_2_select" with "US"
    And I fill in "arrive_in_2_select" with "UK"
    When I fill in the following:
     | application_first_name | appfirst       |
     | application_last_name    | applast |
     | application_phone_1           | 123   |
     | application_phone_2   | 123    |
     | application_phone_3   | 123    |
     | application_flight_details   | kk    |
     | application_application_passengers_attributes_0_city   | le    |
     | application_application_passengers_attributes_0_email   | Mal@gmail.com   |
     
    When I select in the following:
     | application_information_collected_by | entirely by myself       |
     | application_service_branch    | Army |
     | application_current_status           | Active Duty   |
     | application_departure_date_2i   | June    |
     | application_departure_date_3i   | 6    |
     | application_departure_date_1i   | 2012    |
     | application_return_date_2i   | July    |
     | application_return_date_3i   | 6    |
     | application_return_date_1i   | 2012    |
     | application_application_passengers_attributes_0_date_of_birth_2i   | June    |
     | application_application_passengers_attributes_0_date_of_birth_3i   | 12    |
     | application_application_passengers_attributes_0_date_of_birth_1i   | 1988    |
     | application_application_passengers_attributes_0_gender   | Male    |
     | application_application_passengers_attributes_0_state   | AL    |
     
    And I choose "DTR"
    And I choose "Inpatient"
    And I choose "Wounded"
    And I choose "application_has_used_heromiles_false"
    And I press "Submit"
    Then I should see "Application submitted successfully"

  Scenario: View Functionality
    Given I am on the case worker new application page
    Then Fill the valid application fields for "App"
    And I press "Submit"
    Then I should see Button "View/Print/Save"
    Then I follow "View/Print/Save"
    Then I should see "View/Print/Save Application"
    Then I should see "Application - Case Worker Sections"
    When I follow "view"
    Then I should get a download with the filename "Application-.pdf"

  Scenario: Save Functionality
    Given I am on the case worker new application page
    Then Fill the valid application fields for "App"
    And I press "Submit"
    Then I follow "View/Print/Save"
    Then I should see "View/Print/Save Application"
    Then I should see "Application - Case Worker Sections"
    When I follow "save"
    Then I should get a download with the filename "Application-.pdf"
  
  Scenario: Add a note Functionality
    Given I am on the case worker new application page
    Then Fill the valid application fields for "App"
    And I press "Submit"
    Then I should see Button "Add a note"
    Then I follow "Add a Note"
    Then I should see "Note" 
    Then I fill in "application_note_message" with "value added by user"  
    Then I press "Add"
    Then I should see "value added by user"

  Scenario: All the actions displayed in notive_tab
    Given I am on the case worker new application page
    And I press "Save"
    Then Fill the valid application fields for "App"
    And I press "Submit"
    Then I follow "Add a Note"
    Then I should see "Note" 
    Then I fill in "application_note_message" with "value added by admin user"  
    Then I press "Add"
    Then I should see "Submitted as an exception by Lester Freamon" within "div#application-notes"
    Then I should see "Saved by Lester Freamon" within "div#application-notes"
    Then I should see "Note by Lester Freamon" within "div.top-notice"

  Scenario: Checking Mail for submit application by case worker
    Given the user "admin" has register with email "admin@test.com" 
    Given I am on the case worker new application page
    Then Fill the valid application fields for "App"
    And I press "Submit"
    Then I should see "Application submitted successfully"
    When "admin@test.com" opens the email with subject "Hero Miles Application Status"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "A Hero Miles application for Fname Lname has been Submitted as an Exception by Lester Freamon" in the email body
    
