@javascript

Feature: Admin Reports
	  In order to get reports such as Summary Report,Printable Blank Application,Monthly Report,Branch/Airline Reports 
	  As an administrator
	  He/she should be able to view/upload them

	Background:
  		Given the admin has logged in
  
	Scenario: Report Management Page
		Given I am on the report management page
		Then I should see "Reports"
		And I should see "Available Reports"
		And I should see "Options Report Name"
		And I should see "view"
		And I should see "upload"
		And I should see "Printable Blank Application"
		And I should see "Summary Report"
		And I should see "Monthly Report"
		And I should see "Branch/Airline Reports"
	
	Scenario: View and Upload Printable Blank Application
		Given I am on the report management page
		When  I follow "upload-blank-link"
		Then I should see "Upload Blank Application"
		And I should see "Upload a printable blank application:"
		And I should see "Upload"
		Then I attach the file "README" to "app_settings_blank_application"
		And I press "Upload"
		Then I should see "Blank Application uploaded successfully"
	        When I follow "view"
	        Then I should see "Welcome to Rails"

       Scenario: Summary Report with Blank Fields 
                Given I am on the report management page
		When I follow "summary-link"
		And I should see Button "Generate Report"
		And I press "Generate Report"
                Then I should see CSS "div.validation-failed"
		
        Scenario: View Summary Report
		Given I am on the report management page
		When I follow "summary-link"
		Then I should see "Summary Report"
		And I should see "Start Year"
		And I should see "End Year"
		And I should see "Medical Center"
		And I should see "Organization"
		And I should see Button "Generate Report"
		When I select "2009" from "year_from"
		And I select "2011" from "year_till"
		And I select "All" from "medical_center"
		And I select "All" from "organization"
		And I press "Generate Report"
		Then I should see "Hero Miles Report"
        
	Scenario: Summary Report Results
  		Given the summary report page
		And I should see "Tickets" for each Year
		And I should see "Value" for each Year
		And I should see "Mileage" for each Year
		And I should see "Total Tickets" grand total of all "Tickets" 
		And I should see "Total Value" grand total of all "Value" 
		And I should see "Total Miles" grand total of all "Mileage" 
		Then I should see "View/Print"
		And I should see "Save"
		And I should see "Email"
		
	Scenario: View Summary Report In PDF Format
		Given the summary report page
		When I follow "View/Print"
		Then I should get a download with the filename "Report.pdf"
    	
	Scenario: Save Summary Report In PDF Format
		Given the summary report page
		When I follow "Save" 
		Then I should get a download with the filename "Report.pdf"

	Scenario: Email Summary Report In PDF Format
		Given the summary report page
		When I follow "Email" 
		Then I should see "Report - Print/Export Options"
		And I should see "Email"
		And I should see Button "Email as PDF"
		When I fill in "email_" with "test2@server.com"
		And I press "Email as PDF"
		Then I should see "Email sent successfully"

        Scenario: Summary Report with Blank Email Address
		Given the summary report page
		When I follow "Email" 
		Then I should see "Report - Print/Export Options"
		And I should see "Email"
		And I should see Button "Email as PDF"
		When I fill in "email_" with ""
		And I press "Email as PDF"
                Then I should see CSS "div.validation-failed"
		Then I should not see "Email sent successfully"	
		
        Scenario: View Monthly Report
		Given I am on the report management page
		When  I follow "monthly-link"
		Then I should see "Monthly Report"
		And I should see "Medical Center"
		And I should see "Organization"
		And I should see Button "Generate Report"
		And I select "All" from "med_monthly"
		And I select "All" from "org_monthly"
		And I press "Generate Report"
		Then I should see "Hero Miles Report"
		
	Scenario: Monthly Report Results
		Given I am on the monthly report page
		And I should see "January" to "December" Monthly Wise report for each Year
		And I should see "Total Tickets" for each Year
		And I should see "Total Savings" for each Year
		And I should see "Total Miles" for each Year
		Then I should see "View/Print"
		And I should see "Save"
		And I should see "Email"
		
	Scenario: View Monthly Report In PDF Format
		Given I am on the monthly report page
		When I follow "View/Print"
		Then I should get a download with the filename "Report.pdf"
    	
	Scenario: Save Monthly Report In PDF Format
		Given I am on the monthly report page
		When I follow "Save" 
		Then I should get a download with the filename "Report.pdf"

	Scenario: Email Monthly Report In PDF Format
		Given I am on the monthly report page
		When I follow "Email" 
		Then I should see "Report - Print/Export Options"
		And I should see "Email"
		And I should see Button "Email as PDF"
		When I fill in "email_" with "test2@server.com"
		And I press "Email as PDF"
		Then I should see "Email sent successfully"
	
        Scenario: Monthly Report with Blank Email Address
		Given I am on the monthly report page
		When I follow "Email" 
		Then I should see "Report - Print/Export Options"
		And I should see "Email"
		And I should see Button "Email as PDF"
		When I fill in "email_" with ""
		And I press "Email as PDF"
		Then I should not see "Email sent successfully"
                Then I should see CSS "div.validation-failed"
 
	Scenario: View Branch/Airline Reports
		Given I am on the report management page
		When  I follow "branch-link"
		Then I should see "Branch/Airline Reports"
		And I should see "Branch or Airline"
		And I should see "Tickets or Savings or Miles"
		And I should see "Year"
		And I should see "Medical Center"
		And I should see "Organization"
		And I should see Button "Generate Report"
		When I choose "branch_branch"
		And I choose "type_ticket"  
		And I select "2009" from "year"
		And I select "All" from "med"
		And I select "All" from "org"
		And I press "Generate Report"
		Then I should see "Hero Miles Report"
		
	Scenario: Branch/Airline Report Results
  		Given the branch report page
  		And I should see "January" to "December" Monthly Wise Airline report for the Year 2009
		And I should see "2009 Total"
		Then I should see "View/Print"
		And I should see "Save"
		And I should see "Email"
		
	Scenario: View Branch/Airline Report In PDF Format
		Given the branch report page
		When I follow "View/Print"
		Then I should get a download with the filename "Report.pdf"
    	
	Scenario: Save Branch/Airline Report In PDF Format
		Given the branch report page
		When I follow "Save" 
		Then I should get a download with the filename "Report.pdf"	

	Scenario: Email Branch/Airline Report In PDF Format
		Given the branch report page
		When I follow "Email" 
		Then I should see "Report - Print/Export Options"
		And I should see "Email"
		And I should see Button "Email as PDF"
		When I fill in "email_" with "test2@server.com"
		And I press "Email as PDF"
		Then I should see "Email sent successfully "		
	
        Scenario: Branch/Airline Report with balnk email address
		Given the branch report page
		When I follow "Email" 
		Then I should see "Report - Print/Export Options"
		And I should see "Email"
		And I should see Button "Email as PDF"
		When I fill in "email_" with ""
		And I press "Email as PDF"
		Then I should not see "Email sent successfully"
                Then I should see CSS "div.validation-failed"	
	
       Scenario: Checking Mail Branch/Airline Report 
		Given the branch report page
		When I follow "Email" 
		When I fill in "email_" with "test2@server.com"
		And I press "Email as PDF"
		Then I should see "Email sent successfully"  
                When "test2@server.com" opens the email with subject "Hero Miles Application Export"
        	Then I should see the email delivered from "admin@fisherhouse.org"
                Then I should see "A copy of the requested Hero Miles report is attached." in the email body
                Then I should see it is a multi-part email
        	Then attachment 1 should be named "Branch/Airline Report" 

        Scenario: Checking Mail Summary Report
		Given the summary report page
		When I follow "Email" 
		When I fill in "email_" with "test2@server.com"
		And I press "Email as PDF"
		Then I should see "Email sent successfully"
                When "test2@server.com" opens the email with subject "Hero Miles Application Export"
        	Then I should see the email delivered from "admin@fisherhouse.org"
                Then I should see "A copy of the requested Hero Miles report is attached." in the email body
                Then I should see it is a multi-part email
        	Then attachment 1 should be named "Summary Report"

        Scenario: Checking Mail Monthly Report
		Given I am on the monthly report page
		When I follow "Email" 
		When I fill in "email_" with "test2@server.com"
		And I press "Email as PDF"
		Then I should see "Email sent successfully"
                When "test2@server.com" opens the email with subject "Hero Miles Application Export"
        	Then I should see the email delivered from "admin@fisherhouse.org"
                Then I should see "A copy of the requested Hero Miles report is attached." in the email body
                Then I should see it is a multi-part email
        	Then attachment 1 should be named "Monthly Report"

        Scenario: Case Worker Report Management Page
		Given I am on the case worker reports page
                Then I should see "Reports" 
                Then I should see "Available Reports"
                Then I should see "Options"
                Then I should see "Report Name" 
                Then I should see "view"
                Then I should see "Printable Blank Application"
