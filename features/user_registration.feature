Feature: Case-worker Registration
  In order to use the HeroMiles Application System
  New case-workers must be able to register

  Scenario: Empty First Name
    Given I am on the new user registration page
    When I fill in "user_first_name" with ""
    And I press "Register!"
    Then I should see "Please enter your first name"

  Scenario: Empty Last Name
    Given I am on the new user registration page
    When I fill in "user_first_name" with "Omar"
    And I fill in "user_last_name" with ""
    And I press "Register!"
    Then I should see "Please enter your last name"

  Scenario: Empty Phone
    Given I am on the new user registration page
    When I fill in "user_phone_1" with ""
    And I press "Register!"
    Then I should see "Please enter your phone number"

  Scenario: Empty Email
    Given I am on the new user registration page
    When I fill in "user_email" with ""
    And I press "Register!"
    Then I should see "Please enter a valid email"

  Scenario: Invalid Email Format
    Given I am on the new user registration page
    When I fill in "user_email" with "email"
    And I press "Register!"
    Then I should see "Please enter a valid email"

  Scenario: Empty Password
    Given I am on the new user registration page
    When I fill in "user_password" with ""
    And I press "Register!"
    Then I should see "Please enter a password"

  Scenario: Short Password length
    Given there is an organisation "Bayside Health"
    And I am on the new user registration page
    When I fill in "user_first_name" with "Omar"
    And I fill in "user_last_name" with "Little"
    And I fill in "user_job_title" with "Sgt."
    And I select "Bayside Health" from "user_organization_id"
    And I fill in "user_phone_1" with "123"
    And I fill in "user_phone_2" with "123"
    And I fill in "user_phone_3" with "123"
    And I fill in "user_email" with "omar@bayside.com"
    And I fill in "user_password" with "pas"
    And I fill in "user_password_confirmation" with "pas"
    And I press "Register!"
    Then I am on the new user registration page

  Scenario: Password and confirmation don't match
    Given I am on the new user registration page
    When I fill in "user_password" with "abcdefgh"
    And I fill in "user_password_confirmation" with "12345678"
    And I press "Register!"
    Then I should see "Password and confirmation don't match. Please enter them again"

  Scenario: Valid Details
    Given there is an organisation "Bayside Health"
    And I am on the new user registration page
    When I fill in "user_first_name" with "Omar"
    And I fill in "user_last_name" with "Little"
    And I fill in "user_job_title" with "Sgt."
    And I select "Bayside Health" from "user_organization_id"
    And I fill in "user_phone_1" with "123"
    And I fill in "user_phone_2" with "123"
    And I fill in "user_phone_3" with "123"
    And I fill in "user_email" with "omar@bayside.com"
    And I fill in "user_password" with "password"
    And I fill in "user_password_confirmation" with "password"
    And I press "Register!"
    Then I should see "Thank you. You will be notified when your account is approved."

  Scenario: Checking Mail for new user registration
    Given the admin has logged in
    When "admin@heromiles.com" opens the email with subject "A new user 'Lester Freamon' has signed up"
    Then I should see the email delivered from "admin@fisherhouse.org"
    Then I should see "A new user has signed up:" in the email body

  Scenario: Checking No mail for Recevie notification not checked
    Given a user exists with email "test@user.com" and password "password"
    And the user is an administrator
    Given the user is no receive notification
    Given there is an organisation "Bayside Health"
    And I am on the new user registration page
    When I fill in "user_first_name" with "Omar"
    And I fill in "user_last_name" with "Little"
    And I fill in "user_job_title" with "Sgt."
    And I select "Bayside Health" from "user_organization_id"
    And I fill in "user_phone_1" with "123"
    And I fill in "user_phone_2" with "123"
    And I fill in "user_phone_3" with "123"
    And I fill in "user_email" with "omar@bayside.com"
    And I fill in "user_password" with "password"
    And I fill in "user_password_confirmation" with "password"
    And I press "Register!"
    Then I should see "Thank you. You will be notified when your account is approved."
    Then "test@user.com" should receive no emails with subject "A new user 'Omar Little' has signed up"
