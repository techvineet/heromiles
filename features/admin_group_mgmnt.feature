@javascript

Feature: Admin Group Management
	  In order to manage groups
	  As an administrator
	  He/she should be able to create/update/delete them

	Background:
  		Given the admin has logged in

	Scenario: Group Management Page
		Given I am on the group management page
		Then I should see "Group Management"
		And I should see "Add New Group"
		And I should see "Available Groups"

	Scenario: Group Application on new group
		Given I am on the group management page
		When I fill in "group_name" with "Group 1"
		And I press "Create!"
		Then I should be on the new group page
		And I should see "Case Worker Information"
		And I should see "Referred By"
		And I should see "Service Member Information"
		And I should see "Flight Information"
		And I should see "Passenger Information"
		And I should see "Save Template"

	Scenario: Create new group without any info
		Given I am on the group management page
		And there are no groups
		When I fill in "group_name" with "Group 2"
		And I press "Create!"
		And I follow "submit-group"
		Then I should see "Group 2"
		And I should see "Group created successfully"
		And I should see "edit template"
		And I should see "applications"
		And I should see "delete"
		And I should not see "Archive"

	Scenario: Create new group with some info
		Given an existing group "Group1"
		And I am on the group management page
		When I fill in "group_name" with "Group 2"
		And I press "Create!"
		And I fill in "application_referrer_first_name" with "Vineet"
		And I fill in "application_referrer_last_name" with "Sethi"
		And I follow "submit-group"
		And I click "edit template" under "tr#group_2"
		Then I should see "Edit 'Group 2' Template"
		And the "application_referrer_first_name" field should contain "Vineet"
		And the "application_referrer_last_name" field should contain "Sethi"
		
	Scenario: Update a group
		Given an existing group "Group1"
		And I am on the group management page
		When I fill in "group_name" with "Group 2"
		And I press "Create!"
		And I fill in "application_referrer_first_name" with "Vineet"
		And I fill in "application_referrer_last_name" with "Sethi"
	        And I follow "submit-group"
		And I click "edit template" under "tr#group_2"
		And I fill in "application_referrer_first_name" with "Daku"
		And I fill in "application_referrer_last_name" with "Haseena"
		And I follow "submit-group"
		And I click "edit template" under "tr#group_2"
		Then I should see "Edit 'Group 2' Template"
		And the "application_referrer_first_name" field should contain "Daku"
		And the "application_referrer_last_name" field should contain "Haseena"
		
	Scenario: Delete a group
		Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "delete" under "tr#group_2"
		Then I should not see "Group2"
		And I should see "Group1"
		And I should see "Group deleted successfully"

	Scenario: Group applications
		Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_2"
		Then I should see "Group2 Group Application Entry"
		And I should see "Add an Application"
                And I should see "Departing"
                And I should see "Service Member"
                And I should see "Status"
                And I should see "Created"
                And I should see "Submitted"
                And I should see "Completed"
  
	Scenario: Group application template
		Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_2"
		And I follow "add-app-btn"
		Then I should see "New Application"
		And the "application_first_name" field should contain "Vineet"
		And the "application_last_name" field should contain "Sethi"

	Scenario: Group add application to see archive
		Given an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_1"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
		Then I should see "archive"

	Scenario: Group add application to see applications list
		Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_2"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
		And I click "applications" under "tr#group_2"
		Then I should see "Sethi, Vineet"
		And I should see "Saved"
		And I should see "view"

	Scenario: View application for a group
		Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_2"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
		And I click "applications" under "tr#group_2"
		And I follow "view"
		Then I should see "View Application"
		And I should see "Admin View"
		And I should see "Administrator Information"
		And I should see "Service Member Information"
		And I should see "Flight Information"
		And I should see "Passenger Information"
		And I should see "Terms of Use"

	Scenario: Paginate applications
                Given an existing group "Group"
                And there are 12 applications in the group
                And I am on the group management page
                When I click "applications" under "tr#group_1"
                Then I should see "1 2 Next › Last"
                And I follow "2"
                Then I should see "view" within "tr#application_12"
                And I should see "Sethi, Vineet" within "tr#application_12"

        Scenario: View Monthly Report In PDF Format
		Given an existing group "Group"
                And there are 2 applications in the group
		And I am on the group management page
		And I click "applications" under "tr#group_1"
		And I follow "PRINT/EXPORT"
	  	Then I should see Button "Print Application"
	  	Then I should see Button "Download as PDF"
	  	Then I should see Button "Email as PDF"
	  	Then I should see "Email"

        Scenario: Download Applications list as PDF
		Given an existing group "Group1"
		And I am on the group management page
		When I click "applications" under "tr#group_1"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
		And I click "applications" under "tr#group_1"
		And I follow "PRINT/EXPORT"
		Then I should see "Group Applications - Open/Export options"
	        When I follow "Download as PDF"
	        Then I should get a download with the filename "GroupApplications.pdf"

        Scenario: Email Applications list as PDF for valid email address
	        Given an existing group "Group"
                And there are 2 applications in the group
                And I am on the group management page
                When I click "applications" under "tr#group_1"
                And I follow "PRINT/EXPORT"
                Then I fill in "pdf_email_p_" with "aa@gmail.com"
                When I press "Email as PDF"
                Then I should see "Email sent successfully"
 
       Scenario: Email Applications list as PDF for invalid email address
	        Given an existing group "Group"
                And there are 2 applications in the group
                And I am on the group management page
                When I click "applications" under "tr#group_1"
                And I follow "PRINT/EXPORT"
                Then I fill in "pdf_email_p_" with "aa"
                When I press "Email as PDF"
                Then I should see CSS "div.validation-failed" 

        Scenario: Archive and delete options are displayed with different scenario
                Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_2"
		Then I should see "Group2 Group Application Entry"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
                Then I should see "delete" within "tr#group_1"
                Then I should see "archive" within "tr#group_2"

        Scenario:  Select Archive option for group
                Given an existing group "Group1"
		And I am on the group management page
		When I click "applications" under "tr#group_1"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
                Then I should see "archive" within "tr#group_1"
                Then I should see CSS "tr.group"
                When I follow "archive" and confirm popup
                Then I should see "unarchive" within "tr#group_1"
                Then I should see CSS "tr.archived"

       Scenario: Check Display archived group for display all groups
                Given an existing group "Group1"
		And an existing group "Group2"
		And I am on the group management page
		When I click "applications" under "tr#group_2"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
                Then I should see "delete" within "tr#group_1"
                Then I should see "archive" within "tr#group_2"
                When I follow "archive" and confirm popup
                Then I should see CSS "tr.hidden"
                When I check "display_archived"  
                Then I should not see CSS "tr.hidden"
        
       Scenario: Select Unarchive option for group
                Given an existing group "Group1"
		And I am on the group management page
		When I click "applications" under "tr#group_1"
		And I follow "add-app-btn"
		And I press "Save"
		Given I am on the group management page
                Then I should see "archive" within "tr#group_1"
                When I follow "archive" and confirm popup
                Then I should see "unarchive" within "tr#group_1"
                When I check "display_archived" 
                When I follow "unarchive" and confirm popup
                Then I should see "archive" within "tr#group_1"

       Scenario: Checking Mail Applications for group application
	        Given an existing group "Group"
                And there are 2 applications in the group
                And I am on the group management page
                When I click "applications" under "tr#group_1"
                And I follow "PRINT/EXPORT"
                Then I fill in "pdf_email_p_" with "aa@gmail.com"
                When I press "Email as PDF"
                Then I should see "Email sent successfully"
 		When "aa@gmail.com" opens the email with subject "List of Group Applications"
        	Then I should see it is a multi-part email
        	Then I should see the email delivered from "admin@fisherhouse.org"
        	Then attachment 1 should be named "GroupApplications.pdf"
      
