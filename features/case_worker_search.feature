@javascript

Feature: Case Worker Search
	In order to search the applications in Hero Miles application for case worker

	Background:
  		Given the case_worker has logged in
  	
  	  
  	Scenario: Search button display on case worker application page
		Given I am on the case worker applications page
		Then I should see "Search" 
		And I should see Button "Go!"

	Scenario: No result found from search query in case workerapplication management page
                Given there are 1 open "saved" applications "Alexander Mattos" in the group
 		Given the Sphinx indexes are updated
                And I am on the case worker applications page
		Then I should see "Search" 
		And I should see Button "Go!"
		When I fill in "q" with "kkkk"
		And I press "Go!"
		Then I should see "No Results found"
           
        Scenario: Searching particular application in case worker application management page
		Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                Then I should see "Search"
                When I fill in "q" with ""
                And I press "Go!"
                Then I should see "Alexander"
		Then I should see "view"
       
        Scenario: Unsearched application not found in searched list in case worker application management page
                Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given there are 1 open "saved" applications "app user" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                Then I should see "Search" 
		When I fill in "q" with "app"
		And I press "Go!"
		Then I should not see "Search Results for 'Alexander'"
                Then I should not see "Alexander"
         
         Scenario: Searching with blank will display all applications in case worker application management page
 		Given there are 2 open "completed" applications "app1" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
		Then I should see "Search" 
		And I should see Button "Go!"
		When I fill in "q" with ""
		And I press "Go!"
		Then I should see "Displaying all 2 applications"
	
         Scenario: Paginated Lists for searched result in case worker application management page
                Given there are 13 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                Then I should see "Search" 
		When I fill in "q" with "app"
                And I press "Go!"
                Then I should see "Displaying applications 1–11 of 13 in total"
                Then I should see "Next"
                Then I should see "Last"
                When I follow "2"
                Then I should see "Displaying applications 12–13 of 13 in total"
        
         Scenario: Export the case worker applications search results
		Given there are 1 open "declined" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When I fill in "q" with "app"
                And I press "Go!"
                Then I should see "Search Results"
		And I should see "PRINT/EXPORT" 
		When  I follow "export-search-link"
		Then I should see "Print/Export Search Results"
		And I should see "save"
		When I follow "save-all-data-link"
                Then I should get a download with the filename "SearchExport.csv"

          Scenario: Check Default setting for Advanced Search options are in case worker application management page
		Given I am on the case worker applications page
		Then I should see "Advanced search" 
		When  I follow "advanced-search-link"
		And I should see "Show all applications for Baltimore General"
		And I should see "Restrict by Group"
		
		And I should see "Restrict searched status to"
                Then the "completed" checkbox should be checked
                Then the "submitted" checkbox should be checked
                Then the "exceptions" checkbox should be checked
                Then the "saved" checkbox should be checked
                Then the "returned" checkbox should be checked
		Then the "reopened" checkbox should not be checked                
		Then the "declined" checkbox should not be checked  
 
		And I should see "Restrict searched fields to"
		And I should see "Restrict searched dates to"

        Scenario: Searching applications in case worker application management page with restrict to other searched status
		Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given there are 1 open "declined" applications "Axander Mttos" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "declined"
                When I uncheck "exceptions"
                When I fill in "q" with ""
                And I press "Go!"
                Then I should see "Displaying 1 application"
		Then I should see "Declined"

        Scenario: Searching applications in case worker application management page with searched restrict to
		Given there are 1 open "exception" applications "Alexander Mattos" in the group
                Given there are 1 open "completed" applications "Alex Matt" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "service_member"
                When I check "email"
                When I fill in "q" with "Alexander"
                And I press "Go!"
                Then I should see "Exception"
                When I fill in "q" with "Alexapp0@gmail.com" 
		And I press "Go!"
                Then I should see "Completed"

         Scenario: Paginated Lists for searched result in case worker application management page using advanced search combination
                Given there are 13 open "declined" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "service_member"
                When I check "email"
                When I check "declined"
		When I fill in "q" with "app"
                And I press "Go!"
                Then I should see "Displaying applications 1–11 of 13 in total"
                Then I should see "Next"
                Then I should see "Last"
                When I follow "2"
                Then I should see "Displaying applications 12–13 of 13 in total"  

        Scenario: Searching the case worker applications using advance search option with all applications for organization
		Given there are 4 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
		When I follow "advanced-search-link"
	        When I check "all_users"
                When I fill in "q" with "app"
                When I press "Go!"
		Then I should see "Displaying all 4 applications"
        
         Scenario: Searching applications in application management page with searched for request departure for restrict date to
		Given there are 1 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When I follow "advanced-search-link"
                When I select "June" from "_search_from_2i"
                When I select "2" from "_search_from_3i"
		When I select "2012" from "_search_from_1i"
		When I select "June" from "_search_till_2i"
                When I select "10" from "_search_till_3i"
		When I select "2012" from "_search_till_1i"
                When I select "Requested Departure" from "date_for"
                When I fill in "q" with "app"
                When I press "Go!"
                Then I should see "Displaying 1 application"

          Scenario: Searching applications in application management page with searched for arrival for restrict date to
		Given there are 1 open "exception" applications "app one" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When I follow "advanced-search-link"
                When I select "July" from "_search_from_2i"
                When I select "1" from "_search_from_3i"
		When I select "2012" from "_search_from_1i"
		When I select "July" from "_search_till_2i"
                When I select "20" from "_search_till_3i"
		When I select "2012" from "_search_till_1i"
                When I select "Requested Arrival" from "date_for"
                When I fill in "q" with "app"
                When I press "Go!"
                Then I should see "Displaying 1 application"
           
          Scenario: Searching applications in application management page with check service member clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "AJACK AZIMMERMAN" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on service member name
                When I clicking sortable link "td:nth-child(2)" under "table thead"
                When I clicking sortable link "td:nth-child(3)" under "table thead" 
                Then I should see "ZIMMERMAN, JEREMY" within "table tbody tr:nth-child(1)"
                Then I should see "ZIMMERMAN, JACK" within "table tbody tr:nth-child(2)"
                Then I should see "AZIMMERMAN, AJACK" within "table tbody tr:nth-child(4)"
                When I clicking sortable link "td:nth-child(3)" under "table thead" 
                Then I should see "AZIMMERMAN, AJACK" within "table tbody tr:nth-child(1)"
                Then I should see "ZIMMERMAN, JEREMY" within "table tbody tr:nth-child(4)" 
                    
          Scenario: Searching applications in application management page with check departing clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "final_departure_date" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "final_departure_date" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "final_departure_date" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on departing date
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(2)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                When I clicking sortable link "td:nth-child(2)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)" 

         Scenario: Searching applications in application management page with check case worker name column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "case_worker_last_name" for this application with "User"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "case_worker_last_name" for this application with "Ore"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "case_worker_last_name" for this application with "Voull"
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on case worker name
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(8)" under "table thead"
                Then I should see "Lester Voull" within "table tbody tr:nth-child(1)"
                Then I should see "Lester User" within "table tbody tr:nth-child(2)"
                Then I should see "Lester Ore" within "table tbody tr:nth-child(3)"
                When I clicking sortable link "td:nth-child(8)" under "table thead"
                Then I should see "Lester Ore" within "table tbody tr:nth-child(1)"
                Then I should see "Lester Voull" within "table tbody tr:nth-child(3)"

         Scenario: Searching applications in application management page with check status clickable column
		Given there are 1 open "exception" applications "JACK ZIMMERMAN" in the group
                Given there are 1 open "saved" applications "JEREMY ZIMMERMAN" in the group
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on status
                When I clicking sortable link "td:nth-child(3)" under "table thead" 
                When I clicking sortable link "td:nth-child(4)" under "table thead"
                Then I should see "Saved" within "table tbody tr:nth-child(1)"
                Then I should see "Exception" within "table tbody tr:nth-child(2)"
                When I clicking sortable link "td:nth-child(4)" under "table thead"
                Then I should see "Completed" within "table tbody tr:nth-child(1)"
                Then I should see "Exception" within "table tbody tr:nth-child(2)"

         Scenario: Searching applications in application management page with check created_at clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "created_at" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "created_at" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "created_at" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on created_at
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(5)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                 When I clicking sortable link "td:nth-child(5)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)" 

        Scenario: Searching applications in application management page with check booked_at clickable column
		Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "booked_at" for this application with "1901-09-01"
                Given there are 1 open "completed" applications "JEREMY ZIMMERMAN" in the group
                Given Modify "booked_at" for this application with "1902-04-05"
                Given there are 1 open "completed" applications "JACK ZIMMERMAN" in the group
                Given Modify "booked_at" for this application with "1901-03-04" 
                Given the Sphinx indexes are updated
                And I am on the case worker applications page
                When  I follow "advanced-search-link"
                When I check "saved"
                When I fill in "q" with ""
                And I press "Go!"
                # check sorttable on booked_at
                When I clicking sortable link "td:nth-child(3)" under "table thead"
                When I clicking sortable link "td:nth-child(7)" under "table thead"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(1)"
                Then I should see "09/01/1901" within "table tbody tr:nth-child(2)"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(3)"
                 When I clicking sortable link "td:nth-child(7)" under "table thead"
                Then I should see "03/04/1901" within "table tbody tr:nth-child(1)"
                Then I should see "04/05/1902" within "table tbody tr:nth-child(3)" 

        		
   
