HeroMiles::Application.routes.draw do

  # Admin routes
  match "/administration" => 'administration#index', :as => 'admin_home'
  match "/administration/applications" => 'administration#applications', :as => 'application_management'
  match '/administration/applications/new' => 'administration#new_application', :as => 'admin_new_application'
  match '/administration/applications/open' => 'administration#show_open'
  match '/administration/applications/notifications' => 'administration#show_notifications'
  match '/administration/applications/:id' => 'administration#show_application', :as => 'admin_show_application'
  match '/administration/organizations' => 'administration#organizations', :as => 'organization_management'
  match '/administration/medical_centers' => 'administration#medical_centers', :as => 'medical_center_management'
  match '/administration/airports' => 'administration#airports', :as => 'airport_management'
  match '/administration/airlines' => 'administration#airlines', :as => 'airline_management'
  match '/administration/groups' => 'administration#groups', :as => 'group_management'
  match '/administration/groups/new' => 'administration#new_group', :as => 'new_group'
  match '/administration/groups/:id/edit' => 'administration#edit_group', :as => 'edit_group'
  match '/administration/groups/:id/mail' => 'administration#mail_group', :as => 'mail_group'
  match '/administration/groups/:id' => 'administration#show_group', :as => 'show_group'
  match '/administration/settings' => 'administration#settings', :as => 'settings'
  match '/administration/attachments' => 'administration#attachments', :as => 'attachments'

  match '/administration/accounts' => 'administration#accounts', :as => 'account_management'
  match '/administration/accounts/pending' => 'administration#show_pending'
  match '/administration/accounts/mail_pending' => 'administration#mail_pending'
  match '/administration/accounts/mail_active' => 'administration#mail_active'
  match '/administration/accounts/active' => 'administration#show_active'
  match '/administration/accounts/new' => 'administration#new_account', :as => 'new_account'
  match '/administration/accounts/:id' => 'administration#show_account', :as => 'account'

  match '/administration/reports' => 'administration#reports', :as => 'report_management'
  match '/administration/advanced' => 'administration#advanced', :as => 'advanced_management'

  # Case-worker routes
  match "/case_worker" => 'case_worker#index', :as => 'case_worker_home'

  match '/case_worker/applications' => 'case_worker#applications', :as => 'case_worker_applications'
  match '/case_worker/applications/new' => 'case_worker#new_application', :as => 'case_worker_new_application'
  match '/case_worker/applications/open' => 'case_worker#show_open'
  match '/case_worker/applications/notifications' => 'case_worker#show_notifications'
  match '/case_worker/applications/:id' => 'case_worker#show_application', :as => 'case_worker_show_application'

  match '/case_worker/accounts' => 'case_worker#accounts', :as => 'case_worker_accounts'
  match '/case_worker/reports' => 'case_worker#reports', :as => 'case_worker_reports'
  match '/case_worker/groups' => 'case_worker#groups', :as => 'case_worker_groups'
  match '/case_worker/groups/:id/mail' => 'case_worker#mail_group', :as => 'case_worker_mail_group'
  match '/case_worker/groups/:id' => 'case_worker#show_group', :as => 'case_worker_show_group'

  match '/search/applications' => 'search#applications', :as => 'applications_search'
  match '/search/accounts' => 'search#accounts', :as => 'accounts_search'

  match '/sign_up_confirmation' => 'home#sign_up_confirmation'


  devise_for :users, :controllers => {:registrations => 'registrations',
                                      :sessions => 'sessions' } do
    get 'sign_in', :to => 'sessions#new', :as => 'sign_in'
    get 'sign_out', :to => 'devise/sessions#destroy', :as => 'sign_out'
  end

  resources :users, :path => 'accounts', :only => [:create, :update, :destroy] do
    member do
      post 'approve'
    end
  end

  resources :applications do
    member do
      get 'checkout'
      get 'checkin'
      get 'reopen'
      get 'export'
      post 'mail'
    end
    collection do
      get 'airport'
    end
  end

  resources :application_notes, :only => :create

  resources :organizations, :medical_centers, :airports, :airlines, :groups do
    member do
      post 'archive'
      post 'unarchive'
    end
  end

  resources :app_settings, :only => [:create, :update] do
    collection do
      post 'new_category'
      post 'remove_category'
      post 'update_category'
    end
  end

  match '/reports/summary' => 'reports#summary', :as => 'summary_report'
  match '/reports/monthly' => 'reports#monthly', :as => 'monthly_report'
  match '/reports/branch_airline' => 'reports#branch', :as => 'branch_report'

  root :to => 'home#index'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
