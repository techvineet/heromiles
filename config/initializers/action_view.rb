module ActionView
  module Helpers
    module DateHelper
      def time_select_12_hour(object_name, method, options = {}, html_options = {})
        InstanceTag.new(object_name, method, self, options.delete(:object)).to_time_select_tag(options, html_options)
      end
    end
  end

  class FormBuilder
    def time_select_12_hour(method, options = {}, html_options = {})
      @template.time_select(@object_name, method, objectify_options(options), html_options)
    end
  end
end
